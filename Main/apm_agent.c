#include "main_info.h"

static struct communication comm1;
static int comm_fd;
static char buf[20];
static int machine_state;

struct {
	int yaw, roll, pitch, throttle;
} memo;

static void sighandler(int sig, siginfo_t *info, void *none){
	int cmd=info->si_value.sival_int;

	int rld=PRO_RELATED(cmd);
	int instr=PRO_INSTR(cmd);
	int num=PRO_NUM(cmd);
	switch (sig){
#ifndef __DEBUG_NO_NET
	case SERVER_SIG:
#ifndef __DEBUG_APM_PRINT_ONLY
		switch (instr){
			case YAW_INSTR:
				set_yaw(&comm1, num);
				command_write(comm_fd, comm1); 
				break;
			case ROLL_INSTR:
				set_roll(&comm1, num);
				command_write(comm_fd, comm1); 
				break;
			case PITCH_INSTR:
				set_pitch(&comm1, num);
				command_write(comm_fd, comm1); 
				break;
			case THROT_INSTR:
				set_throttle(&comm1, num);
				command_write(comm_fd, comm1); 
				break;
			case MODE_INSTR:
				set_mode(&comm1, num);
				command_write(comm_fd, comm1); 
				break;
			case MIN_INSTR:
				low_radio(&comm1);
				command_write(comm_fd, comm1); 
				break;
			case MAX_INSTR:
				high_radio(&comm1);
				command_write(comm_fd, comm1); 
				break;
			default: break;
		}
#else
		printf("apm_agent> intr: \t%d. num: \t%d.\n", instr, num);
#endif
		break;
#endif
#ifndef __DEBUG_NO_ROUTE
	case ROUTE_SIG:
		break;
#endif
#ifndef __DEBUG_NO_OBSAVD
	case OBSAVD_SIG:
		break;
#endif
	}
}

int apm_agent(void *args){
	machine_state=0;
#ifndef __DEBUG_APM_PRINT_ONLY
        comm_fd=wrap_uart_init(apm_device,57600,8,'N',1);
	if (comm_fd<0){
		return -1;
	}
	reset_radio(&comm1);
	command_write(comm_fd, comm1);
#endif

	struct sigaction act;

	sigfillset(&act.sa_mask);
	act.sa_sigaction=sighandler;
	act.sa_flags=SA_SIGINFO | SA_RESTART;

#ifndef __DEBUG_NO_NET
	printf("apm_agent> server sig registered.\n");
	sigaction(SERVER_SIG, &act, NULL);
#endif
#ifndef __DEBUG_NO_ROUTE
	sigaction(ROUTE_SIG, &act, NULL);
#endif
#ifndef __DEBUG_NO_OBSAVD
	sigaction(OBSAVD_SIG, &act, NULL);
#endif

	while(1){
		pause();
#ifndef __DEBUG_NO_TACTICS
#endif
	}

	return 0;
}
