#include "main_info.h"

static pthread_t gps_thrd;
static pthread_t apmr_thrd;
static void *gps_gather(void *args);
static void *apmr_gather(void *args);

int info_gather(void *args){
	void *retval;

#ifndef __DEBUG_NO_GPS
	printf("info_gather> gps_gather to launch.\n");
	if (wrap_thrd_create(&gps_thrd, gps_gather, NULL)){
		fprintf(stderr, "info_gather> gps_gather launch failed!\n");
		return -1;
	}
#endif
#ifndef __DEBUG_NO_APMR
	printf("info_gather> apmr_gather to launch.\n");
	if (wrap_thrd_create(&apmr_thrd, apmr_gather, NULL)){
		fprintf(stderr, "info_gather> apmr_gather launch failed!\n");
		return -1;
	}
#endif
#ifndef __DEBUF_NO_APMR
	wrap_thrd_join(apmr_thrd, &retval);
	printf("info_gather> apmr return value: %d.\n", (int)retval);
#endif
#ifndef __DEBUF_NO_GPS
	wrap_thrd_join(gps_thrd, &retval);
	printf("info_gather> gps return value: %d.\n", (int)retval);
#endif
	return 0;
}

static void *gps_gather(void *args){
	int fd=gps_init(gps_device ,38400,8,'N',1);
	enum gps_type ty;
	char buf[BUF_SIZE];
	if (fd==-1) {
		perror("cannot open device!");
		return (void *)-1;
	}
	printf("gps inited.\n");
	wrap_sempost(&shrmem->gps_sem);
	struct gps_struct gps_var;
	while (1){
		if (gps_readline(fd, buf)==-2){
			fprintf(stderr, "child 2 exited abnormally!\n");
			return (void *)-1;
		}

		ty=gps_update(buf, &gps_var);
		wrap_semwait(&shrmem->gps_sem);
		shrmem->gps=gps_var;
		wrap_sempost(&shrmem->gps_sem);
	}
	return NULL;
}

static void *apmr_gather(void *args){return NULL;}
