#include "main_info.h"
#include <sys/epoll.h>
#define __DEBUG_HAVE_EPOLL		0

int new_stdout;
int new_stderr;
char log_filename[100];

static int stdout_dump[2];
static int stderr_dump[2];
static char log_buffer[1024];
static FILE *log_file;

#if __DEBUG_HAVE_EPOLL==1
static int epoll_fd;
static struct epoll_event event_collection[4];
#endif

static int create_logfile(){
	return 0;
}

int stdio_remap(){
	if (pipe(stdout_dump)){
		perror("stdio_remap> pipe create error");
		return -1;
	}
	if (pipe(stderr_dump)){
		perror("stdio_remap> pipe create error");
		return -1;
	}

	new_stdout=dup(1);
	if (new_stdout==-1){
		perror("stdio_remap> dup error");
		return -2;
	}
	new_stderr=dup(2);
	if (new_stderr==-1){
		perror("stdio_remap> dup error");
		return -2;
	}

	if (dup2(stdout_dump[1], 1)!=1){
		perror("stdio_remap> dup2 error");
		return -3;
	}
	if (dup2(stderr_dump[1], 2)!=2){
		perror("stdio_remap> dup2 error");
		return -3;
	}

	if (setvbuf(stdout, NULL, _IOLBF, -1)){
		perror("stdio_remap> setvbuf error");
		return -4;
	}
	if (setvbuf(stderr, NULL, _IOLBF, -1)){
		perror("stdio_remap> setvbuf error");
		return -4;
	}

	return 0;
}

int logger(void *args){
	if (create_logfile()){
		fprintf(stderr, "create file error!\n");
		return -1;
	}

	close(stdout_dump[1]);
	close(stderr_dump[1]);

	FILE *pstdout=fdopen(stdout_dump[0], "r");
	if (pstdout==NULL){
		fprintf(stderr, "logger> fdopen 1 error!\n");
		return -1;
	}

	FILE *pstderr=fdopen(stderr_dump[0], "r");

	if (pstderr==NULL){
		fprintf(stderr, "logger> fdopen 2 error!\n");
		return -1;
	}

	if (dup2(new_stdout, 1)!=1){
		perror("logger> dup2 error");
		return -2;
	}
	if (dup2(new_stderr, 2)!=2){
		perror("logger> dup2 error");
		return -2;
	}

#ifndef __DEBUG_NO_LOGGER_SERVER
	printf(log_server);
	printf("\n");
	int netsock=wrap_client(log_server);
	if (netsock<0)
		return -1;
#endif

#if __DEBUG_HAVE_EPOLL==1
	struct epoll_event event;
	FILE *target=NULL;

	epoll_fd=epoll_create(2);
	if (epoll_fd==-1){
		perror("logger> epoll instance create error");
		return -3;
	}

	bzero(&event, sizeof(event));
	event.events=EPOLLIN | EPOLLERR | EPOLLHUP;
	if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, 
			stdout_dump[0], &event)){
		perror("logger> epoll control error");
		return -4;
	}

	bzero(&event, sizeof(event));
	event.events=EPOLLIN | EPOLLERR | EPOLLHUP;
	if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, 
			stderr_dump[0], &event)){
		perror("logger> epoll control error");
		return -4;
	}

	while (1){
		int nevt=epoll_wait(epoll_fd, event_collection, 
				4, 0);
		int i, len;
		log_t tag;
		for (i=0;i<nevt;i++){
			if (event_collection[i].events&EPOLLERR
			|| event_collection[i].events&EPOLLHUP){
				fprintf(stderr, "logger> epoll wait error!\n");
				continue;
			}
			else{
				target=event_collection[i].data.fd==stdout_dump[0]?
					pstdout:pstderr;
				tag=event_collection[i].data.fd==stdout_dump[0]?
					0:1<<31;
				if (fgets(log_buf, 1024, target)==NULL)
					continue;
				printf(log_buf);
//				fprintf(log_file, log_buf);
#ifndef __DEBUG_NO_LOGGER_SERVER
				len=strlen(log_buf);
				tag+=len;
				if (wrap_send(netsock, &tag, sizeof(tag), 0)==-1
				|| wrap_send(netsock, log_buf, len, 0)==-1){
					fprintf(stderr, "logger> network send error!\n");
					continue;
				}
#endif
			}
		}

	}
	close(epoll_fd);
#else
	while (1){
	
	}
#endif
}
