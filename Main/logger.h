#ifndef _logger_H_
#define _logger_H_
#include <linux/types.h>

typedef __u32 log_t;

extern int new_stdout;
extern int new_stderr;
extern char log_filename[];

int stdio_remap();

#endif
