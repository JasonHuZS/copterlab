#include "main_info.h"

int gps_agent(void *args){
	int fd=gps_init(gps_device ,38400,8,'N',1);
	enum gps_type ty;
	char buf[BUF_SIZE];
	if (fd==-1) {
		perror("cannot open device!");
		return -1;
	}
	printf("gps inited.\n");
	wrap_sempost(&shrmem->gps_sem);
	struct gps_struct gps_var;
	while (1){
		if (gps_readline(fd, buf)==-2){
			fprintf(stderr, "child 2 exited abnormally!\n");
			return -1;
		}

		ty=gps_update(buf, &gps_var);
		wrap_semwait(&shrmem->gps_sem);
		shrmem->gps=gps_var;
		wrap_sempost(&shrmem->gps_sem);
	}
	return 0;
}
