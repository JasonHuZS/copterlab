#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#include "main_info.h"
#define READ_BUF_SIZE		(100)

struct mission mission_list[]={
#ifndef __DEBUG_NO_APM
	STATIC_MISSION(apm_agent, NULL),
#endif
#ifndef __DEBUG_NO_ATTITUDE
	STATIC_MISSION(attitude, NULL),
#endif
#ifndef __DEBUG_NO_GPS
	STATIC_MISSION(gps_agent, NULL),
#endif
#ifndef __DEBUG_NO_NET
	STATIC_MISSION(net_agent, NULL),
#endif
#ifndef __DEBUG_NO_OBSAVD
	STATIC_MISSION(obsavd, NULL)
#endif
};
struct shr_block *shrmem;

char ctrl_server[100];
char data_server[100];
char route_server[100];

char gps_device[100];
char apm_device[100];
char obsavd_device[100];
char inertial_device[100];

void configure_setup(const char *filename);

int main(int argc, char **argv){
	pid_t pid;
	int i;
	
	if (argc<2)
		configure_setup("configure");
	else
		configure_setup(argv[1]);
	shrmem=wrap_anonmmap(SHR_PAGES);
	if (shrmem==MAP_FAILED){
		fprintf(stderr, "mmap error!\n");
		return -1;
	}
	if (wrap_anonmutex(&shrmem->gps_sem)){
		return -1;
	}
	if (wrap_semwait(&shrmem->gps_sem)){
		return -1;
	}
	
	for (i=0;i<mission_num(mission_list);i++){
		pid=wrap_fork();
		if (pid==0)
			do_mission(&mission_list[i]);
		else
			mission_list[i].pid=pid;
	}
	pid_t my_pid=getpid();
	int status;
//	printf("inside of parent: %d.\n",my_pid);
	usleep(10000);
	while ((pid=wait(&status))>0){
		//find where is pid and startup undertaker
	}
	if (errno==EINTR)
		fprintf(stderr, "unhandlable error occurs!\n");
	wrap_semdestroy(&shrmem->gps_sem);
	if (wrap_munmap(shrmem, SHR_PAGES)){
		fprintf(stderr, "munmap error!\n");
		return -1;
	}
	return 0;
}

void configure_setup(const char *filename){
	FILE *fp=fopen(filename, "r");
	if (fp==NULL){
		fprintf(stderr, "cannot open file: %s\n", filename);
		fprintf(stderr, "use default setting.\n");
		strcpy(ctrl_server, CTRL_SERVER);
		strcpy(data_server, DATA_SERVER);
		strcpy(route_server, ROUTE_SERVER);
		strcpy(gps_device, GPS_DEVICE);
		strcpy(apm_device, APM_DEVICE);
		strcpy(obsavd_device, OBSAVD_DEVICE);
		strcpy(inertial_device, INERTIAL_DEVICE);
	}
	else {
		char *readbuf=malloc(READ_BUF_SIZE);
		char *ip=malloc(READ_BUF_SIZE);
		char *ctrl_port=malloc(READ_BUF_SIZE);
		char *data_port=malloc(READ_BUF_SIZE);
		char *route_port=malloc(READ_BUF_SIZE);
		while (fgets(readbuf, READ_BUF_SIZE, fp)){
			if (readbuf[0]=='#' || readbuf[0]=='\0' 
					|| readbuf[0]==' ') continue;
			int c, l;
			l=strlen(readbuf);
			for (c=0; c<l; c++)
				if (readbuf[c]=='='){
					readbuf[c]='\0';
					c++;
					break;
				}
			if (strcmp(readbuf, "ip")==0)
				strcpy(ip, readbuf+c);
			else if (strcmp(readbuf, "ctrl_port")==0)
				strcpy(ctrl_port, readbuf+c);
			else if (strcmp(readbuf, "data_port")==0)
				strcpy(data_port, readbuf+c);
			else if (strcmp(readbuf, "route_port")==0)
				strcpy(route_port, readbuf+c);
			else if (strcmp(readbuf, "gps")==0)
				strcpy(gps_device, readbuf+c);
			else if (strcmp(readbuf, "apm")==0)
				strcpy(apm_device, readbuf+c);
			else if (strcmp(readbuf, "ultrasonic")==0)
				strcpy(obsavd_device, readbuf+c);
			else if (strcmp(readbuf, "mpu6050")==0)
				strcpy(inertial_device, readbuf+c);
		}
		sprintf(ctrl_server, "%s:%s", ip, ctrl_port);
		sprintf(data_server, "%s:%s", ip, data_port);
		sprintf(route_server, "%s:%s", ip, route_port);
		free(route_port);
		free(data_port);
		free(ctrl_port);
		free(readbuf);
		printf("inited from file: %s.\n", filename);
		printf("------------networking-------------\n");
		printf("\tctrl: \t%s.\n", ctrl_server);
		printf("\tdata: \t%s.\n", data_server);
		printf("\troute: \t%s.\n", route_server);
		printf("--------------device---------------\n");
		printf("\tgps: \t%s.\n", gps_device);
		printf("\tapm: \t%s.\n", apm_device);
		printf("\tobsavd:\t%s.\n", obsavd_device);
		printf("\tmpu6050:\t%s.\n", inertial_device);
	}
}
