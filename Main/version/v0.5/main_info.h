#ifndef _main_info_H_
#define _main_info_H_
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <linux/types.h>
#include <sys/types.h>

#include "navigation/gps_updtr.h"
#include "navigation/comm_apm.h"
#include "navigation/compass.h"
#include "navigation/mpu6050.h"
#include "navigation/inertial.h"
#include "navigation/protocol.h"

#include "networking/ctrl.h"
#include "networking/data.h"

#include "wrapper/io.h"
#include "wrapper/ipc.h"
#include "wrapper/proc.h"
#include "wrapper/socket.h"
#include "wrapper/threading.h"

#define IP				"192.168.1.124"
#define CTRL_PORT			(10001)
#define DATA_PORT			(10002)
#define ROUTE_PORT			(10003)

#define CTRL_SERVER			"192.168.1.124:10001"
#define DATA_SERVER			"192.168.1.124:10002"
#define ROUTE_SERVER			"192.168.1.124:10003"

#define GPS_DEVICE			"/dev/ttyUSB0"
#define APM_DEVICE			"/dev/ttyUSB1"
#define OBSAVD_DEVICE			"/dev/ttyUSB2"

#define INERTIAL_DEVICE			"/dev/i2c-1"

#define mission_num(list)		(sizeof(list)/sizeof(struct mission))

#define SERVER_SIG			(34)
#define ROUTE_SIG			(35)
#define OBSAVD_SIG			(36)

#define SHR_PAGES			(2)
struct shr_block{
	sem_t gps_sem;
	struct gps_struct gps;

	data_buf_t to_send;
};

extern struct shr_block *shrmem;

extern struct mission mission_list[];

extern char ctrl_server[];
extern char data_server[];
extern char route_server[];

extern char gps_device[];
extern char apm_device[];
extern char obsavd_device[];
extern char inertial_device[];

int apm_agent(void *args);
int attitude(void *args);
int gps_agent(void *args);
int net_agent(void *args);
int obsavd(void *args);

#endif
