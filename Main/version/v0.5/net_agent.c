#include "main_info.h"

static pthread_t ctrl_thrd;
static pthread_t data_thrd;
static pthread_t route_thrd;

static data_args_t data_args;
static ctrl_args_t ctrl_args;

int net_agent(void *args){
	void *retval;

#ifndef __DEBUG_NO_DATA
	data_args.data=&shrmem->to_send;
	data_args.server_info=data_server;

	printf("net agent: data threading.\n");

	if (wrap_thrd_create(&data_thrd, data_plane_start, (void *)&data_args)){
		fprintf(stderr, "create thread error!\n");
		return -1;
	}
#endif
#ifndef __DEBUG_NO_CTRL
	ctrl_args.server_info=ctrl_server;
	ctrl_args.ctrl_proc=mission_list[0].pid;
	ctrl_args.sig=SERVER_SIG;

	printf("net agent: ctrl:\n");
	printf("\rpid: %d, sig: %d\n", ctrl_args.ctrl_proc, ctrl_args.sig);

	if (wrap_thrd_create(&ctrl_thrd, ctrl_plane_start, (void *)&ctrl_args)){
		fprintf(stderr, "create thread error!\n");
		return -1;
	}
#endif
#ifndef __DEBUG_NO_ROUT

#endif

#ifndef __DEBUG_NO_ROUT

#endif
#ifndef __DEBUG_NO_CTRL
	wrap_thrd_join(ctrl_thrd, &retval);
	printf("return value:\t%d.\n", (int)retval);
#endif
#ifndef __DEBUG_NO_DATA
	wrap_thrd_join(data_thrd, &retval);
	printf("return value:\t%d.\n", (int)retval);
#endif
	return 0;
}
