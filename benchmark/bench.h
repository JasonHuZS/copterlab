#ifndef _bench_H_
#define _bench_H_

#include <sys/times.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>

/*
 *		BENCHMARK FRAMEWORK INSTRUCTION
 * to use this framework, two macros have to be defined: 
 * 		BENCH_LOOP, BENCH_BODY()
 * where BENCH_LOOP is the times of the loop doing the BENCH_BODY(),
 * and BENCH_BODY() is the actual body that is done by the loop for 
 * BENCH_LOOP times.
 *
 * after defining these macros, at the last line of the c file, declare 
 * 		BENCH_MAIN()
 * noticing there is no semi-column(;) follows.
 */

struct tms tms_o,tms_n;
struct timespec ts_o,ts_n;
clock_t times_o,times_n;
clock_t clock_o,clock_n;
time_t time_o,time_n;

#define BENCH_MAIN()	\
int main() {\
	long ticks=sysconf(_SC_CLK_TCK);\
	int i;\
	times_o=times(&tms_o);\
	if (errno==EFAULT){\
		perror("times fault");\
		return -1;\
	}\
	printf("times result:\n");\
	printf("\tclock_t: \t%d\n",times_o);\
	printf("\ttms_utime: \t%d\n",tms_o.tms_utime);\
	printf("\ttms_stime: \t%d\n",tms_o.tms_stime);\
	printf("\ttms_cutime: \t%d\n",tms_o.tms_cutime);\
	printf("\ttms_cstime: \t%d\n",tms_o.tms_cstime);\
	clock_o=clock();\
	printf("clock result:\n");\
	printf("\tclock_t: \t%d\n",clock_o);\
	time_o=time(NULL);\
	printf("time result:\n");\
	printf("\ttime_t: \t%d\n",time_o);\
	if(clock_gettime(CLOCK_REALTIME, &ts_o)){\
		perror("gettime error");\
		return -2;\
	}\
	printf("gettime:\n");\
	printf("\ttv_sec: \t%d\n",ts_o.tv_sec);\
	printf("\ttv_nsec: \t%d\n\n",ts_o.tv_nsec);\
\
	for(i=0;i<BENCH_LOOP;i++)\
		BENCH_BODY();\
	\
	times_n=times(&tms_n);\
	if (errno==EFAULT){\
		perror("times fault");\
		return -1;\
	}\
	printf("times result:\n");\
	printf("\tclock_t: \t%d\n",times_n);\
	printf("\ttms_utime: \t%d\n",tms_n.tms_utime);\
	printf("\ttms_stime: \t%d\n",tms_n.tms_stime);\
	printf("\ttms_cutime: \t%d\n",tms_n.tms_cutime);\
	printf("\ttms_cstime: \t%d\n",tms_n.tms_cstime);\
	clock_n=clock();\
	printf("clock result:\n");\
	printf("\tclock_t: \t%d\n",clock_n);\
	time_n=time(NULL);\
	printf("time result:\n");\
	printf("\ttime_t: \t%d\n\n",time_n);\
	if(clock_gettime(CLOCK_REALTIME, &ts_n)){\
		perror("gettime error");\
		return -2;\
	}\
	printf("gettime:\n");\
	printf("\ttv_sec: \t%d\n",ts_n.tv_sec);\
	printf("\ttv_nsec: \t%d\n\n",ts_n.tv_nsec);\
\
	printf("static information:\n");\
	printf("wal-clock time: %lf\n",(double)(times_n-times_o)/ticks);\
	printf("\twal-clock: %lf seconds\n",ts_n.tv_sec-ts_o.tv_sec+(double)(ts_n.tv_nsec-ts_o.tv_nsec)/1000000000);\
	printf("\tuser time:\t\t%lf\n",(double)(tms_n.tms_utime-tms_o.tms_utime)/ticks);\
	printf("\tsys time:\t\t%lf\n",(double)(tms_n.tms_stime-tms_o.tms_stime)/ticks);\
	printf("\tchildren user time:\t%lf\n",(double)(tms_n.tms_cutime-tms_o.tms_cutime)/ticks);\
	printf("\tchildren sys time:\t%lf\n",(double)(tms_n.tms_cstime-tms_o.tms_cstime)/ticks);\
	printf("process clock: %lf\n",(double)(clock_n-clock_o)/CLOCKS_PER_SEC);\
	printf("times: %d in seconds\n",(time_n-time_o));\
	printf("efficiency: %lf seconds per BENCH_BODY()\n",(double)(clock_n-clock_o)/CLOCKS_PER_SEC/BENCH_LOOP);\
\
	return 0;\
}


#endif
