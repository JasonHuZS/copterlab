#include <stdio.h>
#include <stdlib.h>

#include "ds/list.h"

inline struct dlist *alloc_list_head(){
	struct dlist *res=malloc(sizeof(struct dlist));
	if(res==NULL) return NULL;
	res->prev=res;
	res->next=res;
	res->obj=NULL;
	return res;
}

inline struct dlist *alloc_list(void *obj){
	struct dlist *res=malloc(sizeof(struct dlist));
	if (res==NULL) return NULL;
	res->prev=NULL;
	res->next=NULL;
	res->obj=obj;
	return res;
}
inline void destory_list(struct dlist *link){
	if (link!=NULL) free(link);
}

inline struct dlist *prev_list(struct dlist *link){
	return link->prev;
}
inline struct dlist *next_list(struct dlist *link){
	return link->next;
}

inline void add_list(struct dlist *list, struct dlist *link){
	link->next=list->next;
	link->prev=list;
	list->next=link;
}
inline void del_list(struct dlist *link){
	if (link->prev!=link&&link->prev!=NULL){
		link->prev->next=link->next;
		link->next->prev=link->prev;
		link->prev=link->next=NULL;
	}else if (link->prev==link){
		link->prev=link->next=NULL;
	}	
}

void append_list(struct dlist *pos, void *obj){
	struct dlist *res=alloc_list(obj);
	if (res==NULL) return;
	add_list(pos, res);
}

void eliminate_list(struct dlist *head){
	struct dlist *tmp=head->next;
	free(head);
	for(;tmp!=head;tmp->next)
		free(head);
}
