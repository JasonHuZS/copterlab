#include <stdio.h>
#include <stdlib.h>
#include "ds/rbtree.h"

static rbtree_node_t nil={
	.left=NULL,
	.right=NULL,
	.parent=NULL,
	.key=0,
	.val=NULL,
	._color='b'
};
static rbtree_node_t *nil_p=&nil;

inline rbtree_node_t *get_nil(){
	return nil_p;
}

rbtree_t *alloc_tree(rbtree_node_t *root){
	rbtree_t *tree=malloc(sizeof(rbtree_t));
	if (tree=NULL)
		return NULL;
	tree->root=root;
	return tree;
}
inline void free_tree(rbtree_t *tree){
	free(tree);
}
void traverse_free(rbtree_node_t *node){
	if (node->left!=nil_p)
		traverse_free(node->left);
	if (node->right!=nil_p)
		traverse_free(node->right);
	free(node);
	return;
}
void recur_free_tree(rbtree_t *tree){
	traverse_free(tree->root);
	free(tree);
}

rbtree_node_t *alloc_node(long key, void *val){
	rbtree_node_t *node=malloc(sizeof(rbtree_node_t));
	if (node==NULL)
		return nil_p;
	node->left=nil_p;
	node->right=nil_p;
	node->parent=nil_p;
	node->_color='b';
	node->key=key;
	node->val=val;
	return node;
}
inline void free_node(rbtree_node_t *node){
	free(node);
}

inline void static_init_node(rbtree_node_t *node, long key, void *val){
	if (node==nil_p||node==NULL)
		return;
	node->left=nil_p;
	node->right=nil_p;
	node->parent=nil_p;
	node->_color='b';
	node->key=key;
	node->val=val;
	return;
}

rbtree_node_t *minimum(rbtree_node_t *node){
	while (node->left!=nil_p)
		node=node->left;
	return node;
}
rbtree_node_t *maximum(rbtree_node_t *node){
	while (node->right!=nil_p)
		node=node->right;
	return node;
}

rbtree_node_t *tree_search(rbtree_node_t *node, long key){
	if (node==nil_p || key==node->key)
		return node;
	if (key<node->key)
		return tree_search(node->left, key);
	else
		return tree_search(node->right, key);
}
rbtree_node_t *tree_iter_search(rbtree_node_t *node, long key){
	rbtree_node_t *x=node;
	while (x!=nil_p && key!=x->key)
		if (key<x->key)
			x=x->left;
		else
			x=x->right;
	return x;
}

rbtree_node_t *tree_successor(rbtree_node_t *node){
	rbtree_node_t *x=node;
	if (x->right!=nil_p)
		return minimum(x->right);
	rbtree_node_t *y=x->parent;
	while (y!=nil_p && x==y->right){
		x=y;
		y=y->parent;
	}
	return y;
}
rbtree_node_t *tree_predecessor(rbtree_node_t *node){
	rbtree_node_t *x=node;
	if (x->left!=nil_p)
		return maximum(x->left);
	rbtree_node_t *y=x->parent;
	while (y!=nil_p && x==y->left){
		x=y;
		y=y->parent;
	}
	return y;
}

void left_rotate(rbtree_t *tree, rbtree_node_t *x){
	rbtree_node_t *y=x->right;
	x->right=y->left;
	if (y->left!=nil_p)
		y->left->parent=x;
	y->parent=x->parent;
	if (x->parent==nil_p)
		tree->root=y;
	else if (x==x->parent->left)
		x->parent->left=y;
	else
		x->parent->right=y;
	y->left=x;
	x->parent=y;
	return;
}
void right_rotate(rbtree_t *tree, rbtree_node_t *y){
	rbtree_node_t *x=y->left;
	y->left=x->right;
	if (x->right!=nil_p)
		x->right->parent=y;
	x->parent=y->parent;
	if (y->parent==nil_p)
		tree->root=x;
	else if (y==y->parent->right)
		y->parent->right=x;
	else
		y->parent->left=x;
	x->right=y;
	y->parent=x;
	return;
}

static void tree_ifixup(rbtree_t *tree, rbtree_node_t *z){
	rbtree_node_t *x,*y;
	while (z->parent->_color=='r'){
		if (z->parent==z->parent->parent->left){
			y=z->parent->parent->right;
			if (y->_color=='r'){
				z->parent->_color='b';
				y->_color='b';
				z->parent->parent->_color='r';
				z=z->parent->parent;
			}
			else{
				if (z==z->parent->right){
					z=z->parent;
					left_rotate(tree, z);
				}
				z->parent->_color='b';
				z->parent->parent->_color='r';
				right_rotate(tree, z->parent->parent);
			}
		}
		else{
			y=z->parent->parent->left;
			if (y->_color=='r'){
				z->parent->_color='b';
				y->_color='b';
				z->parent->parent->_color='r';
				z=z->parent->parent;
			}
			else{
				if (z==z->parent->left){
					z=z->parent;
					right_rotate(tree, z);
				}
				z->parent->_color='b';
				z->parent->parent->_color='r';
				left_rotate(tree, z->parent->parent);
			}
		}
	}
	tree->root->_color='b';
	return;
}
void tree_insert(rbtree_t *tree, rbtree_node_t *z){
	rbtree_node_t *x,*y;
	y=nil_p;
	x=tree->root;
	while (x!=nil_p){
		y=x;
		if (z->key<x->key)
			x=x->left;
		else
			x=x->right;
	}
	z->parent=y;
	if (y==nil_p)
		tree->root=z;
	else if (z->key<y->key)
		y->left=z;
	else
		y->right=z;
	z->left=z->right=nil_p;
	z->_color='r';
	tree_ifixup(tree, z);
	return;
}

static void transplant(rbtree_t *tree, rbtree_node_t *u, rbtree_node_t *v){
	if (u->parent==nil_p)
		tree->root=v;
	else if (u==u->parent->left)
		u->parent->left=v;
	else
		u->parent->right=v;
	v->parent=u->parent;
	return;
}

static void tree_dfixup(rbtree_t *tree, rbtree_node_t *x){
	rbtree_node_t *w;
	while (x!=tree->root && x->_color=='b'){
		if (x==x->parent->left){
			w=x->parent->right;
			if (w->_color=='r'){
				w->_color='b';
				x->parent->_color='r';
				left_rotate(tree, x->parent);
				w=x->parent->right;
			}
			if (w->left->_color=='b' && w->right->_color=='b'){
				w->_color='r';
				x=x->parent;
			}
			else {
				if (w->right->_color=='b'){
					w->left->_color='b';
					w->_color='r';
					right_rotate(tree, w);
					w=x->parent->right;
				}
				w->_color=x->parent->_color;
				x->parent->_color='b';
				w->right->_color='b';
				left_rotate(tree, x->parent);
				x=tree->root;
			}
		}
		else {
			w=x->parent->left;
			if (w->_color=='r'){
				w->_color='b';
				x->parent->_color='r';
				right_rotate(tree, x->parent);
				w=x->parent->right;
			}
			if (w->left->_color=='b' && w->right->_color=='b'){
				w->_color='r';
				x=x->parent;
			}
			else {
				if (w->left->_color=='b'){
					w->right->_color='b';
					w->_color='r';
					left_rotate(tree, w);
					w=x->parent->left;
				}
				w->_color=x->parent->_color;
				x->parent->_color='b';
				w->left->_color='b';
				right_rotate(tree, x->parent);
				x=tree->root;
			}
		}
	}
	x->_color='b';
	return;
}

void tree_delete(rbtree_t *tree, rbtree_node_t *z){
	rbtree_node_t *x,*y;
	char y_color;
	y=z;
	y_color=y->_color;
	if (z->left==nil_p){
		x=z->right;
		transplant(tree, z, z->right);
	}
	else if (z->right==nil_p){
		x=z->left;
		transplant(tree, z, z->left);
	}
	else {
		y=minimum(z->right);
		y_color=z->_color;
		x=y->right;
		if (y->parent==z){
			x->parent=y;
		}
		else {
			transplant(tree, y, y->right);
			y->right=z->right;
			y->right->parent=y;
		}
		transplant(tree, z, y);
		y->left=z->left;
		y->left->parent=y;
		y->_color=z->_color;
	}
	if (y_color=='b')
		tree_dfixup(tree, x);
	return;
}

static int __tree_bheight(rbtree_node_t *node){
	if (node==nil_p)
		return 1;
	if (node->_color=='r'){
		int bh1,bh2;
		bh1=__tree_bheight(node->left);
		bh2=__tree_bheight(node->right);
		if (bh1!=bh2)
			return -1;
		else
			return bh1;
	}
	else {
		int bh1,bh2;
		bh1=__tree_bheight(node->left);
		bh2=__tree_bheight(node->right);
		if (bh1!=bh2)
			return -1;
		else
			return bh1+1;
	}
}
int tree_bheight(rbtree_t *tree){
	return __tree_bheight(tree->root);
}

static int __tree_height(rbtree_node_t *node){
	if (node==nil_p)
		return 1;
	int h1,h2;
	h1=__tree_height(node->left);
	h2=__tree_height(node->right);
	return 1+(h1>h2?h1:h2);
}
int tree_height(rbtree_t *tree){
	return __tree_height(tree->root);
}
