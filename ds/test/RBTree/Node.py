class Node:
    def __init__(self, key=None, val=None, color='b', left=None, right=None, parent=None):
        self.left=left
        self.right=right
        self.parent=parent
        self.key=key
        self.val=val
        self.color=color

    def __str__(self):
        if self==Node.NIL:
            return "NIL"
        else:
            return "key: "+str(self.key)+"\n"+\
                   "val: "+str(self.val)+"\n"+\
                   "color: "+str(self.color)+"\n"+\
                   "left: "+str(self.left)+"\n"+\
                   "right: "+str(self.right)+"\n"
    
    def set_left(self, left):
        self.left=left
        return self.left
    def set_right(self, right):
        self.right=right
        return self.right
    def set_parent(self, parent):
        self.parent=parent
        return self.parent
    def set_kv(self, key, val):
        self.key=key
        self.val=val
        return self

Node.NIL=Node()
