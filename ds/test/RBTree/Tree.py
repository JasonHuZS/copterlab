if __name__=='__main__':
    from Node import *
else:
    from .Node import *

class Tree:
    NIL=Node.NIL
    root=None
    
    def __init__(self, root=NIL):
        self.root=root
        self.root.parent=self.NIL
        self.root.left=self.root.right=self.NIL
    
    def __str__(self):
        return str(self.root)
    
    def minimum(tree, node=root):
        while node.left!=tree.NIL:
            node=node.left
        return node
    def maximum(tree,node=root):
        while node.right!=tree.NIL:
            node=node.right
        return node

    def search(tree, key, node=root):
        if node==tree.NIL or key==node.key:
            return node
        if key<node.key:
            return tree.search(node.left, key)
        else:
            return tree.search(node.right, key)
    def iter_search(tree, key, node=root):
        x=node
        while x!=tree.NIL and key!=x.key:
            if key<x.key:
                x=x.left
            else:
                x=x.right
        return x

    def successor(tree, node=root):
        x=node
        if x.right!=tree.NIL:
            return tree.minimum(x.right)
        y=x.parent
        while y!=tree.NIL and x==y.right:
            x=y
            y=y.parent
        return y
    def predecessor(tree, node=root):
        x=node
        if x.left!=tree.NIL:
            return tree.maximum(x.left)
        y=x.parent
        while y!=tree.NIL and x==y.left:
            x=y
            y=y.parent
        return y
    
    def left_rotate(tree, x):
        y=x.right
        x.right=y.left
        if y.left!=tree.NIL:
            y.left.parent=x
        y.parent=x.parent
        if x.parent==tree.NIL:
            tree.root=y
        elif x==x.parent.left:
            x.parent.left=y
        else:
            x.parent.right=y
        y.left=x
        x.parent=y
        return
    def right_rotate(tree, y):
        x=y.left
        y.left=x.right
        if x.right!=tree.NIL:
            x.right.parent=y
        x.parent=y.parent
        if y.parent==tree.NIL:
            tree.root=x
        elif y==y.parent.right:
            y.parent.right=x
        else:
            y.parent.left=x
        x.right=y
        y.parent=x
        return

    def insert(tree, z):
        y=tree.NIL
        x=tree.root
        while x!=tree.NIL:
            y=x
            if z.key<x.key:
                x=x.left
            else:
                x=x.right
        z.parent=y
        if y==tree.NIL:
            tree.root=z
        elif z.key<y.key:
            y.left=z
        else:
            y.right=z
        z.left=tree.NIL
        z.right=tree.NIL
        z.color='r'
        tree.ifix_up(z)
        return tree
    def ifix_up(tree, z):
        while z.parent.color=='r':
            if z.parent==z.parent.parent.left:
                y=z.parent.parent.right
                if y.color=='r':
                    z.parent.color='b'
                    y.color='b'
                    z.parent.parent.color='r'
                    z=z.parent.parent
                else:
                    if z==z.parent.right:
                        z=z.parent
                        tree.left_rotate(z)
                    z.parent.color='b'
                    z.parent.parent.color='r'
                    tree.right_rotate(z.parent.parent)
            else:
                y=z.parent.parent.left
                if y.color=='r':
                    z.parent.color='b'
                    y.color='b'
                    z.parent.parent.color='r'
                    z=z.parent.parent
                else:
                    if z==z.parent.left:
                        z=z.parent
                        tree.right_rotate(z)
                    z.parent.color='b'
                    z.parent.parent.color='r'
                    tree.left_rotate(z.parent.parent)
        tree.root.color='b'
        return
    __iadd__=insert

    def transplant(tree, u, v):
        if u.parent==tree.NIL:
            tree.root=v
        elif u==u.parent.left:
            u.parent.left=v
        else:
            u.parent.right=v
        v.parent=u.parent
        return
    def delete(tree, z):
        y=z
        y_color=y.color
        if z.left==tree.NIL:
            x=z.right
            tree.transplant(z, z.right)
        elif z.right==tree.NIL:
            x=z.left
            tree.transplant(z, z.left)
        else:
            y=tree.minimum(z.right)
            y_color=y.color
            x=y.right
            if y.parent==z:
                x.parent=y
            else:
                tree.transplant(y, y.right)
                y.right=z.right
                y.right.parent=y
            tree.transplant(z, y)
            y.left=z.left
            y.left.parent=y
            y.color=z.color
        if y_color=='b':
            tree.dfix_up(x)
        return tree
    def dfix_up(tree, x):
        while x!=tree.root and x.color=='b':
            if x==x.parent.left:
                 w=x.parent.right
                 if w.color=='r':
                    w.color='b'
                    x.parent.color='r'
                    tree.left_rotate(x.parent)
                    w=x.parent.right
                 if w.left.color=='b' and w.right.color=='b':
                    w.color='r'
                    x=x.parent
                 else:
                    if w.right.color=='b':
                        w.left.color='b'
                        w.color='r'
                        tree.right_rotate(w)
                        w=x.parent.right
                    w.color=x.parent.color
                    x.parent.color='b'
                    w.right.color='b'
                    tree.left_rotate(x.parent)
                    x=tree.root
            else:
                w=x.parent.left
                if w.color=='r':
                    w.color='b'
                    x.parent.color='r'
                    tree.right_rotate(x.parent)
                    w=x.parent.left
                if w.left.color=='b' and w.right.color=='b':
                    w.color='r'
                    x=x.parent
                else:
                    if w.left.color=='b':
                        w.right.color='b'
                        w.color='r'
                        tree.left_rotate(w)
                        w=x.parent.left
                    w.color=x.parent.color
                    x.parent.color='b'
                    w.left.color='b'
                    tree.right_rotate(x.parent)
                    x=tree.root
        x.color='b'
        return
    __isub__=delete

if __name__=='__main__':
    node=[Node.NIL]
    for i in range(1,10):
        node.append(Node(i,i*100))
    tree=Tree(node[1])
    print("1:\n"+str(tree))
    
    tree.insert(node[2])
    tree.insert(node[3])
    tree.insert(node[4])
    tree.insert(node[5])
    print("2:\n"+str(tree))

    tree.insert(node[6])
    tree.insert(node[7])
    tree.insert(node[8])
    tree.insert(node[9])
    print("3:\n"+str(tree))

    tree.delete(node[1])
    print("4: \n"+str(tree))
