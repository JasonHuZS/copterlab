class dlist:
	def __init__(self, obj):
		self.prev=None
		self.back=None
		self.obj=obj

	def __str__(self):
		st=str(self.obj)+' '
		tmp=self.back
		while tmp!=self:
			st+=str(tmp.obj)+' '
			tmp=tmp.back
		return st

	def headize(self):
		self.prev=self
		self.back=self
		return self

	def add(self, link):
		link.back=self.back
		link.prev=self
		self.back=link

	def delete(self):
		if self.prev!=self and self.prev!=None:
			self.prev.back=self.back
			self.back.prev=self.prev
			self.prev=self.back=None
		elif self.prev==self:
			self.prev=self.back=None
		
	def append(self, obj):
		self.add(dlist(obj))

def test():
	a=dlist(100).headize()
	print(a)
	b=dlist(110)
	a.add(b)
	print(a)
	b.append(120)
	print(a)

if __name__=='__main__':
	test()
