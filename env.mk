ifndef ARCH
ARCH=ARM
endif
ifndef CROSS_COMPILE
CROSS_COMPILE=arm-linux-gnueabihf-
endif
ifndef KERNEL_DIR
KERNEL_DIR=~/src/linux-sunxi/
endif
PROJECT_DIR=$(shell pwd)
INC_DIR=${PROJECT_DIR}/include
SCRIPT_DIR=${PROJECT_DIR}/script
STALIB_DIR=${PROJECT_DIR}/target/staticlib

CC=${CROSS_COMPILE}gcc
LD=${CROSS_COMPILE}gcc
AR=${CROSS_COMPILE}ar

ARFLAGS=crs
CFLAGS=-I${INC_DIR}
RM_FLAGS=-rf .tmp_versions *.o *.ko *.mod.c modules.order Module.symvers .*.cmd

defines=$(shell cat Main/.config | sed -n -e 's/^\(__DEBUG.*\)/-D\1/p')
CFLAGS+=$(defines)

export ARCH
export CROSS_COMPILE
export KERNEL_DIR
export PROJECT_DIR
export INC_DIR
export SCRIPT_DIR
export STALIB_DIR

export CC
export LD
export AR

export ARFLAGS
export CFLAGS
export RM_FLAGS
