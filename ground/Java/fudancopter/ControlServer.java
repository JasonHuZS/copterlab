/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fudancopter;

import java.io.*;
import fudancopter.Server.WrapSocket;
import fudancopter.DataType.ByteUtil;
import fudancopter.DataType.Protocol;

/**
 *
 * @author hu
 */
public class ControlServer implements Runnable {
    private static ControlServer cserver=null;
    private WrapSocket sock;
    private final int port;
    
    public ControlServer(){
        port=10001;
    }
    public ControlServer(int p){
        port=p;
    }
    
    public static WrapSocket getSocket(){
        return cserver.sock;
    }
    
    public static boolean sendCmd(int instr, int val){
        if (getSocket().isConnected()){
            int cmd=Protocol.protocolCmd(Protocol.SERVER_AGENT, instr, val);
            byte[] cmd_stream=ByteUtil.getBytes(cmd);
            try{
                getSocket().send(cmd_stream);
                return true;
            }
            catch (IOException e){
                return false;
            }
        }
        return false;
    }
    
    public static boolean setMode(int mode){
        if (mode<1 || mode>6){
            return false;
        }
        return sendCmd(Protocol.MODE, mode);
    }
    
    public static boolean launch(){
        return sendCmd(Protocol.LAUNCH, 0);
    }
    
    public static boolean land(){
        return sendCmd(Protocol.LAND, 0);
    }
    
    public static boolean urgentLand(){
        return sendCmd(Protocol.URGENT_LAND, 0);
    }
    
    @Override
    public void run(){
        cserver=this;
        try{
            sock=new WrapSocket(port);
        }
        catch (IOException e){
            System.err.println("Socket creation error: port "+port);
            return;
        }
        while (true){
            try {
                sock.accept();
            }
            catch (IOException e){
                MainFrame.getFrame().setCtrlDisconnected();
                continue;
            }
            MainFrame gpsf=MainFrame.getFrame();    
            gpsf.setCtrlConnected();
            
            while (sock.isConnected()){}
            gpsf.setCtrlDisconnected();
        }
    }
    
}
