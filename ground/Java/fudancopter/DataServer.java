/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fudancopter;

import java.io.*;
import fudancopter.Server.WrapSocket;
import java.util.*;

/**
 *
 * @author hu
 */
public class DataServer implements Runnable {
    public static final byte[] GPS={0x47, 0x50, 0x53};
    private final int port;
    
    public DataServer(){
        port=10000;
    }
    
    public DataServer(int p){
        port=p;
    }
    
    @Override
    public void run(){
        WrapSocket sock;
        try {
            sock=new WrapSocket(port);
        }
        catch(IOException e){
            System.err.println("Socket creation error: port "+port);
            return;
        }
        
        while (true){
            try {
                sock.accept();
            }
            catch (IOException e){
                MainFrame.getFrame().setDataDisconnected();
                continue;
            }
            MainFrame gpsf=MainFrame.getFrame();
            gpsf.setDataConnected();
            
            byte[] tag;
            byte[] info;
            while (sock.isConnected()){
                try {
                    tag=sock.recv(3);
                    if (Arrays.equals(tag, GPS)){
                        info=sock.recv(64);
                        gpsf.setGPS(fudancopter.DataType.GPS.parseBytes(info));
                    }
                }
                catch (IOException e){
                    break;
                }
            }
            gpsf.setDataDisconnected();
        }
    }
}
