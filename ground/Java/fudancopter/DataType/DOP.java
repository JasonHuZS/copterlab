/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fudancopter.DataType;

/**
 *
 * @author hu
 */
public class DOP{
    public float hdop;
    public float vdop;
    public float pdop;
    
    public DOP(){
        hdop=vdop=pdop=0.0f;
    }
    
    public DOP(float ihdop, float ivdop, float ipdop){
        hdop=ihdop;
        vdop=ivdop;
        pdop=ipdop;
    }
}