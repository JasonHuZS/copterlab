/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fudancopter.DataType;

/**
 *
 * @author hu
 */

public class GPS {
    public Position pos;
    public DOP dop;
    public String altitude;
    public short satellites;
    public String speed;
    public String avail;
    
    public GPS(Position ipos, String alt, DOP idop, short sate, String spd){
        pos=ipos;
        dop=idop;
        altitude=alt;
        satellites=sate;
        speed=spd;
        avail="Invalid";
    }
    
    public GPS(Position ipos, String alt, DOP idop, short sate, String spd, char av){
        this(ipos,alt,idop,sate,spd);
        if (Character.toUpperCase(av)=='V'){
            avail="Invalid";
        }
        else{
            avail="Valid";
        }
    }
    
    public static GPS parseBytes(byte[] stream){
        int latd=ByteUtil.getInt(ByteUtil.subBytes(stream, 8, 4));
        int latm=ByteUtil.getInt(ByteUtil.subBytes(stream, 12, 4));
        int lond=ByteUtil.getInt(ByteUtil.subBytes(stream, 16, 4));
        int lonm=ByteUtil.getInt(ByteUtil.subBytes(stream, 20, 4));
        char ns=(char)stream[24];
        char ew=(char)stream[25];
        float alt=ByteUtil.getFloat(ByteUtil.subBytes(stream, 28, 4));
        float pdop=ByteUtil.getFloat(ByteUtil.subBytes(stream, 32, 4));
        float hdop=ByteUtil.getFloat(ByteUtil.subBytes(stream, 36, 4));
        float vdop=ByteUtil.getFloat(ByteUtil.subBytes(stream, 40, 4));
        float spd=ByteUtil.getFloat(ByteUtil.subBytes(stream, 44, 4));
        char alu=(char)stream[60];
        char stat=(char)stream[61];
        short sate=(short)stream[62];
        char spu=(char)stream[63];
        
        String spus="km/hr";
        if (spu=='N'){
            spus="knots";
        }
        
        GPS gps=new GPS(new Position(new Degree(lond, lonm/10000),ew,
                new Degree(latd, latm/10000), ns),
        String.valueOf(alt)+alu,
        new DOP(hdop, vdop, pdop),
        sate,
        String.valueOf(spd)+spus,
        stat);
        
        return gps;
    }
}
