/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fudancopter.DataType;

/**
 *
 * @author hu
 */
public class Position{
    public Degree longitude;
    public char ew;
    public Degree latitude;
    public char ns;
    
    public Position(){
        longitude=new Degree();
        ew='E';
        latitude=new Degree();
        ns='N';
    }
    
    public Position(Degree lon, char ewi, Degree lat, char nsi){
        longitude=lon;
        ew=Character.toUpperCase(ewi);
        latitude=lat;
        ns=Character.toUpperCase(nsi);
    }
}