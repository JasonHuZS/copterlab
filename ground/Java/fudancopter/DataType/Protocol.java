/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fudancopter.DataType;

/**
 *
 * @author hu
 */
public final class Protocol {
    public static final int SERVER_AGENT=(0x1)<<29;
    
    public static final int YAW=(0x1)<<25;
    public static final int ROLL=(0x2)<<25;
    public static final int PITCH=(0x3)<<25;
    public static final int THROTTLE=(0x4)<<25;
    public static final int MODE=(0x5)<<25;
    
    public static final int LAUNCH=(0xc)<<25;
    public static final int LAND=(0xd)<<25;
    public static final int URGENT_LAND=(0xe)<<25;
    
    public static final int RELATED=(0x1)<<24;
    
    public static final int NUMBER_BITS=0xfff;
    
    public static final int protocolCmd(int agent, int instr, int val){
        return agent+instr+(val&NUMBER_BITS);
    }
}
