/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fudancopter;

import fudancopter.Server.WrapSocket;
import java.io.*;

/**
 *
 * @author FORHU
 */
public class PathServer implements Runnable {
    private int port;
    
    public PathServer(){
        port=10002;
    }
    
    public PathServer(int p){
        port=p;
    }
    
    @Override
    public void run(){
        WrapSocket sock;
        try{
            sock=new WrapSocket(port);
        }
        catch (IOException e){
            System.err.println("Socket creation error: port "+port);
            return;
        }
        
        while (true){
            try{
                sock.accept();
            }
            catch (IOException e){
                continue;
            }
            
            while(sock.isConnected()){
                //do path routing
                //send path
            }
        }
    }
    
}
