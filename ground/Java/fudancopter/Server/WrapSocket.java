/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fudancopter.Server;

import java.net.*;
import java.io.*;

/**
 *
 * @author hu
 */
public class WrapSocket {
        private final ServerSocket sock;
        private Socket client=null;
    
    public WrapSocket(int port) throws IOException {
        sock=new ServerSocket(port);
        client=null;
    }
    
    public void accept() throws IOException {
        client=sock.accept();
    }
    
    public byte[] recv(int len) throws IOException {
        int total=0;
        byte[] reb=new byte[len];
        ByteArrayOutputStream buf=new ByteArrayOutputStream();
        InputStream is=client.getInputStream();
        while (total<len){
            int res=is.read(reb);
            if(res==-1){
                client=null;
                break;
            }
            total+=res;
            buf.write(reb, 0, res);
        }
        return buf.toByteArray();
    }
    
    public void send(byte[] buf) throws IOException {
        OutputStream os=client.getOutputStream();
        os.write(buf);
    }
    
    public boolean isConnected(){
        return client!=null&&client.isConnected();
    }
}
