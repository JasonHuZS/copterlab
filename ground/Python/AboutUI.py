from PyQt4 import QtCore, QtGui
from ui_About import *

class AboutUI(QtGui.QWidget):
    def __init__(self, parent=None):
        super(AboutUI, self).__init__(parent)

        self.ui=Ui_AboutForm()
        self.ui.setupUi(self)

        self.pic=QtGui.QPixmap('img\\fudan-logo.png')
        self.pic=self.pic.scaled(self.pic.size()/2)
        self.pic_item=QtGui.QGraphicsPixmapItem(self.pic)
        self.pic_item.setOpacity(0.15)
        self.pic_item.setPos(0,0)
        
        self.scene=QtGui.QGraphicsScene()
        self.scene.addItem(self.pic_item)

        self.ui.graphicsView.setScene(self.scene)
        self.setupText()

    def setupText(self):
        font=QtGui.QFont('Calibri')
        font.setPointSize(30)
        font.setBold(True)
        font.setItalic(True)

        fudancopter=QtGui.QGraphicsTextItem('FudanCopter')
        fudancopter.setFont(font)
        fudancopter.setPos(40,0)
        self.scene.addItem(fudancopter)

        font2=QtGui.QFont('Consolas')
        font.setPointSize(10)

        power=QtGui.QGraphicsTextItem('Powered by HuStmpHrrr')
        power.setFont(font2)
        power.setPos(150,270)
        self.scene.addItem(power)
        

if __name__=='__main__':
    import sys
    app = QtGui.QApplication([])
    ui=AboutUI()
    ui.show()
    sys.exit(app.exec_())
