from ui_Help import *
from PyQt4 import QtCore, QtGui
import sys

class HelpWindow(Ui_Form):
    def __init__(self):
        super(HelpWindow, self).__init__()

    def setupUi(self, form):
        super(HelpWindow, self).setupUi(form)
        self.showPic()
    
    def showPic(self):
        self.pic=QtGui.QPixmap('img\\xbox-gamepad.jpg')
        self.pic=self.pic.scaled(self.pic.size()/2, QtCore.Qt.KeepAspectRatio,
                        QtCore.Qt.SmoothTransformation)

        self.scene=QtGui.QGraphicsScene()
        self.scene.addPixmap(self.pic)

        self.graphicsView.setScene(self.scene)

class HelpUI(QtGui.QWidget):
    def __init__(self, parent=None):
        super(HelpUI, self).__init__(parent)

        self.ui=HelpWindow()
        self.ui.setupUi(self)
        

def showHelp():
    ui=HelpUI()
    ui.show()

if __name__ == "__main__":
    app = QtGui.QApplication([])
    ui=HelpUI()
    ui.show()
    sys.exit(app.exec_())
