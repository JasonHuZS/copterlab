import ctypes

ERROR_SUCCESS=0L
ERROR_DEVICE_NOT_CONNECTED=1167L
ERROR_EMPTY=4306L

XINPUT_FLAG_GAMEPAD=1

BATTERY_DEVTYPE_GAMEPAD=0
BATTERY_DEVTYPE_HEADSET=1

class XINPUT_BATTERY_INFORMATION(ctypes.Structure):
    _fields_=[('BatteryType', ctypes.c_byte),
              ('BatteryLevel', ctypes.c_byte)]
    BATTERY_TYPE_DISCONNECTED=0
    BATTERY_TYPE_WIRED=1
    BATTERY_TYPE_ALKALINE=2
    BATTERY_TYPE_NIMH=3
    BATTERY_TYPE_UNKNOWN=0xff
    BATTERY_LEVEL_EMPTY=0
    BATTERY_LEVEL_LOW=1
    BATTERY_LEVEL_MEDIUM=2
    BATTERY_LEVEL_FULL=3

class XINPUT_GAMEPAD(ctypes.Structure):
    _fields_=[('wButtons', ctypes.c_uint16),
             ('bLeftTrigger', ctypes.c_uint8),
             ('bRightTrigger', ctypes.c_uint8),
             ('sThumbLX', ctypes.c_int16),
             ('sThumbLY', ctypes.c_int16),
             ('sThumbRX', ctypes.c_int16),
             ('sThumbRY', ctypes.c_int16)]
    kMaps={0x0001:"XINPUT_GAMEPAD_DPAD_UP",
        0x0002:"XINPUT_GAMEPAD_DPAD_DOWN",
        0x0004:"XINPUT_GAMEPAD_DPAD_LEFT",
        0x0008:"XINPUT_GAMEPAD_DPAD_RIGHT",
        0x0010:"XINPUT_GAMEPAD_START",
        0x0020:"XINPUT_GAMEPAD_BACK",
        0x0040:"XINPUT_GAMEPAD_LEFT_THUMB",
        0x0080:"XINPUT_GAMEPAD_RIGHT_THUMB",
        0x0100:"XINPUT_GAMEPAD_LEFT_SHOULDER",
        0x0200:"XINPUT_GAMEPAD_RIGHT_SHOULDER",
        0x1000:"XINPUT_GAMEPAD_A",
        0x2000:"XINPUT_GAMEPAD_B",
        0x4000:"XINPUT_GAMEPAD_X",
        0x8000:"XINPUT_GAMEPAD_Y"
    }
    XINPUT_GAMEPAD_DPAD_UP=0x0001
    XINPUT_GAMEPAD_DPAD_DOWN=0x0002
    XINPUT_GAMEPAD_DPAD_LEFT=0x0004
    XINPUT_GAMEPAD_DPAD_RIGHT=0x0008
    XINPUT_GAMEPAD_START=0x0010
    XINPUT_GAMEPAD_BACK=0x0020
    XINPUT_GAMEPAD_LEFT_THUMB=0x0040
    XINPUT_GAMEPAD_RIGHT_THUMB=0x0080
    XINPUT_GAMEPAD_LEFT_SHOULDER=0x0100
    XINPUT_GAMEPAD_RIGHT_SHOULDER=0x0200
    XINPUT_GAMEPAD_A=0x1000
    XINPUT_GAMEPAD_B=0x2000
    XINPUT_GAMEPAD_X=0x4000
    XINPUT_GAMEPAD_Y=0x8000

class XINPUT_STATE(ctypes.Structure):
    _fields_=[('dwPacketNumber', ctypes.c_uint32),
             ('Gamepad', XINPUT_GAMEPAD)]

class XINPUT_VIBRATION(ctypes.Structure):
    _fields_=[("wLeftMotorSpeed", ctypes.c_ushort),
                ("wRightMotorSpeed", ctypes.c_ushort)]

class XINPUT_CAPABILITIES(ctypes.Structure):
    _fields_=[("Type", ctypes.c_byte),
              ("SubType", ctypes.c_byte),
              ("Flags", ctypes.c_uint16),
              ("Gamepad", XINPUT_GAMEPAD),
              ("Vibration", XINPUT_VIBRATION)]
    XINPUT_DEVTYPE_GAMEPAD=1
    XINPUT_CAPS_VOICE_SUPPORTED=0x4
    
class XINPUT_KEYSTROKE(ctypes.Structure):
    _fields_=[("VirtualKey", ctypes.c_uint16),
              ("Unicode", ctypes.c_wchar),
              ("Flags", ctypes.c_int16),
              ("UserIndex", ctypes.c_byte),
              ("HidCode", ctypes.c_byte)]
    Vkey={0x0000:"VK_PAD_NO",
        0x5800:"VK_PAD_A",
        0x5801:"VK_PAD_B",
        0x5802:"VK_PAD_X",
        0x5803:"VK_PAD_Y",
        0x5804:"VK_PAD_RSHOULDER",
        0x5805:"VK_PAD_LSHOULDER",
        0x5806:"VK_PAD_LTRIGGER",
        0x5807:"VK_PAD_RTRIGGER",

        0x5810:"VK_PAD_DPAD_UP",
        0x5811:"VK_PAD_DPAD_DOWN",
        0x5812:"VK_PAD_DPAD_LEFT",
        0x5813:"VK_PAD_DPAD_RIGHT",
        0x5814:"VK_PAD_START",
        0x5815:"VK_PAD_BACK",
        0x5816:"VK_PAD_LTHUMB_PRESS",
        0x5817:"VK_PAD_RTHUMB_PRESS",

        0x5820:"VK_PAD_LTHUMB_UP",
        0x5821:"VK_PAD_LTHUMB_DOWN",
        0x5822:"VK_PAD_LTHUMB_RIGHT",
        0x5823:"VK_PAD_LTHUMB_LEFT",
        0x5824:"VK_PAD_LTHUMB_UPLEFT",
        0x5825:"VK_PAD_LTHUMB_UPRIGHT",
        0x5826:"VK_PAD_LTHUMB_DOWNRIGHT",
        0x5827:"VK_PAD_LTHUMB_DOWNLEFT",

        0x5830:"VK_PAD_RTHUMB_UP",
        0x5831:"VK_PAD_RTHUMB_DOWN",
        0x5832:"VK_PAD_RTHUMB_RIGHT",
        0x5833:"VK_PAD_RTHUMB_LEFT",
        0x5834:"VK_PAD_RTHUMB_UPLEFT",
        0x5835:"VK_PAD_RTHUMB_UPRIGHT",
        0x5836:"VK_PAD_RTHUMB_DOWNRIGHT",
        0x5837:"VK_PAD_RTHUMB_DOWNLEFT"
    }
    Vflags={0x0:'XINPUT_KEYSTROKE_NOKEY',
            0x1:'XINPUT_KEYSTROKE_KEYDOWN',
            0x2:'XINPUT_KEYSTROKE_KEYUP',
            0x5:'XINPUT_KEYSTROKE_REPEAT'
    }
    
    VK_PAD_A=0x5800
    VK_PAD_B=0x5801
    VK_PAD_X=0x5802
    VK_PAD_Y=0x5803
    VK_PAD_RSHOULDER=0x5804
    VK_PAD_LSHOULDER=0x5805
    VK_PAD_LTRIGGER=0x5806
    VK_PAD_RTRIGGER=0x5807

    VK_PAD_DPAD_UP=0x5810
    VK_PAD_DPAD_DOWN=0x5811
    VK_PAD_DPAD_LEFT=0x5812
    VK_PAD_DPAD_RIGHT=0x5813
    VK_PAD_START=0x5814
    VK_PAD_BACK=0x5815
    VK_PAD_LTHUMB_PRESS=0x5816
    VK_PAD_RTHUMB_PRESS=0x5817

    VK_PAD_LTHUMB_UP=0x5820
    VK_PAD_LTHUMB_DOWN=0x5821
    VK_PAD_LTHUMB_RIGHT=0x5822
    VK_PAD_LTHUMB_LEFT=0x5823
    VK_PAD_LTHUMB_UPLEFT=0x5824
    VK_PAD_LTHUMB_UPRIGHT=0x5825
    VK_PAD_LTHUMB_DOWNRIGHT=0x5826
    VK_PAD_LTHUMB_DOWNLEFT=0x5827

    VK_PAD_RTHUMB_UP=0x5830
    VK_PAD_RTHUMB_DOWN=0x5831
    VK_PAD_RTHUMB_RIGHT=0x5832
    VK_PAD_RTHUMB_LEFT=0x5833
    VK_PAD_RTHUMB_UPLEFT=0x5834
    VK_PAD_RTHUMB_UPRIGHT=0x5835
    VK_PAD_RTHUMB_DOWNRIGHT=0x5836
    VK_PAD_RTHUMB_DOWNLEFT=0x5837

    XINPUT_KEYSTROKE_KEYDOWN=0x0001
    XINPUT_KEYSTROKE_KEYUP=0x0002
    XINPUT_KEYSTROKE_REPEAT=0x0005

xinput1_3=ctypes.windll.xinput1_3

XInputEnable=xinput1_3.XInputEnable
XInputEnable.argtypes=[ctypes.c_bool]
XInputEnable.restype=None
def xinputEnable(boolean):
    XInputEnable(boolean)

XInputGetCapabilities=xinput1_3.XInputGetCapabilities
XInputGetCapabilities.argtypes=[ctypes.c_uint16, ctypes.c_uint16,
                                ctypes.POINTER(XINPUT_CAPABILITIES)]
XInputGetCapabilities.restype=ctypes.c_uint16
def xinputGetCapabilities(indx, flags):
    capa=XINPUT_CAPABILITIES()
    res=XInputGetCapabilities(indx, flags, ctypes.byref(capa))
    return res, capa

XInputGetKeystroke=xinput1_3.XInputGetKeystroke
XInputGetKeystroke.argtypes=[ctypes.c_uint16, ctypes.c_uint16,
                             ctypes.POINTER(XINPUT_KEYSTROKE)]
XInputGetKeystroke.restype=ctypes.c_uint16
def xinputGetKeystroke(indx):
    keystrk=XINPUT_KEYSTROKE()
    res=XInputGetKeystroke(indx, 0, ctypes.byref(keystrk))
    return res, keystrk

XInputGetBatteryInformation=xinput1_3.XInputGetBatteryInformation
XInputGetBatteryInformation.argtypes=[ctypes.c_uint32,
                                      ctypes.c_byte,
                                      ctypes.POINTER(XINPUT_BATTERY_INFORMATION)]
XInputGetBatteryInformation.restype=ctypes.c_uint32
def xinputGetBatteryInformation(indx, dev):
    battery=XINPUT_BATTERY_INFORMATION()
    res=XInputGetBatteryInformation(0, 0, ctypes.byref(battery))
    return res, battery

XInputGetState=xinput1_3.XInputGetState
XInputGetState.argtypes=[ctypes.c_int32, ctypes.POINTER(XINPUT_STATE)]
XInputGetState.restype=ctypes.c_int32
def xinputGetState(indx):
    state=XINPUT_STATE()
    res=XInputGetState(0, ctypes.byref(state))
    return res, state

XInputSetState = xinput1_3.XInputSetState
XInputSetState.argtypes = [ctypes.c_uint, ctypes.POINTER(XINPUT_VIBRATION)]
XInputSetState.restype = ctypes.c_uint
def xinputSetState(indx, left_motor, right_motor):
    vibration = XINPUT_VIBRATION(int(left_motor * 65535), int(right_motor * 65535))
    res=XInputSetState(indx, ctypes.byref(vibration))
    return res

if __name__=='__main__':
    xinputSetState(0, 0.2, 0.2)

    res, state=xinputGetState(0)
    pad=state.Gamepad
    print pad.bLeftTrigger
    print pad.bRightTrigger
    print pad.sThumbLX
    print pad.sThumbLY
    print pad.sThumbRX
    print pad.sThumbRY
    print hex(pad.wButtons)

    res, battery=xinputGetBatteryInformation(0, 0)
    print battery.BatteryLevel
    print battery.BatteryType

    while True:
        res, keys=xinputGetKeystroke(0)
        print keys.Vkey[keys.VirtualKey], keys.Vflags[keys.Flags]
