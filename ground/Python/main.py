from Server import *
from Xinput import *
from PyQt4 import QtCore, QtGui
from HelpUI import *
from AboutUI import *

import time
import protocol
import struct
from ui_simpleui import Ui_MainWindow
import sys
import threading
import time

ctrl_port=10001
data_port=10002
route_port=10003
log_port=10004

sock=None
z_mid=1500
z_saved=1500

class mainWindow(QtGui.QMainWindow):
    main_ui=None
    vol_std={10.11: None, 11.0: None, 11.5: None}
    vol_tag=['empty', 'low', 'medium', 'high']
    gps_rep=['NO_GPS', 'NO_FIX', 'GPS_OK_FIX_2D', 'GPS_OK_FIX_3D']
    
    lxsig=QtCore.pyqtSignal(int)
    lysig=QtCore.pyqtSignal(int)
    rxsig=QtCore.pyqtSignal(int)
    rysig=QtCore.pyqtSignal(int)
    zsig=QtCore.pyqtSignal(int)

    statussig=QtCore.pyqtSignal(str)
    gearsig=QtCore.pyqtSignal(int)
    modesig=QtCore.pyqtSignal(int)

    spvsig=QtCore.pyqtSignal(str)
    gpssig=QtCore.pyqtSignal(str)
    logsig=QtCore.pyqtSignal(str)
    
    def __init__(self, parent=None):
        super(mainWindow, self).__init__(parent)
        mainWindow.main_ui=self
        self.ui=Ui_MainWindow()
        self.ui.setupUi(self)
        self.help_ui=HelpUI()
        self.help_ui.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.about_ui=AboutUI()
        self.about_ui.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.ui.jsWebView.load(QtCore.QUrl('about:blank'))
        
        self.ui.clearLogButton.clicked.connect(
            lambda: mainWindow.get_ui().ui.logBrowser.clear(),
            QtCore.Qt.QueuedConnection)

        #self.closeEvent=self._closeEvent
        self.ui.actionHelp.triggered.connect(self._showhelp)
        self.ui.actionAbout.triggered.connect(self._showabout)
        
        self.lxsig.connect(self._setlx, QtCore.Qt.QueuedConnection)
        self.lysig.connect(self._setly, QtCore.Qt.QueuedConnection)
        self.rxsig.connect(self._setrx, QtCore.Qt.QueuedConnection)
        self.rysig.connect(self._setry, QtCore.Qt.QueuedConnection)
        self.zsig.connect(self._setz, QtCore.Qt.QueuedConnection)

        self.statussig.connect(self._setstatus, QtCore.Qt.QueuedConnection)
        self.gearsig.connect(self._setgear, QtCore.Qt.QueuedConnection)
        self.modesig.connect(self._setmode, QtCore.Qt.QueuedConnection)
        self.spvsig.connect(self._setsupervisor, QtCore.Qt.QueuedConnection)
        self.logsig.connect(self._appendlog, QtCore.Qt.QueuedConnection)
        self.gpssig.connect(self._updategps, QtCore.Qt.QueuedConnection)

    @staticmethod
    def get_ui():
        if mainWindow.main_ui==None:
            mainWindow.main_ui=mainWindow()
        return mainWindow.main_ui

    def closeEvent(self, e):
        self.help_ui.close()
        self.close()

    def _showhelp(self):
        mainWindow.get_ui().help_ui.show()

    def _showabout(self):
        mainWindow.get_ui().about_ui.show()

    @QtCore.pyqtSlot(int)
    def _setlx(self, val):
        mainWindow.get_ui().ui.lxlabel.setText(str(val))
        mainWindow.get_ui().ui.lxSlider.setValue(val)

    @QtCore.pyqtSlot(int)
    def _setly(self, val):
        mainWindow.get_ui().ui.lylabel.setText(str(val))
        mainWindow.get_ui().ui.lySlider.setValue(val)

    @QtCore.pyqtSlot(int)
    def _setrx(self, val):
        mainWindow.get_ui().ui.rxlabel.setText(str(val))
        mainWindow.get_ui().ui.rxSlider.setValue(val)

    @QtCore.pyqtSlot(int)
    def _setry(self, val):
        mainWindow.get_ui().ui.rylabel.setText(str(val))
        mainWindow.get_ui().ui.rySlider.setValue(val)

    @QtCore.pyqtSlot(int)
    def _setz(self, val):
        mainWindow.get_ui().ui.zlabel.setText(str(val))
        mainWindow.get_ui().ui.zSlider.setValue(val)

    @QtCore.pyqtSlot(str)
    def _setstatus(self, status):
        mainWindow.get_ui().ui.statusbar.showMessage(status)

    @QtCore.pyqtSlot(int)
    def _setgear(self, val):
        mainWindow.get_ui().ui.geardial.setValue(val)
        mainWindow.get_ui().ui.gearlabel.setText(str(val))

    @QtCore.pyqtSlot(int)
    def _setmode(self, val):
        mainWindow.get_ui().ui.modedial.setValue(val)
        mainWindow.get_ui().ui.modelabel.setText(str(val))

    @QtCore.pyqtSlot(str)
    def _appendlog(self, log):
        mainWindow.get_ui().ui.logBrowser.append(log)

    @QtCore.pyqtSlot(str)
    def _setsupervisor(self, data):
        roll, pitch, yaw, arm, armrc, armcheck, auto_arm, battery, \
        mtra, mtrb, mtrc, mtrd, barometer, abd1, abd2, pres, gpres, \
        gpsstat, abd3, hdop, gpslat, gpslong, = struct.unpack(
            "hhhBBBBhHHHHhccffBcHii", data)
        
        fui=mainWindow.get_ui().ui
        fui.rollBar.setValue(roll)
        fui.pitchBar.setValue(pitch)
        fui.yawBar.setValue(yaw)

        fui.motorABar.setValue(mtra)
        fui.motorlabelA.setText(str(mtra))
        fui.motorBBar.setValue(mtrb)
        fui.motorlabelB.setText(str(mtrb))
        fui.motorCBar.setValue(mtrc)
        fui.motorlabelC.setText(str(mtrc))
        fui.motorDBar.setValue(mtrd)
        fui.motorlabelD.setText(str(mtrd))

        fui.voltageNumber.setProperty('value', float(battery)/1000)
        c=0
        for k in sorted(mainWindow.vol_std.keys()):
            if float(battery)/1000>k:
                c+=1
            else:
                break
        fui.volLabel.setText(mainWindow.vol_tag[c])
        fui.altNumber.setProperty('value', float(barometer)/1000)
        fui.presNumber.setProperty('value', pres)
        fui.gpresNumber.setProperty('value', gpres)

        fui.gpsStatusSlider.setValue(gpsstat)
        fui.gpsStatLabel.setText(mainWindow.gps_rep[gpsstat])
        fui.hdopNumber.setProperty('value', float(hdop)/100)
        fui.latNumber.setProperty('value', float(gpslat)/10000000)
        fui.longNumber.setProperty('value', float(gpslong)/10000000)

        fui.armSlider.setValue(int(arm))
        fui.rccheckSlider.setValue(int(armrc))
        fui.prearmSlider.setValue(int(armcheck))
        fui.autoarmSlider.setValue(int(auto_arm))

    @QtCore.pyqtSlot(str)
    def _updategps(self, data):
        th,tm,ts,latd,latm,lond,lonm,ns,ew,pad1,pad2,alt,pdop,hdop,vdop,\
        spd,tms,tmns,posfix,alu,stat,sate,spu=struct.unpack(
            "<HHIIIIIccccffffflllccBc", byte)

def setlx(val):
    mainWindow.get_ui().lxsig.emit(val)
    try:
        by=struct.pack('I', protocol.protocolCmd(protocol.YAW, val))
        sock.sendall(by)
    except Exception as err: pass

def setly(val):
    mainWindow.get_ui().lysig.emit(val)

def setrx(val):
    mainWindow.get_ui().rxsig.emit(val)
    try:
        by=struct.pack('I', protocol.protocolCmd(protocol.ROLL, val))
        sock.sendall(by)
    except Exception as err: pass

def setry(val):
    mainWindow.get_ui().rysig.emit(val)
    try:
        by=struct.pack('I', protocol.protocolCmd(protocol.PITCH, val))
        sock.sendall(by)
    except Exception as err: pass

def setz(val):
    global z_saved
    if val>=900 and val<=2100:
        mainWindow.get_ui().zsig.emit(val)
        z_saved=val
        try:
            by=struct.pack('I', protocol.protocolCmd(protocol.THROTTLE, val))
            sock.sendall(by)
        except Exception as err: pass

def setstatus(status):
    mainWindow.get_ui().statussig.emit(status)

def appendlog(log):
    mainWindow.get_ui().logsig.emit(log)

gear=0
mode=0

mag=[1000, 700, 300, 150]
zmag=[2, 1, 0.5, 0.3]

def get_mag(val):
        return int(float(val)/65530*mag[gear])+1500

def lz_mag(val):
        return int(float(-val)*zmag[gear])+z_mid

def rz_mag(val):
        return int(float(val)*zmag[gear])+z_mid

def setgear(val):
    if val>=0 and val<len(mag):
        global gear
        gear=val
        mainWindow.get_ui().gearsig.emit(val)

def setmode(val):
    if val>=0 and val<7:
        global mode
        mode=val
        mainWindow.get_ui().modesig.emit(val)
        try:
            by=struct.pack('I', protocol.protocolCmd(protocol.MODE, val))
            sock.sendall(by)
        except Exception as err: pass

#
# control server thread
#
def key_supervisor():
    global gear, z_mid
    pad_saved=XINPUT_GAMEPAD()
    while True:
        res, keys=xinputGetKeystroke(0)
        res, state=xinputGetState(0)
        if res==ERROR_SUCCESS:
            if keys.Flags==keys.XINPUT_KEYSTROKE_KEYDOWN:
                if keys.VirtualKey==keys.VK_PAD_DPAD_UP:
                    setgear(gear+1)
                elif keys.VirtualKey==keys.VK_PAD_DPAD_DOWN:
                    setgear(gear-1)
                elif keys.VirtualKey==keys.VK_PAD_DPAD_LEFT:
                    setmode(mode-1)
                elif keys.VirtualKey==keys.VK_PAD_DPAD_RIGHT:
                    setmode(mode+1)
                elif keys.VirtualKey==keys.VK_PAD_RSHOULDER:
                    z_mid=z_saved
                elif keys.VirtualKey==keys.VK_PAD_B:
                    setz(900)
            elif keys.VirtualKey!=0 and keys.Flags==keys.XINPUT_KEYSTROKE_REPEAT:
                if keys.VirtualKey==keys.VK_PAD_DPAD_UP:
                    setgear(gear+1)
                elif keys.VirtualKey==keys.VK_PAD_DPAD_DOWN:
                    setgear(gear-1)
                elif keys.VirtualKey==keys.VK_PAD_DPAD_LEFT:
                    setmode(mode-1)
                elif keys.VirtualKey==keys.VK_PAD_DPAD_RIGHT:
                    setmode(mode+1)
            pad=state.Gamepad
            if pad.wButtons&pad.XINPUT_GAMEPAD_LEFT_SHOULDER!=0:
                if keys.VirtualKey==keys.VK_PAD_LSHOULDER and keys.Flags==keys.XINPUT_KEYSTROKE_KEYDOWN:
                    if pad.bLeftTrigger!=0:
                        pad_saved.bLeftTrigger=pad.bLeftTrigger
                        pad_saved.bRightTrigger=0
                        setz(lz_mag(pad.bLeftTrigger))
                    elif pad.bRightTrigger!=0:
                        pad_saved.bLeftTrigger=0
                        pad_saved.bRightTrigger=pad.bRightTrigger
                        setz(rz_mag(pad.bRightTrigger))
                    else:
                        pad_saved.bLeftTrigger=0
                        pad_saved.bRightTrigger=0
                        setz(z_mid)
                elif keys.VirtualKey==keys.VK_PAD_LSHOULDER and keys.Flags==keys.XINPUT_KEYSTROKE_KEYUP:
                    pass
                else:
                    if pad_saved.bLeftTrigger!=pad.bLeftTrigger:
                        pad_saved.bLeftTrigger=pad.bLeftTrigger
                        setz(lz_mag(pad.bLeftTrigger))
                    elif pad_saved.bRightTrigger!=pad.bRightTrigger:
                        pad_saved.bRightTrigger=pad.bRightTrigger
                        setz(rz_mag(pad.bRightTrigger))
            if pad_saved.sThumbLX!=pad.sThumbLX:
                pad_saved.sThumbLX=pad.sThumbLX
                setlx(get_mag(pad.sThumbLX))
            if pad_saved.sThumbLY!=pad.sThumbLY:
                pad_saved.sThumbLY=pad.sThumbLY
                setly(get_mag(pad.sThumbLY))
            if pad_saved.sThumbRX!=pad.sThumbRX:
                pad_saved.sThumbRX=pad.sThumbRX
                setrx(get_mag(pad.sThumbRX))
            if pad_saved.sThumbRY!=pad.sThumbRY:
                pad_saved.sThumbRY=pad.sThumbRY
                setry(get_mag(pad.sThumbRY))

            time.sleep(0.015)

#
# logging server thread
#
log_sock=None
log_addr=('', log_port)

def log_server():
    global log_sock
    log_sock=ServerSocket(log_addr)
    mask=1<<31

    while True:
        c, client=log_sock.accept()
        appendlog("LOG SERVER: connected to "+str(client)+".")
        
        while True:
            try:
                recv_bytes=log_sock.recv(4)
                info_len=struct.unpack("<I", recv_bytes)
                stdout=True
                if info_len&mask!=0:
                    stdout=False
                    info_len^=mask
                recv_bytes=log_sock.recv(info_len)
                log=struct.unpack("<"+str(info_len)+"s", recv_bytes)
                head="STDOUT: " if stdout else "STDERR: "
                log=head+log
                appendlog(log.strip())
            except Exception as err:
                appendlog("LOG SERVER: disconnected.")
                break

#
# data server thread
#
data_sock=None
data_addr=('', data_port)

def data_server():
    global data_sock
    data_sock=ServerSocket(data_addr)

    while True:
        c, client=data_sock.accept()
        appendlog("DATA SERVER: connected to "+str(client)+".")

        while True:
            try:
                recv_bytes=log_sock.recv(4)
                info_type=struct.unpack("<4s", recv_bytes)

                if info_type=="GPS":
                    recv_bytes=log_sock.recv(64)
                    mainWindow.get_ui().gpssig.emit(recv_bytes)
                elif info_type=="APM":
                    recv_bytes=log_sock.recv(44)
                    mainWindow.get_ui().spvsig.emit(recv_bytes)
                else:
                    pass
            except Exception as err:
                appendlog("DATA SERVER: disconnected.")
                break

#
# route server thread
#
route_sock=None
route_addr=('', route_port)

def route_server():
    global route_sock
    route_sock=ServerSocket(route_addr)

    while True:
        c, client=route_sock.accept()
        appendlog("ROUTE SERVER: connected to "+str(client)+".")

        while True:
            pass

if __name__=='__main__':
    def show_ui():
        app=QtGui.QApplication(sys.argv)
        mainWindow.get_ui().show()
        setlx(900)
        setly(900)
        setrx(900)
        setry(900)
        setz(900)
        setstatus("disconnected")
        sys.exit(app.exec_())

    def do_serve():
        global sock
        host=''
        port=ctrl_port
        sock=ServerSocket((host, port))

        while True:
            c, client=sock.accept()
            appendlog("CONTROL SERVER: connected to "+str(client)+".")
            setstatus('connected')

            while True:
                time.sleep(0.1)
                try:
                    sock.recv(1)
                except Exception as err:
                    print err
                    appendlog("CONTROL SERVER: disconnected.")
                    setstatus('disconnected')
                    break

    ui_thrd=threading.Thread(target=show_ui)
    ui_thrd.start()

    sv_thrd=threading.Thread(target=do_serve)
    sv_thrd.daemon=True
    sv_thrd.start()

    log_thrd=threading.Thread(target=log_server)
    log_thrd.daemon=True
    log_thrd.start()

    time.sleep(0.5)
    key_thrd=threading.Thread(target=key_supervisor)
    key_thrd.daemon=True
    key_thrd.start()
