SERVER_AGENT=(0x1)<<29
    
YAW=(0x1)<<25
ROLL=(0x2)<<25
PITCH=(0x3)<<25
THROTTLE=(0x4)<<25
MODE=(0x5)<<25
    
LAUNCH=(0xc)<<25
LAND=(0xd)<<25
URGENT_LAND=(0xe)<<25
    
RELATED=(0x1)<<24
    
NUMBER_BITS=0xfff

def protocolCmd(instr, val):
    return SERVER_AGENT+instr+(val&NUMBER_BITS)
