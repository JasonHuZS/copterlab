class Degree:
    def __init__(self, d, m):
        self.d=d
        self.m=m
        return

    def __str__(self):
        return "%3d"%(self.d)+"d"+"%2.4f"%(self.m)+"m"
