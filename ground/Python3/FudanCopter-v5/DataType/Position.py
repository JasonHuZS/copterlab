from .Degree import Degree

class Position:
    def __init__(self, longitude, ew, latitude, ns):
        self.longitude=longitude
        self.ew=ew.upper()
        self.latitude=latitude
        self.ns=ns.upper()
        return

    def __str__(self):
        return "longitude:\t"+str(self.longitude)+" "+str(self.ew)+"\n"+\
               "latitude:\t" +str(self.latitude)+" "+str(self.ns)+"\n"
