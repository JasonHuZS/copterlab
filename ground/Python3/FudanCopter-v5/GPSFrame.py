if __name__=='__main__':
    from DataType import *
else:
    from .DataType import *
from tkinter import *
from tkinter import ttk

class GPSFrame(ttk.Frame):
    def __init__(self, master=None):
        ttk.Frame.__init__(self, master)

        ttk.Label(self, text="GPS : ").grid(column=1, row=1, sticky=(W, E))
        ttk.Label(self, text="Long: ").grid(column=1, row=2, sticky=(W, E))
        ttk.Label(self, text="Lat : ").grid(column=1, row=3, sticky=(W, E))
        ttk.Label(self, text="HDOP: ").grid(column=1, row=4, sticky=(W, E))
        ttk.Label(self, text="altit: ").grid(column=1, row=5, sticky=(W, E))
        ttk.Label(self, text="sate: ").grid(column=1, row=6, sticky=(W, E))
        ttk.Label(self, text="speed: ").grid(column=1, row=7, sticky=(W, E))
        ttk.Label(self, text="avail: ").grid(column=1, row=8, sticky=(W, E))

        self.long=StringVar()
        self.lat=StringVar()
        self.hdop=StringVar()
        self.altitude=StringVar()
        self.sate=StringVar()
        self.speed=StringVar()
        self.avail=StringVar()

        ttk.Label(self, textvariable=self.long).grid(column=2, row=2, sticky=(W, E))
        ttk.Label(self, textvariable=self.lat).grid(column=2, row=3, sticky=(W, E))
        ttk.Label(self, textvariable=self.hdop).grid(column=2, row=4, sticky=(W, E))
        ttk.Label(self, textvariable=self.altitude).grid(column=2, row=5, sticky=(W, E))
        ttk.Label(self, textvariable=self.sate).grid(column=2, row=6, sticky=(W, E))
        ttk.Label(self, textvariable=self.speed).grid(column=2, row=7, sticky=(W, E))
        ttk.Label(self, textvariable=self.avail).grid(column=2, row=8, sticky=(W, E))

        self.long.set("000d00.0000m E")
        self.lat.set(" 00d00.0000m N")
        self.hdop.set("0.00")
        self.altitude.set("10.0m")
        self.sate.set("00")
        self.speed.set("0m/s")
        self.avail.set("Invalid")

        for child in self.winfo_children():
            child.grid_configure(padx=5, pady=5)

        return

    def set(self, gps):
        self.long.set(str(gps.pos.longitude)+" "+gps.pos.ew)
        self.lat.set(str(gps.pos.latitude)+" "+gps.pos.ns)
        self.hdop.set(str(gps.dop.hdop))
        self.altitude.set(str(gps.altitude))
        self.sate.set(str(gps.satellites))
        self.speed.set(str(gps.speed))
        self.avail.set("Invalid" if str(gps.avail).upper()=='V' else "Valid")
        return

if __name__=='__main__':
    root=Tk()
    root.title("GPSFlame test")
    root.columnconfigure(0,weight=1)
    root.rowconfigure(0,weight=1)
    root.minsize(250,300)
    
    mainf=ttk.Frame(root, padding="10 10 10 10")
    mainf.grid(column=0, row=0, sticky=(N, W, E, S))
    mainf.columnconfigure(0, weight=1)
    mainf.rowconfigure(0, weight=1)
    mainf.configure(width=600, height=500, borderwidth=5, relief="sunken")
    
    gpsf=GPSFrame(mainf)
    gpsf.grid(column=0, row=0)
    gpsf.configure(width=100, height=200, borderwidth=5, relief="sunken")
    
    clicked=False
    def doClick(*args):
        global clicked
        if clicked==False:
            gpsd=GPS(Position(Degree(120,12.345),'e',
                              Degree(30,57.342),'n'),
                     "10m",
                     DOP(70.0, 80.0, 90.0),
                     3,
                     "1m/s",
                     'V')
            gpsf.set(gpsd)
            clicked=True
        else:
            gpsd=GPS(Position(Degree(50,32.365),'w',
                              Degree(88,10.342),'s'),
                     "20m",
                     DOP(10.0, 20.0, 30.0),
                     8,
                     "4m/s",
                     'A')
            gpsf.set(gpsd)
            clicked=False
        return
    ttk.Button(mainf, text="click", command=doClick).grid(column=0, row=2)

    for child in mainf.winfo_children():
        child.grid_configure(padx=5, pady=5)

    root.mainloop()
