class DOP:
    def __init__(self, pdop, hdop, vdop):
        self.hdop=hdop
        self.vdop=vdop
        self.pdop=pdop
        return

    def __str__(self):
        return "PDOP:\t"+str(self.pdop)+"\n"+\
               "HDOP:\t"+str(self.hdop)+"\n"+\
               "VDOP:\t"+str(self.vdop)+"\n"
               
