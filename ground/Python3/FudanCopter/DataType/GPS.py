from .Position import Position
from .Degree import Degree
from .DOP import DOP
import struct

class GPS:
    def __init__(self, pos, altitude, dop, satellites, speed, avail=False):
        self.pos=pos
        self.altitude=altitude
        self.dop=dop
        self.satellites=satellites
        self.speed=speed
        self.avail=avail.upper()
        return

    def __str__(self):
        return str(self.pos)+str(self.dop)+\
               "altitude: "+str(self.altitude)+\
               "satellites: "+str(self.satellites)+\
               "speed: "+str(self.speed)+\
               "avail: "+str(self.avail)+"\n"
            
def parseBytes(byte):
    th,tm,ts,latd,latm,lond,lonm,ns,ew,pad1,pad2,alt,pdop,hdop,vdop,\
        spd,tms,tmns,posfix,alu,stat,sate,spu=struct.unpack("<HHIIIIIccccffffflllccBc", byte)
    gps=GPS(Position(Degree(lond, lonm/10000), str(ew, "ascii"),
                     Degree(latd, latm/10000), str(ns, "ascii")),
            str(alt)+str(alu, "ascii"),
            DOP(pdop,hdop,vdop),
            sate,
            str(spd)+str(spu, "ascii"),
            str(stat, "ascii"))
    return gps
