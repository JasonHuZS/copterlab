from FudanCopter import *
import threading

root=Tk()
root.title("GPSFlame test")
root.columnconfigure(0,weight=1)
root.rowconfigure(0,weight=1)
root.minsize(250,300)

mainf=ttk.Frame(root, padding="10 10 10 10")
mainf.grid(column=0, row=0, sticky=(N, W, E, S))
mainf.columnconfigure(0, weight=1)
mainf.rowconfigure(0, weight=1)
mainf.configure(width=600, height=500, borderwidth=5, relief="sunken")

gpsf=GPSFrame(mainf)
gpsf.grid(column=0, row=0)
gpsf.configure(width=100, height=200, borderwidth=5, relief="sunken")

clicked=False
def doClick(*args):
    global clicked
    if clicked==False:
        gpsd=GPS(Position(Degree(120,12.345),'e',
                          Degree(30,57.342),'n'),
                 "10m",
                 DOP(70.0, 80.0, 90.0),
                 3,
                 "1m/s",
                 'V')
        gpsf.set(gpsd)
        clicked=True
    else:
        gpsd=GPS(Position(Degree(50,32.365),'w',
                          Degree(88,10.342),'s'),
                 "20m",
                 DOP(10.0, 20.0, 30.0),
                 8,
                 "4m/s",
                 'A')
        gpsf.set(gpsd)
        clicked=False
    return
ttk.Button(mainf, text="click", command=doClick).grid(column=0, row=2)

for child in mainf.winfo_children():
    child.grid_configure(padx=5, pady=5)

p=10
def dispGPS():
    global p
    host=''
    port=10000
    server=Server.ServerSocket((host, port), 1)
    while True:
        server.accept()
        p=server.recv(64)
        gps=parseBytes(p)
        gpsf.set(gps)
        server.close()
    return

m=threading.Thread(target=dispGPS)
m.daemon=True
m.start()

root.mainloop()
