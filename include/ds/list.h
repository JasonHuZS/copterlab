#ifndef _list_H_
#define _list_H_

struct dlist{
	struct dlist *prev;
	struct dlist *next;
	void *obj;
};

#define dlist_foreach(var, head)		for(var=head->next;var!=head;var=var->next)
#define dlist_reverse_foreach(var, head)	for(var=head->prev;var!=head;var=var->prev)

#define DLIST_HEADER_INIT(link)	\
	do{\
		link.prev=&link;\
		link.next=&link;\
		link.obj=NULL;\
	}while(0)
inline struct dlist *alloc_list_head();

#define DLIST_INIT(link,obj)		\
	do{\
		link.prev=NULL;\
		link.next=NULL;\
		link.obj=obj;\
	}while(0)
inline struct dlist *alloc_list(void *obj);
inline void destory_list(struct dlist *link);

inline struct dlist *prev_list(struct dlist *link);
inline struct dlist *next_list(struct dlist *link);

inline void add_list(struct dlist *list, struct dlist *link);
inline void del_list(struct dlist *link);

void append_list(struct dlist*pos, void *obj);

void eliminate_list(struct dlist *head);

#endif
