#ifndef _rbtree_H_
#define _rbtree_H_

struct rbtree_node{
	struct rbtree_node *left;
	struct rbtree_node *right;
	struct rbtree_node *parent;
	long key;
	void *val;
	char _color;
};
typedef struct rbtree_node rbtree_node_t;

typedef struct rbtree{
	struct rbtree_node *root;
} rbtree_t;

rbtree_t *alloc_tree(rbtree_node_t *root);
inline void free_tree(rbtree_t *tree);
void traverse_free(rbtree_node_t *node);
void recur_free_tree(rbtree_t *tree);

inline rbtree_node_t *get_nil();

rbtree_node_t *alloc_node(long key, void *val);
inline void free_node(rbtree_node_t *node);
/* function: static_init_node
 * note that this function should be used to initialize node statically 
 *     allocated. dynamically allocated nodes are preferred.
 */
inline void static_init_node(rbtree_node_t *node, long key, void *val);

rbtree_node_t *minimum(rbtree_node_t *node);
rbtree_node_t *maximum(rbtree_node_t *node);

/* function: tree_search
 * this function is a recursive one. iterative search works much more 
 *     efficiently, that is:
 * 	tree_iter_search()
 */
rbtree_node_t *tree_search(rbtree_node_t *node, long key);
rbtree_node_t *tree_iter_search(rbtree_node_t *node, long key);

rbtree_node_t *tree_successor(rbtree_node_t *node);
rbtree_node_t *tree_predecessor(rbtree_node_t *node);

void left_rotate(rbtree_t *tree, rbtree_node_t *x);
void right_rotate(rbtree_t *tree, rbtree_node_t *y);

void tree_insert(rbtree_t *tree, rbtree_node_t *z);

void tree_delete(rbtree_t *tree, rbtree_node_t *z);

int tree_bheight(rbtree_t *tree);
int tree_height(rbtree_t *tree);

#endif
