#ifndef _basic_types_H_
#define _basic_types_H_
#include <linux/types.h>

enum gps_type {
	NONE=0,
	GPGGA=1,
	GPGLL=2,
	GPGSA=3,
	GPGSV=4,
	GPRMC=5,
	GPVTG=6,
	GPMSS=7,
	GPZDA=8
};

/*
 * 		GPS mode
 * SPS mode: standard positioning service, with <=100 meters precision
 * PPS mode: precise positioning service, with <=10 meters precision
 */
enum pos_fix {
	INVALID=0,	/*fix not available or invalid*/
	SPS=1,		/*GPS SPS mode, fix valid*/
	DSPS=2,		/*Differential GPS, SPS mode, fix valid*/
	PPS=3		/*GPS PPS mode, fix valid*/
};

enum mod_indct{
	SELF='A',
	DIFF='D',
	ESTI='E',
	NVAL='N'
};

struct utc_time {
	__u16 h;
	__u16 m;
	__u32 s;	//multiplied by 1000
};
struct date {
	__u8 day;
	__u8 month;
	__u8 year;
};
struct degree {
	__u32 d;
	__u32 m;	//multiplied by 10000
};
struct position {
	struct degree latitude;
	struct degree longitude;
	char ns;
	char ew;
};

struct satellite_info {
	__u8 id;
	__u8 elevation;	//degrees
	__u16 azimuth;	//degrees
	__u16 snr;	//dBHz
};

#endif
