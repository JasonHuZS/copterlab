#ifndef _comm_apm_H_
#define _comm_apm_H_

struct communication {
    unsigned int head;
    unsigned int ch1;
    unsigned int ch2;
    unsigned int ch3;
    unsigned int ch4;
    unsigned int ch5;
    unsigned int ch6;
    unsigned int ch7;
    unsigned int ch8;
    unsigned int tail;
};

//reset the radio. throttle = 1000, roll,pitch,yaw=1500
int reset_radio(struct communication *comm);

//ch1-ch8 = 1000
int low_radio(struct communication *comm);

//ch1-ch8 = 2000
int high_radio(struct communication *comm);

//roll = 1500, pitch = 1500, throttle = 1000, yaw = 2000
int arm_motor(struct communication *comm);

//translate the 16_t comm to 8_t buf for UART writing
int send_command(struct communication comm, unsigned char *buf);

//write the new channels' values to apm through UART
int command_write(int fd,struct communication comm);

//constrain the pwm value between 1000 to 2000
int constrain_16t(int pwm);

//set the roll value, should be between 1000 to 2000
int set_roll(struct communication *comm, int pwm);

//set the pitch value, should be between 1000 to 2000
int set_pitch(struct communication *comm, int pwm);

//set the throttle value, should be between 1000 to 2000
int set_throttle(struct communication *comm, int pwm);

//set the yaw value, should be between 1000 to 2000
int set_yaw(struct communication *comm, int pwm);

//set the mode like stablelize, loiter and so on
int set_mode(struct communication *comm, int mode);

#endif
