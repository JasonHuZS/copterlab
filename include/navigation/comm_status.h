#ifndef _comm_status_H_
#define _comm_status_H_

////////////////////////////////////////////////////////////////
//////////////////information for use///////////////////////////
/*
roll              : in pi*1000
pitch             : in pi*1000
yaw               : in pi*1000
arm               : 0 => disarm  ;  1 => armed;
pre_arm_rc_check  : 0 => failed  ;  1 => successed;
pre_arm_check     : 0 => failed  ;  1 => successed;
auto_armed        : 0 => failed  ;  1 => successed;
battery_voltage   : in mV
motor_a           : [900-2100]
motor_b           : [900-2100]
motor_c           : [900-2100]
motor_d           : [900-2100]
barometer_alt     : in mM
pressure          : in hectopascal
ground_pressure   : in hectopascal
GPS_status        : 0 => NO_GPS ; 1 => NO_FIX ; 2 => GPS_OK_FIX_2D ; 3 => GPS_OK_FIX_3D ;
GPS_HDOP          : in cM
GPS_latitude      : in degrees * 10,000,000
GPS_longitude     : in degrees * 10,000,000
*/
/////////////////////////////////////////////////////////////////

struct status {
  short int roll;
  short int pitch;
  short int yaw;
  unsigned char arm;
  unsigned char pre_arm_rc_check;
  unsigned char pre_arm_check;
  unsigned char auto_armed;
  short int battery_voltage;
  unsigned short int motor_a;
  unsigned short int motor_b;
  unsigned short int motor_c;
  unsigned short int motor_d;
  short int barometer_alt;
  float pressure;
  float ground_pressure;
  unsigned char GPS_status;
  unsigned short int GPS_HDOP;
  int GPS_latitude;
  int GPS_longitude;
};

int read_line(int fd, unsigned char *rx);
int read_status(int fd, struct status *inf);

#endif
