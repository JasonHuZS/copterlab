#ifndef _compass_H_
#define _compass_H_
#include <linux/types.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <linux/ioctl.h>
#include <fcntl.h>
#include "wrapper/io.h"

/*			compass HMC5883
 *  in this project, we are using HMC5883 as default. any codes that
 *  implements read, write, initialization, or any other basic ops 
 *  are based on this chip and I2C.
 */

#define COMPASS_ADDRESS      0X1E
#define CONFIGREGA           0X00
#define CONFIGREGB           0X01
#define MAGGAIN              0X20
//#define POSITIVEBIASCONFIG   0X11
#define POSITIVEBIASCONFIG   (SAMPLEAVERAGING_8<<5 | DATAOUTPUTRATE_75HZ<<2 | NORMALOPERATION) 
#define NEGATIVEBIASCONFIG   0X12
#define NORMALOPERATION      0X10
#define MODEREGISTER         0X02
#define CONTINUOUSCONVERSION 0X00
#define SINGLECONVERSION     0X01

// CONFIGREGA VALID SAMPLE AVERAGING FOR 5883L
#define SAMPLEAVERAGING_1    0X00
#define SAMPLEAVERAGING_2    0X01
#define SAMPLEAVERAGING_4    0X02
#define SAMPLEAVERAGING_8    0X03

// CONFIGREGA VALID DATA OUTPUT RATES FOR 5883L
#define DATAOUTPUTRATE_0_75HZ 0X00
#define DATAOUTPUTRATE_1_5HZ  0X01
#define DATAOUTPUTRATE_3HZ    0X02
#define DATAOUTPUTRATE_7_5HZ  0X03
#define DATAOUTPUTRATE_15HZ   0X04
#define DATAOUTPUTRATE_30HZ   0X05
#define DATAOUTPUTRATE_75HZ   0X06

struct compass{
	__s32 x,y,z;
};

struct compass_field{
	float x,y,z;
};

#define compass_write(fd, __cmd, __data)	wrap_i2c_write(fd, __cmd, __data)
#define compass_read(fd, __cmd)			wrap_i2c_read(fd, __cmd)

/* function: compass_specify
 * ret val: pass if 0
 * this function specefies HMC5883 from HMC5843
 * note that can continue processing only if ret val is 0
 */
int compass_specify(int fd);

int compass_init(int fd);
int compass_format(int fd, struct compass *buf);

/* the meaning of ret val of this function
 * if return 0, that means a period of iteration is over
 * if return 1, iteration should be continued
 */
int compass_handle(struct compass *buf, struct compass_field *res);

void compass_offset(struct compass_field *fld);

inline struct compass_field compass_get_offset();

#endif
