#ifndef _gps_rcnsr_H_
#define _gps_rcnsr_H_
#define BUF_SIZE		(2000)

int gps_init(char *port, int baudrate, int databits, int parity, int stopbits);
int gps_readline(int fd, char *ret_buf);

#endif
