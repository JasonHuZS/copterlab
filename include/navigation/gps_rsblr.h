#ifndef _gps_rsblr_H_
#define _gps_rsblr_H_
#include "navigation/measures.h"

#define GPGGA_MAGIC	"GPGGA"
#define GPGLL_MAGIC	"GPGLL"
#define GPGSA_MAGIC	"GPGSA"
#define GPGSV_MAGIC	"GPGSV"
#define GPRMC_MAGIC	"GPRMC"
#define GPVTG_MAGIC	"GPVTG"
#define GPMSS_MAGIC	"GPMSS"
#define GPZDA_MAGIC	"GPZDA"

//full line of string as input
int __parse_gpgga(char *str, struct gpggainfo *infop);
int __parse_gpgll(char *str, struct gpgllinfo *infop);
int __parse_gpgsa(char *str, struct gpgsainfo *infop);
int __parse_gpgsv(char *str, struct gpgsvinfo *infop);
int __parse_gprmc(char *str, struct gprmcinfo *infop);
int __parse_gpvtg(char *str, struct gpvtginfo *infop);
int __parse_gpmss(char *str, struct gpmssinfo *infop);
int __parse_gpzda(char *str, struct gpzdainfo *infop);
int general_parse(char *str, struct gpsinfo *infop, struct satellite_info *info_arr);

#endif
