#ifndef _gps_updtr_H_
#define _gps_updtr_H_
#include <linux/types.h>
#include <time.h>
#include "navigation/basic_types.h"
#include "navigation/measures.h"
#include "navigation/gps_rcnsr.h"

struct gps_struct {
	struct utc_time time;	//UTC time
	struct position pos;	//position coordinate
	float altitude;		//MSL(mean sea level) altitude
	float pdop;		//position dilution of precision
	float hdop;		//horizontal dilution of precision
	float vdop;		//vertical dilution of precision
	float speed;
	struct timespec stamp;	//the timestamp of last update
	enum pos_fix pos_fix;	//position fix indicator
	char alt_unit;		//unit of altitude
	char status;		//A=data valid or V=data not valid
	__u8 satellites;	//satallites used/12
	char unit;		//unit of speed
};
enum gps_type gps_update(char *str, struct gps_struct *var);

#endif
