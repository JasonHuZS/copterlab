#ifndef _inertial_H_
#define _inertial_H_

#include "navigation/mpu6050.h"
#include "navigation/matrix.h"
#include <stdio.h>

#define ratio_g 93965			//update rate at 100Hz
#define ratio_a 16384
#define g 	9.8
#define d_time	0.01

void matrix_set_A(Mtype angle_x, Mtype angle_y, Mtype angle_z, Mtype (*A)[3][3]);
void get_C_mea(Mtype J[3], Mtype K[3], Mtype (*C)[3][3]);
void accel_calib(int fd, float (*accel_cal)[3]);
void IMU_update(int fd, Mtype accel_cal[3], Mtype (*angle)[3], Mtype (*Z)[3]);
void DCM_update_Kalman(Mtype angle[3], Mtype Z[3], Mtype Comp[3], Mtype (*DCM)[3][3], Mtype (*Poss)[3][3]);

#endif
