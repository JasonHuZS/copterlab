#ifndef _matrix_H_
#define _matrix_H_

#include <stdio.h>

typedef float Mtype;

void matrix_init(Mtype (*Q)[3][3]);
void matrix_multiply_3x1(Mtype x[3][3], Mtype y[3], Mtype (*z)[3]);
void matrix_multiply_3x3(Mtype x[3][3], Mtype y[3][3], Mtype (*z)[3][3]);
void matrix_numMul(Mtype x[3][3], Mtype num, Mtype (*z)[3][3]);
void matrix_transform(Mtype x[3][3], Mtype (*z)[3][3]);
void matrix_add_3x1(Mtype x[3], Mtype y[3], Mtype (*z)[3]);
void matrix_add_3x3(Mtype x[3][3], Mtype y[3][3], Mtype (*z)[3][3]);
void matrix_subtract_3x1(Mtype x[3], Mtype y[3], Mtype (*z)[3]);
void matrix_subtract_3x3(Mtype x[3][3], Mtype y[3][3], Mtype (*z)[3][3]);
void matrix_reverse(Mtype x[3][3], Mtype (*z)[3][3]);
void cross_product(Mtype x[3], Mtype y[3], Mtype (*z)[3]);

void matrix_copy(Mtype A[3][3], Mtype (*B)[3][3]);
void matrix_zero(Mtype (*A)[3][3]);

#endif
