#ifndef _measures_H_
#define _measures_H_
#include <linux/types.h>
#include "navigation/basic_types.h"

struct gpggainfo {
	struct utc_time time;
	struct position pos;
	enum pos_fix pos_fix;
/* val  description
 *  0	0 Fix not available or invalid
 *  1	GPS SPS Mode, fix valid
 *  2	Differential GPS, SPS Mode, fix valid
 *  3	GPS PPS Mode, fix valid
 */
	__u8 satellites;
	float hdop;
	float altitude;
	float geoid;
	char alt_unit;
	char geoid_unit;
	__u16 diff_corr;
	__u16 ref_id;
};
struct gpgllinfo {
	struct position pos;
	struct utc_time time;
	char status;
	enum mod_indct mod;
};

struct gpgsainfo {
	char mode1;
/* val	description
 *  M	Manual: forced to operate in 2D or 3D mode
 *  A	2DAutomatic: allowed to automatically switch 2D/3D
 */
	char mode2;
/* val	description
 *  1	Fix Not Available
 *  2	2D
 *  3	3D
 */
	__u8 ch1;
	__u8 ch2;
	__u8 ch3;
	__u8 ch4;
	__u8 ch5;
	__u8 ch6;
	__u8 ch7;
	__u8 ch8;
	__u8 ch9;
	__u8 ch10;
	__u8 ch11;
	__u8 ch12;
	float pdop;
	float hdop;
	float vdop;
};

struct gpgsvinfo {
	__u8 nmsg;
	__u8 msg;
	__u16 stat_in_view;
	struct satellite_info *info_arr;
};

struct gprmcinfo {
	struct utc_time time;
	struct position pos;
	char status;
	char mag_var_dir;
	float speed;	//knots
	float course;	//degrees
	struct date date;
	float mag_var;
	enum mod_indct mod;
};

struct gpvtginfo {
	float course;
	float course2;
	float speed;
	float speed2;
	char ref;
	char ref2;
	char unit;
	char unit2;
	enum mod_indct mod;
};

struct gpmssinfo {
	__u16 sig_strength;
	__u16 snr;
	float freq;
	__u32 rate;
};

struct gpzdainfo {
	struct utc_time time;
	struct date date;
	__u16 offset;
	__u16 offset2;
};

struct gpsinfo {
	enum gps_type type;
	union {
		struct gpggainfo gpgga;
		struct gpgllinfo gpgll;
		struct gpgsainfo gpgsa;
		struct gpgsvinfo gpgsv;
		struct gprmcinfo gprmc;
		struct gpvtginfo gpvtg;
		struct gpmssinfo gpmss;
		struct gpzdainfo gpzda;
	} info;
};

#endif
