#ifndef _mpu6050_H_
#define _mpu6050_H_
#include <linux/types.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <linux/ioctl.h>
#include <fcntl.h>
#include "wrapper/io.h"

#define	SMPLRT_DIV		0x19
#define	CONFIG			0x1A
#define	GYRO_CONFIG		0x1B
#define	ACCEL_CONFIG	        0x1C
#define	ACCEL_XOUT_H	        0x3B
#define	ACCEL_XOUT_L	        0x3C
#define	ACCEL_YOUT_H	        0x3D
#define	ACCEL_YOUT_L	        0x3E
#define	ACCEL_ZOUT_H	        0x3F
#define	ACCEL_ZOUT_L	        0x40
#define	TEMP_OUT_H		0x41
#define	TEMP_OUT_L		0x42
#define	GYRO_XOUT_H		0x43
#define	GYRO_XOUT_L		0x44	
#define	GYRO_YOUT_H		0x45
#define	GYRO_YOUT_L		0x46
#define	GYRO_ZOUT_H		0x47
#define	GYRO_ZOUT_L		0x48
#define	PWR_MGMT_1		0x6B
#define	WHO_AM_I	        0x75

#define SLV_ADDR		0x68

struct mpu6050_data{
	float accelx,accely,accelz;
	float gyrox,gyroy,gyroz;
};


int mpu6050_init(int fd);

#define mpu6050_write(fd, __cmd, __data)	wrap_i2c_write(fd, __cmd, __data)
#define mpu6050_read(fd, __cmd)			wrap_i2c_read(fd, __cmd)

int mpu6050_accelx(int fd, float *acc);
int mpu6050_accely(int fd, float *acc);
int mpu6050_accelz(int fd, float *acc);
int mpu6050_gyrox(int fd, float *acc);
int mpu6050_gyroy(int fd, float *acc);
int mpu6050_gyroz(int fd, float *acc);

int mpu6050_fetch(int fd, struct mpu6050_data *data);

#endif
