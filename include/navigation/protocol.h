#ifndef _protocol_H_
#define _protocol_H_
#include <sys/types.h>
#include <linux/types.h>

/* 		control plane protocol
 *
 * this protocol is used between different processes to send signals 
 * to control the process which do actual communication to the control 
 * board. all any other augmented fancy design should be based on this.
 */

/* 			protocol bitmap
 *
 *     all the bit mask should be fixed in a 4-byte-length space. bitmap is 
 * shown as followed:
 * 3		 2		 1
 * 1		 4		 6		 8		 0
 * x x x - - - - $ ? ? ? ? ? ? ? ? ? ? ? ? ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^
 *
 * where:
 * 	x	AGENT_BITS
 * 	-	INSTR_BITS
 * 	$	RELATED_BIT
 * 	?	unmapped bits
 * 	^	NUMBER_BITS
 */

/*  process agent mask  */
#define SERVER_AGENT		(0x1)	/* human control process magic */
#define OBSAVD_AGENT		(0x2)	/* obstacle-avoiding process magic */
#define ROUTINE_AGENT		(0x3)	/* normal routine process magic */
#define AGENT_BITS		(((__u32)0x7)<<29)/* bit occupation notation */

/*  instruction mask  */
#define YAW_INSTR		(0x1)
#define ROLL_INSTR		(0x2)
#define PITCH_INSTR		(0x3)
#define THROT_INSTR		(0x4)
#define MODE_INSTR		(0x5)

#define LAUNCH_INSTR		(0xc)
#define LAND_INSTR		(0xd)
#define URGENT_LAND		(0xe)

#define MIN_INSTR		(0xa)
#define MAX_INSTR		(0xb)

#define INSTR_BITS		(((__u32)0xf)<<25)

/*  number mask  */
#define NUMBER_BITS		(0xfff)

/*  operations and special masks  */
/*  sign mask  */
#define RELATED_BIT		(((__u32)0x1)<<24)

/*  operations  */
#define PRO_CMD_ABS(__agent, __instr, __number) \
	((((__u32)__agent)<<29)+(((__u32)__instr)<<25)+((__u32)__number))

#define PRO_CMD_RLTD(__agent, __instr, __number) \
	((((__u32)__agent)<<29)+(((__u32)__instr)<<25)+RELATED_BIT+(((__u32)__number)&NUMBER_BITS))

#define PRO_RELATED(cmd)	((cmd&RELATED_BIT)>>24)

#define PRO_NUM(cmd)		\
	(cmd&RELATED_BIT?((((__s16)(cmd&NUMBER_BITS))<<4)>>4):cmd&NUMBER_BITS)

#define PRO_INSTR(cmd)		((cmd&INSTR_BITS)>>25)

#define PRO_AGENT(cmd)		((cmd>>29)&0x7)

#endif
