#ifndef _ultrasonic_H_
#define _ultrasonic_H_
#include <stdio.h>

#define DETECT_CMD		(0xB4)

#define DOWN_ADDR		(0x68)
#define LEFT_ADDR		(0x00)
#define RIGHT_ADDR		(0x00)
#define FRONT_ADDR		(0x00)
#define BACK_ADDR		(0x00)

typedef enum detect_direction{
	detect_down=0,
	detect_left=1,
	detect_right=2,
	detect_front=3,
	detect_back=4
} detect_dir_t;

extern int addr[];

FILE *obsavd_init(char *filename);

int set_device(FILE *fp, detect_dir_t direction);

int send_general_command(FILE *fp, detect_dir_t direction, unsigned short cmd);

int detect_obstacle(FILE *fp, detect_dir_t direction);

int read_position(FILE *fp, detect_dir_t direction);
#endif
