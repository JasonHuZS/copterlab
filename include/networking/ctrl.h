#ifndef _ctrl_H_
#define _ctrl_H_
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include "navigation/protocol.h"

#define YAW		"YAW"
#define ROLL		"ROL"
#define PITCH		"PIT"
#define THROTTLE	"THR"

#define LAUNCH		"LAU"
#define LAND		"LAN"

typedef struct{
	char *server_info;
	pid_t ctrl_proc;
	int sig;
} ctrl_args_t;

void *ctrl_plane_start(void *args);

#endif
