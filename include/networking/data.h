#ifndef _data_H_
#define _data_H_
#include <string.h>
#include "wrapper/ipc.h"
#include "navigation/gps_updtr.h"

#define GPS			"GPS"

typedef struct{
	sem_t gps_sem;
	struct gps_struct gps;
	char gps_sent;
} data_buf_t;

typedef struct{
	data_buf_t *data;
	char *server_info;	//in the form of "IP:port"
} data_args_t;

/* function: data_plane_start
 *     this function servers as a client on the copter. keep sending data 
 * back to the gound station at regular time (not strict).
 * 
 * note:
 *     about the parameter:
 *         void *args
 *     it is indeed a pointer to data_args_t, that means a type cast is 
 * required here. void * here is used to match the input protocol of 
 * function pthread_create.
 */
void *data_plane_start(void *args);

#endif
