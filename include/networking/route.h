#ifndef _route_H_
#define _route_H_
#include <string.h>

typedef struct{
	char *server_info;	//in the form of "ip:port"
} route_args_t;

/* function: route_plane_start
 *     this function servers as a client on the copter. recieving routing 
 * data from server.
 * 
 * note:
 *     about the parameter:
 *         void *args
 *     it is indeed a pointer to route_args_t, that means a type cast is 
 * required here. void * here is used to match the input protocol of 
 * function pthread_create.
 */
void *route_plane_start(void *args);

#endif
