#ifndef _io_H_
#define _io_H_
#include <sys/types.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include "wrapper/tty_func.h"

/* 			wrapper of io
 * mainly include UART and I2C, and wrapper of read/write function.
 */

/* UART functions */
int wrap_uart_init(char *port, int baudrate, int datab, int parity, int stopb);

inline void wrap_uart_close(int fd);

/* I2C functions */
int wrap_i2c_open(char *dev);

#define i2c_set_slvaddr(fd, slvaddr)		ioctl(fd,I2C_SLAVE,slvaddr)

inline int wrap_i2c_write(int fd, __u8 cmd, __u8 data);

inline __s16 wrap_i2c_read(int fd, __u8 cmd);

/* read/write wrapper */
ssize_t wrap_read(int fd, void *buf, size_t len);

ssize_t wrap_write(int fd, const void *buf, size_t len);

#endif
