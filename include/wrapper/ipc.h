#ifndef _ipc_H_
#define _ipc_H_
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <semaphore.h>


/* mmap related wrapper */

/* function: wrap_namedmmap
 * allocate named pages for IPC
 * note that fd is the file descriptor of an successfully opened,
 * existed file on the disk, and this file will be closed.
 * note that if this function returns unexpectedly(namely MAP_FAILED),
 * this fault should be kind of fatal(since this wrapper has handled
 * some of easy error).
 */
void *wrap_namedmmap(int fd, unsigned int pages);

/* function: wrap_anonmmap
 * allocate anonymous pages for IPC
 * note that pages cannot be zero, otherwise return NULL.
 */
void *wrap_anonmmap(unsigned int pages);

/* function: wrap_munmap
 * unmap the pages that are allocated by mmap
 */
inline int wrap_munmap(void *addr, unsigned int pages);

/* POSIX IPC wrapper */

/* POSIX semaphore */

/* function: wrap_namedsem
 * allocate named semaphore and initialize it with value of val.
 * note that the name of the semaphore is arranged by this function.
 * make sure name is a char array and at least 20 long.
 */
sem_t *wrap_namedsem(char *name, int val);

/* function: wrap_anonsem
 * allocate anonymous semaphore and init it with value of val.
 * the difference between this and the above one is whether being 
 * named or not.
 * use sem_destroy() to destroy semaphore after used.
 */
int wrap_anonsem(sem_t *sem, int val);
/* function: wrap_anonmutex
 * simplified version of the upper one, the val of which is defaultly 1.
 * serve as a mutex and hard to be wrong.
 */
inline int wrap_anonmutex(sem_t *sem);

/* function: wrap_semdestroy
 * dummy wrapper for consistency.
 */
inline int wrap_semdestroy(sem_t *sem);

/* function: wrap_semwait
 * wrapper of sem_wait(). the case of being interrupted by
 * signals has been handled. this function should be seldomly 
 * false. if so, it's severe.
 */
int wrap_semwait(sem_t *sem);
/* function: wrap_sempost
 * pseudo-wrapper of sem_post().
 */
inline int wrap_sempost(sem_t *sem);

#endif
