#ifndef _proc_H_
#define _proc_H_
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

/* 			mission_t
 *     this structure is used to decribe the mission to be executed 
 * in the processes those are forked in the main process.
 *     the actual function to be executed is:
 *     		int (*go) (void *)
 *     this prototype allows argument to be passed inside via a pointer:
 *     		void *args
 *     
 *     once if the process(which is executing 'go') exits abnormally, and 
 * an exit state is returned via zombie. main process should call:
 *              int (*undertake) (int)
 *     to which the exit state is passed as an int. this undertaker should 
 * be implemented by user. and this function will be launched as a new process 
 * and this process SHOULD NEVER FAIL.
 *
 * NOTICE:
 *     member of mission_t, go, should be implemented before use. if the 
 * pass-in argument, args, is used, it's the implemented functions' 
 * responsibility to check if the pointer is available or not.
 */

typedef struct mission{
	int (*go) (void *);
	void *args;
	int (*undertake) (int);
	pid_t pid;
} mission_t;

#define STATIC_MISSION(__go, __args)	{.go=__go, .args=__args, .undertake=do_rip, .pid=-1}
#define STATIC_MISSION_UDRTK(__go, __args, __udrtk)	\
	{.go=__go, .args=__args, .undertake=__udrtk, .pid=-1}

#define INVAL_MISSION			(-128)

#define exec_mission(mission)		(*mission->go)(mission->args)
#define be_undertaker(mission,state)	(*mission->undertake)(state)

void do_mission(mission_t *mission_p);

/*		REST_IN_PEACE / do_rip
 * this function is used to tell the main process that the undertaker is 
 * omitted.
 */
#define REST_IN_PEACE			(do_rip)
int do_rip(int state);
void do_undertaking(mission_t *mission_p, int state);

/* function: wrap_fork
 * fork wrapper
 */
pid_t wrap_fork();

/* function: wrap_waitall
 * wait for all child processes to end
 * return -1 if any unrecoverable error
 * return 0 if all children have been waited.
 */
int wrap_waitall();

#endif
