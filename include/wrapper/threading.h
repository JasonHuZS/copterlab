#ifndef _threading_H_
#define _threading_H_
#include <pthread.h>
#include "wrapper/ipc.h"

/* 			wrapper of pthread
 * this head file is the wrapper of pthread, which provides 
 * multi-threading support.
 */

/*    POSIX threading API    */
int wrap_thrd_create(pthread_t *thread, void *(*routine) (void *), void *arg);

inline int wrap_thrd_cancel(pthread_t thread);

int wrap_thrd_join(pthread_t thread, void **retval);

inline int wrap_thrd_detach_me();

/*    POSIX mutex API    */
/*    normal use    */
inline int wrap_mutex_init(pthread_mutex_t *mutex);

/*    error checking feature - for debugging use    */
int wrap_errchk_mutex_init(pthread_mutex_t *mutex);

/*    recursive   */
int wrap_recur_mutex_init(pthread_mutex_t *mutex);

/*    dummy wrapper for destroy    */
inline int wrap_mutex_destory(pthread_mutex_t *mutex);


#endif
