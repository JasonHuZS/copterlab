#ifndef _tty_func_H_
#define _tty_func_H_

#define FILE_OPEN_ERROR		"Cannot open file!"
#define BAUD_RATE_ERROR		"Cannot set baudrate!"
#define GET_ATTR_ERROR		"Cannot get attributes!"
#define SET_ATTR_ERROR		"Cannot set attributes!"

int open_and_init_tty(const char *dev);
int open_tty(const char *dev);
void close_tty(int fd);

//modifiers
int set_baudrate(int fd, int baud);
int set_dataformat(int fd,int databits,int parity,int stopbits);
int set_databit(int fd, int databint);
int set_parity(int fd, int parity);
int set_stopbit(int fd, int stopbit);
int hwflow_ctrl(int fd);
int swflow_ctrl(int fd);

//termios controllers
int store_termios(int fd);
int recover_termios(int fd);

#endif
