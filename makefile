makedir=navigation wrapper ds networking
subdir=$(makedir) benchmark Main
innavi=GPS Inertial MPU6050 Compass navi Comm Ultrasonic

.PHONY:all test dependency bench clean tags clean_tags clean_all\
	 $(addprefix test,${innavi} networking) main

all: dependency
	@for dir in ${makedir}; do \
	cd $${dir} &&make;cd ..; done
include *.mk

main: 
	@cd Main&&make
ifeq (${DO_FTP},1)
	@if script/ftp.pl target/ ${USER}@${IP}:${TARGET_BASE}/ ${PASS}; \
		then \
		echo "target files uploaded."; \
		else \
		echo "target files not upload."; \
		fi
endif

ifndef obj
test:
	$(error please use test as follow: make test obj=xxx)
else
test: dependency
ifeq ($(findstring ${obj},${innavi}),${obj})
	@cd navigation&&make test obj=${obj}
else
ifeq (networking, ${obj})
	@cd networking&&make test
endif
endif
ifeq (${DO_FTP},1)
	@if script/ftp.pl target/test ${USER}@${IP}:${TARGET_BASE}/test ${PASS}; \
		then \
		echo "target files uploaded."; \
		else \
		echo "target files not upload."; \
		fi
endif
endif

$(addprefix test,${innavi} networking):
	@make test obj=$(subst test,,$@)

#benchmark section
bench: dependency
	@cd benchmark&&make
ifeq (${DO_FTP},1)
	@if script/ftp.pl target/bench ${USER}@${IP}:${TARGET_BASE}/bench ${PASS}; \
		then \
		echo "target files uploaded."; \
		else \
		echo "target files not upload."; \
		fi
endif

tags:
	@ctags -R
	@cscope -Rbq

clean_tags:
	-@rm -rf tags cscope.*

dependency:
	@echo calculating dependency...
	@script/dependency.pl . ${subdir} >> /dev/null
	@echo calculating finished.
	@echo

configure:
	@script/do_conf.sh

clean:
	@for dir in ${subdir} target ground; do \
	cd $${dir} &&make clean;cd ..; done

clean_all: clean clean_tags
