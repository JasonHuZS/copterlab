#define _BSD_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "wrapper/io.h"
#include "navigation/comm_apm.h"

#define HEAD            21845
#define TAIL            43690
#define MAX             2000
#define MIN             1000
#define MID             1500

int reset_radio(struct communication *comm){
    comm->head = HEAD;
    comm->tail = TAIL;
    comm->ch1 = MID;
    comm->ch2 = MID;
    comm->ch3 = MIN;
    comm->ch4 = MID;
    comm->ch5 = MIN;
    comm->ch6 = MIN;
    comm->ch7 = MIN;
    comm->ch8 = MIN;
    return 0;
}

int low_radio(struct communication *comm){
    comm->head = HEAD;
    comm->tail = TAIL;
    comm->ch1 = MIN;
    comm->ch2 = MIN;
    comm->ch3 = MIN;
    comm->ch4 = MIN;
    comm->ch5 = MIN;
    comm->ch6 = MIN;
    comm->ch7 = MIN;
    comm->ch8 = MIN;
    return 0; 
}

int high_radio(struct communication *comm){
    comm->head = HEAD;
    comm->tail = TAIL;
    comm->ch1 = MAX;
    comm->ch2 = MAX;
    comm->ch3 = MAX;
    comm->ch4 = MAX;
    comm->ch5 = MAX;
    comm->ch6 = MAX;
    comm->ch7 = MAX;
    comm->ch8 = MAX;
    return 0; 
}

int arm_motor(struct communication *comm){
    comm->head = HEAD;
    comm->tail = TAIL;
    comm->ch1 = MID;
    comm->ch2 = MID;
    comm->ch3 = MIN;
    comm->ch4 = MAX;
    return 0; 
}

int send_command(struct communication comm, unsigned char *buf){
    unsigned int tmp;
    tmp = comm.head;
    *(buf+0) = (unsigned char)tmp;
    *(buf+1) = (unsigned char)(comm.head>>8);
    tmp = comm.ch1;
    *(buf+2) = (unsigned char)tmp;
    *(buf+3) = (unsigned char)(comm.ch1>>8);
    tmp = comm.ch2;
    *(buf+4) = (unsigned char)tmp;
    *(buf+5) = (unsigned char)(comm.ch2>>8);
    tmp = comm.ch3;
    *(buf+6) = (unsigned char)tmp;
    *(buf+7) = (unsigned char)(comm.ch3>>8);
    tmp = comm.ch4;
    *(buf+8) = (unsigned char)tmp;
    *(buf+9) = (unsigned char)(comm.ch4>>8);
    tmp = comm.ch5;
    *(buf+10) = (unsigned char)tmp;
    *(buf+11) = (unsigned char)(comm.ch5>>8);
    tmp = comm.ch6;
    *(buf+12) = (unsigned char)tmp;
    *(buf+13) = (unsigned char)(comm.ch6>>8);
    tmp = comm.ch7;
    *(buf+14) = (unsigned char)tmp;
    *(buf+15) = (unsigned char)(comm.ch7>>8);
    tmp = comm.ch8;
    *(buf+16) = (unsigned char)tmp;
    *(buf+17) = (unsigned char)(comm.ch8>>8);
    tmp = comm.tail;
    *(buf+18) = (unsigned char)tmp;
    *(buf+19) = (unsigned char)(comm.tail>>8);
    return 0;
}

int command_write(int fd,struct communication comm){
     unsigned char buf[20];
     send_command(comm,buf);
     int sl = 0;
     int i;
     int res = 0;
     while (1) {
		res=wrap_write(fd,buf+sl,20-sl);
		if(res<0){
			perror("write wrong!");
		        return -1;
		}
		else {
			sl += res;
		}
			if(sl==20) break;
     }
     return 0;
}

int constrain_16t(int pwm){
     if(pwm<1000) return 1000;
     else {
       if(pwm>2000) return 2000;
       else { return pwm; }
     }
}

int set_roll(struct communication *comm, int pwm){
     pwm = constrain_16t(pwm);
     comm->ch1 = pwm;
     return 0;
}

int set_pitch(struct communication *comm, int pwm){
     pwm = constrain_16t(pwm);
     comm->ch2 = pwm;
     return 0;
}

int set_throttle(struct communication *comm, int pwm){
     pwm = constrain_16t(pwm);
     comm->ch3 = pwm;
     return 0;
}

int set_yaw(struct communication *comm, int pwm){
     pwm = constrain_16t(pwm);
     comm->ch4 = pwm;
     return 0;
}

int set_mode(struct communication *comm, int mode){
     switch(mode){
	     case 1:
		     comm->ch5 = 1100;
		     break;
             case 2:
		     comm->ch5 = 1280;
		     break;
	     case 3:
		     comm->ch5 = 1420;
		     break;
	     case 4:
		     comm->ch5 = 1550;
		     break;
	     case 5:
		     comm->ch5 = 1690;
		     break;
	     case 6:
		     comm->ch5 = 1900;
		     break;
             default:
		     return -1;
     }
     return 0;
}
