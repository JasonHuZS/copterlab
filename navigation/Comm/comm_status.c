#define _BSD_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "wrapper/io.h"
#include "navigation/comm_status.h"
#define READ_BUF 150

int read_line(int fd, unsigned char *rx){
  int valid_sentence = 0;
  unsigned char buf[READ_BUF];
  int i;
  int term_offset;
  int is_begin=0;
  int is_end=0;

  wrap_read(fd,buf,READ_BUF);
  for(i=0;i<READ_BUF;i++){

    switch(buf[i]){
      case 'T':
	if(term_offset<46){
	  term_offset = 0;
	  valid_sentence = 0;
          is_end = 0;
	  is_begin = 0;
	  break;
	}
	is_end++;
	if (is_begin == 2 && is_end ==2){
	  valid_sentence = 1;
	  term_offset = 0;
	  is_begin = 0;
	  is_end = 0;
	}
        break;

      case 'X':
	if(is_begin == 2) break;
	term_offset = 0;
	is_end = 0;
	is_begin++;
	break;
      
      default:
	break;
    }

    if(valid_sentence == 1) return valid_sentence;
    
    if (term_offset < 46 && is_begin == 2){
      rx[term_offset++] = buf[i+1];
      if(term_offset <= 46 && rx[term_offset-1] == 84)
      {
	  term_offset = 0;
	  valid_sentence = 0;
          is_end = 0;
	  is_begin = 0;
      }
      is_end = 0;
    }
  }
  return valid_sentence;
}

int read_status(int fd, struct status *inf){
  unsigned char rx[46];
 
  if(read_line(fd,rx)){
  inf->roll  = rx[1]*256+rx[0];
  inf->pitch = rx[3]*256+rx[2];
  inf->yaw = rx[5]*256+rx[4];
  inf->arm = rx[6];
  inf->pre_arm_rc_check = rx[7];
  inf->pre_arm_check = rx[8];
  inf->auto_armed = rx[9];
  inf->battery_voltage = rx[11]*256+rx[10];
  inf->motor_a = rx[16]*256+rx[15];
  inf->motor_b = rx[18]*256+rx[17];
  inf->motor_c = rx[20]*256+rx[19];
  inf->motor_d = rx[22]*256+rx[21];
  inf->barometer_alt = rx[24]*256+rx[23]; //mm
  inf->pressure = ((float)(rx[26]*256+rx[25]+1000000))/1000; //
  inf->ground_pressure =((float)(rx[28]*256+rx[27]+1000000))/1000; //
  inf->GPS_status = rx[29];
  inf->GPS_HDOP = rx[31]*256+rx[30];
  inf->GPS_latitude = rx[35]*256*256*256+rx[34]*256*256+rx[33]*256+rx[32];
  inf->GPS_longitude = rx[39]*256*256*256+rx[38]*256*256+rx[37]*256+rx[36];

  return 0;
  }
  else {return -1;}
}
