#define _BSD_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "wrapper/io.h"
#include <navigation/comm_apm.h>
#include <navigation/comm_status.h>

#define DEVICE	"/dev/ttyUSB0"
#define PI 3.1415

struct communication comm1;
struct status inf;

int main() {

        int fd = wrap_uart_init(DEVICE,57600,8,'N',1);

	int res=0;
	if (fd==-1) {
		perror("cannot open device!");
		return -1;
	}

        while(1){
        int in;
	//scanf("%d",&in);
	in=10;
	switch(in){
		case 0:
		       return 0;
		case 1:
                       reset_radio(&comm1);
		       command_write(fd,comm1);
		       break;
		case 2:
		       low_radio(&comm1);
		       command_write(fd,comm1);
		       break;
		case 3:
		       high_radio(&comm1);
		       command_write(fd,comm1);
		       break;
		case 4:
		       arm_motor(&comm1);
		       command_write(fd,comm1);
		       break;
		case 5:
		       set_throttle(&comm1,2700);
		       command_write(fd,comm1);
		       break;
		case 6:
		       set_roll(&comm1,1700);
		       command_write(fd,comm1);
		       break;
		case 7:
		       set_pitch(&comm1,1700);
		       command_write(fd,comm1);
		       break;
		case 8:
		       set_yaw(&comm1,1700);
		       command_write(fd,comm1);
		       break;
		case 9:
		       set_mode(&comm1,3);
		       command_write(fd,comm1);
		       break;
		default: 
		       printf("\nerror!\n");
		       break;
              	}

        if(!read_status(fd,&inf)){printf("We got new data:\n");}
        printf("ROLL:%.3f\n",((float)inf.roll)/1000/PI*180);
        printf("PITCH:%.3f\n",((float)inf.pitch)/1000/PI*180);
        printf("YAW:%.3f\n",((float)inf.yaw)/1000/PI*180);
        printf("ARM:%d\n",inf.arm);
        printf("Auto_armed:%d\n",inf.auto_armed);
        printf("Battery:%.3f V\n",((float)inf.battery_voltage)/1000);
        printf("Motor_A:%d\n",inf.motor_a);
        printf("Motor_B:%d\n",inf.motor_b);
        printf("Motor_C:%d\n",inf.motor_c);
        printf("Motor_D:%d\n",inf.motor_d);
	printf("Alt:%.3f m\n",((float)inf.barometer_alt)/1000);
	printf("Pressure:%.3f\n",inf.pressure);
	printf("Ground_pressure:%.3f\n",inf.ground_pressure);
	printf("GPS_status:%d\n",inf.GPS_status);
	printf("GPS_HDOP:%.2f\n",((float)inf.GPS_HDOP)/100);
	printf("Lat:%d\n",inf.GPS_latitude);
	printf("Lng:%d\n",inf.GPS_longitude);
	printf("\n");

	usleep(100000);
  }
	return 1;
}
