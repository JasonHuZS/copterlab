#define _BSD_SOURCE
#include <math.h>
#include <unistd.h>
#include <string.h>
#include "navigation/compass.h"

#define HISTORY_SIZE		(16)
#define COMPASS_ACCLIMIT	(8)

static int off_init;
static int his_no;
static int his_ind;
static struct compass history[COMPASS_ACCLIMIT];
static struct compass acc_mag;
static struct compass_field calibration;
static struct compass_field offset;
static struct compass_field mag_his[HISTORY_SIZE];

int compass_specify(int fd){
	int res;
	if (compass_write(fd, CONFIGREGA, SAMPLEAVERAGING_8<<5 | DATAOUTPUTRATE_75HZ<<2 | NORMALOPERATION))
		return -1;
	res=compass_read(fd, CONFIGREGA);
	if (res == -1) return -1;
	if (res == SAMPLEAVERAGING_8<<5 | DATAOUTPUTRATE_75HZ<<2 | NORMALOPERATION) return 0;
	return -2;
}

int compass_init(int fd){
	int attempt=0, success=0, goodc=0;
	int ret;
	struct compass atmp;
	float cal[3];
	float exp_x=766, exp_yz=713, gain_mul=660.0/1090;
	bzero(history, sizeof(history));
	his_ind=0;
	his_no=0;

	off_init=0;
	bzero(&acc_mag, sizeof(acc_mag));
	calibration.x=calibration.y=calibration.z=1.0;
	offset.x=offset.y=offset.z=0.0;

#if 1
	offset.y=220.0;
	offset.x=220.0;
	offset.z=220.0;
#endif	

/* Calibration Calculation */
	while ( success==0 && attempt==0 && goodc<5){
		attempt++;

		if (compass_write(fd, CONFIGREGA, POSITIVEBIASCONFIG))
			continue;
		usleep(50000);
		if (compass_write(fd, CONFIGREGB, 0x60))
			if (compass_write(fd, MODEREGISTER, SINGLECONVERSION))
				continue;
		usleep(50000);
	
		ret=compass_format(fd, &atmp);
		if (ret==-3) continue;
		if (ret==-1 || ret==-2)	return -1;
		usleep(10000);

		cal[0]=fabsf(exp_x/((float)atmp.x));
		cal[1]=fabsf(exp_yz/((float)atmp.y));
		cal[2]=fabsf(exp_yz/((float)atmp.z));

		if (attempt>2 
				&& cal[0]>0.7 && cal[0]<1.35 
				&& cal[1]>0.7 && cal[1]<1.35 
				&& cal[2]>0.7 && cal[2]<1.35) {
			goodc++;
			calibration.x+=cal[0];
			calibration.y+=cal[1];
			calibration.z+=cal[2];
		} 
	}

	if (goodc>=5){
		calibration.x=calibration.x*gain_mul/goodc;
		calibration.y=calibration.y*gain_mul/goodc;
		calibration.z=calibration.z*gain_mul/goodc;
		success=1;
	}
	else{
		calibration.x=calibration.y=calibration.z=1.0;
	}

	if (compass_write(fd, CONFIGREGA, POSITIVEBIASCONFIG))
		return -2;
	usleep(50000);
	if (compass_write(fd, CONFIGREGB, 0x60))
		return -2;
	if (compass_write(fd, MODEREGISTER, CONTINUOUSCONVERSION))
		return -2;
	usleep(50000);
	return 0;
}

int compass_format(int fd, struct compass *buf){
	__s16 ac;
	__u8 buffer[6];
	buffer[0]=0x03;
	if (write(fd, buffer, 1)!=1) {
		perror("I2C pseudo-write error!");
		return -1;
	}
	if (read(fd, buffer, 6)!=6) {
		perror("I2C read error!");
		return -2;
	}
	ac=buffer[0];ac<<=8;ac+=buffer[1];
	if (ac==-4096) return -3;
	buf->x=ac;
	ac=buffer[2];ac<<=8;ac+=buffer[3];
	if (ac==-4096) return -3;
	buf->z=ac;
	ac=buffer[4];ac<<=8;ac+=buffer[5];
	if (ac==-4096) return -3;
	buf->y=ac;
	return 0;
}

int compass_handle(struct compass *buf, struct compass_field *res){
	if (his_no==0){
		his_no=1;
		int i;
		for (i=0;i<COMPASS_ACCLIMIT;i++){
			history[i].x=-buf->x;
			history[i].y=buf->y;
			history[i].z=-buf->z;
		}
		acc_mag.x=-buf->x*COMPASS_ACCLIMIT;
		acc_mag.y=buf->y*COMPASS_ACCLIMIT;
		acc_mag.z=-buf->z*COMPASS_ACCLIMIT;
		his_ind=0;

		res->y=offset.y-buf->x*calibration.x;
		res->x=offset.x-buf->y*calibration.y;
		res->z=offset.z-buf->z*calibration.z;

		return 0;
	}
	struct compass mag;
	struct compass del=history[his_ind];
	mag.x=-buf->x;
	mag.y=buf->y;
	mag.z=-buf->z;
	history[his_ind]=mag;
	his_ind++;
	his_ind%=COMPASS_ACCLIMIT;
	
	acc_mag.x+=mag.x-del.x;
	acc_mag.y+=mag.y-del.y;
	acc_mag.z+=mag.z-del.z;

	res->y=offset.y+calibration.x*(acc_mag.x/COMPASS_ACCLIMIT);
	res->x=offset.x-calibration.y*(acc_mag.y/COMPASS_ACCLIMIT);
	res->z=offset.z+calibration.z*(acc_mag.z/COMPASS_ACCLIMIT);
	return 0;
}

void compass_offset(struct compass_field *fld){
	static int index=0;
	int i;
	struct compass_field b1,b2;
	b2=*fld;

	if (!off_init){
		off_init=1;
		for (i=0;i<HISTORY_SIZE;i++){
			mag_his[i].x=b2.x-offset.x;
			mag_his[i].y=b2.y-offset.y;
			mag_his[i].z=b2.z-offset.z;
		}
	}

	b1.x=offset.x+mag_his[index].x;
	b1.y=offset.y+mag_his[index].y;
	b1.z=offset.z+mag_his[index].z;

	struct compass_field diff;
	float sq;
	float length;
	diff.x=b2.x-b1.x;
	diff.y=b2.y-b1.y;
	diff.z=b2.z-b1.z;
	length=sqrtf(diff.x*diff.x+diff.y*diff.y+diff.z*diff.z);

	if (length<50.0){
		index=(index+1)%HISTORY_SIZE;
		return;
	}
	mag_his[index].x=b2.x+0.5-offset.x;
	mag_his[index].y=b2.y+0.5-offset.y;
	mag_his[index].z=b2.z+0.5-offset.z;
	index=(index+1)%HISTORY_SIZE;

	float length1,length2,fac;
	length1=sqrtf(b1.x*b1.x+b1.y*b1.y+b1.z*b1.z);
	length2=sqrtf(b2.x*b2.x+b2.y*b2.y+b2.z*b2.z);
	fac=0.01*(length2-length1)/length;

	diff.x*=fac;
	diff.y*=fac;
	diff.z*=fac;
	length=sqrtf(diff.x*diff.x+diff.y*diff.y+diff.z*diff.z);
	if (length>10.0){
		fac=10.0/length;
		diff.x*=fac;
		diff.y*=fac;
		diff.z*=fac;
	}

	struct compass_field noff;
	noff.x=offset.x-diff.x;
	noff.y=offset.y-diff.y;
	noff.z=offset.z-diff.z;

	if (noff.x<-2000.0)
		noff.x=-2000.0;
	else if (noff.x>2000.0)
		noff.x=2000.0;
	if (noff.y<-2000.0)
		noff.y=-2000.0;
	else if (noff.y>2000.0)
		noff.y=2000.0;
	if (noff.z<-2000.0)
		noff.z=-2000.0;
	else if (noff.z>2000.0)
		noff.z=2000.0;

	offset=noff;
	return;
}

inline struct compass_field compass_get_offset(){
	return offset;
}
