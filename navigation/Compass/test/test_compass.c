#include "navigation/compass.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

#define I2C_DEV		"/dev/i2c-1"

int main() {
	int oc=0;
	int fd=open(I2C_DEV, O_RDWR);
	if (fd<0) {
		perror("i2c cannot open");;
		return -1;
	}
	if (ioctl(fd,I2C_SLAVE,COMPASS_ADDRESS)) {
		perror("failed to acquire bus");
		return -1;
	}
	usleep(100000);
	if (compass_specify(fd)){
		fprintf(stderr,"not HMC5888!\n");
		return -1;
	}
	printf("HMC5888L recognised!\n");

	if (compass_init(fd)){
		fprintf(stderr,"init failed!\n");
		return -1;
	}
	printf("init succeeded!\n");
	struct compass buf;
	struct compass_field field;
	float length;
	while(1){
		if (compass_format(fd, &buf)){
			continue;
		}
		if (compass_handle(&buf, &field)){
			continue;
		}
		oc++;
		if (oc==5){
			compass_offset(&field);
			oc=0;
		}
		length=sqrtf(field.x*field.x+field.y*field.y+field.z*field.z);
		printf("x:%f, y:%f, z:%f, l=%f\n",field.x,field.y,field.z,length);
		usleep(20000);
	}
}
