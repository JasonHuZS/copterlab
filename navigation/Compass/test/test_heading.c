#include "navigation/compass.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

#define I2C_DEV		"/dev/i2c-1"
#define PI		(3.141592653)

float matrix[3][3];
static float min[3], max[3], offset[3];
static float heading;

float calculate_heading(float (*dcm)[3], struct compass_field *fld){
	float cos_pitch_sq = 1.0f-(dcm[2][1]*dcm[2][1]);
	
	// Tilt compensated magnetic field Y component:
	float headY = fld->y * dcm[2][2] - fld->z * dcm[2][1];
	
	// Tilt compensated magnetic field X component:
	float headX=fld->x*cos_pitch_sq-dcm[2][0] * (fld->y * dcm[2][1]+fld->z*dcm[2][2]);
	
	// magnetic heading
	// 6/4/11 - added constrain to keep bad values from ruining DCM Yaw - Jason S.
	float heading=atan2f(-headY,headX);
	if (heading>3.15)
		heading=3.15;
	else if (heading<-3.15)
		heading=-3.15;
	
	return heading;
}

int main() {
	int oc=0;
	heading=0.0;
	int fd=open(I2C_DEV, O_RDWR);
	if (fd<0) {
		perror("i2c cannot open");;
		return -1;
	}
	if (ioctl(fd,I2C_SLAVE,COMPASS_ADDRESS)) {
		perror("failed to acquire bus");
		return -1;
	}
	usleep(100000);
	if (compass_specify(fd)){
		fprintf(stderr,"not HMC5888!\n");
		return -1;
	}
	printf("HMC5888L recognised!\n");

	if (compass_init(fd)){
		fprintf(stderr,"init failed!\n");
		return -1;
	}
	printf("init succeeded!\n");
	struct compass buf;
	struct compass_field field;
	float length;

	matrix[0][0]=matrix[1][1]=matrix[2][2]=1.0;
	matrix[0][1]=matrix[0][2]=0.0;
	matrix[1][0]=matrix[1][2]=0.0;
	matrix[2][0]=matrix[2][0]=0.0;

	min[0]=min[1]=min[2]=0.0;
	max[0]=max[1]=max[2]=0.0;
	offset[0]=offset[1]=offset[2]=0.0;
	while(1){
		if (compass_format(fd, &buf)){
			continue;
		}
		if (compass_handle(&buf, &field)){
			usleep(20000);
			continue;
		}
		oc++;
		if (oc==5){
			heading=calculate_heading(matrix,&field);
			compass_offset(&field);
			oc=0;

			struct compass_field mag=field;
			if( mag.x < min[0] )
				min[0] = mag.x;
			if( mag.y < min[1] )
				min[1] = mag.y;
			if( mag.z < min[2] )
				min[2] = mag.z;
			
			// capture max
			if( mag.x > max[0] )
				max[0] = mag.x;
			if( mag.y > max[1] )
				max[1] = mag.y;
			if( mag.z > max[2] )
				max[2] = mag.z;
			
			// calculate offsets
			offset[0] = -(max[0]+min[0])/2;
			offset[1] = -(max[1]+min[1])/2;
			offset[2] = -(max[2]+min[2])/2;
			
			printf("heading: %f deg.\n",heading*180/PI);
			struct compass_field off=compass_get_offset();
			printf("offset: x: %f,y: %f,z: %f.\n",off.x,off.y,off.z);
		}
		length=sqrtf(field.x*field.x+field.y*field.y+field.z*field.z);
		printf("x:%f, y:%f, z:%f, l=%f\n",field.x,field.y,field.z,length);
	}
}
