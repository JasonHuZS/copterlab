#define _BSD_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "navigation/gps_rcnsr.h"
#include "navigation/basic_types.h"
#include "wrapper/io.h"
//#define __DEBUG_GPS_RCNSR

static char buffer[BUF_SIZE];
static int offset;

int gps_init(char *port, int baudrate, int databits, int parity, int stopbits){
	int fd=wrap_uart_init(port, baudrate, databits, parity, stopbits);
	bzero(buffer, BUF_SIZE);
	offset=0;
	return fd;
}

int gps_readline(int fd, char *ret_buf){
	int head=0,tail;
	int i,c,c0;
	int last_offset;
	int len,res,flag=0;
//eliminate all blank character to find the head.
	for(c=0;c<offset;c++){
		if(buffer[c]!='\r'&&buffer[c]!='\n'&&buffer[c]!=' '&&buffer[c]!='\0')
			break;
	}
	head=c;
#ifdef __DEBUG_GPS_RCNSR
	printf("phase 1: buffer[c]=%c.\nhead=%d.\n",buffer[c],head);
#endif
//to see if there is already a line in the buffer.
	for(;c<offset;c++){
		if (buffer[c]=='\r'){
			buffer[c]='\0';
			tail=c;
#ifdef __DEBUG_GPS_RCNSR
			printf("phase 2: jump.\n");
#endif
			goto copy_phase_2;
		}
	}
#ifdef __DEBUG_GPS_RCNSR
	printf("start read.\n");
#endif
//if not, keep read from gps.
	while(1) {
		last_offset=offset;
	//read some data from gps
		res=read(fd, buffer+offset, BUF_SIZE-offset);
		if (res<0){
			if (errno==EAGAIN||errno==EINTR) continue;
			perror("read wrong");
			bzero(buffer, BUF_SIZE);
			offset=0;
			return -1;
		}else if(res==0){
#ifdef __DEBUG_GPS_RCNSR
			printf("read nothing.c0: %d.\n",c0);
#endif
			c0++;
			if (c0>300){
#ifdef __DEBUG_GPS_RCNSR
				printf("currently no available data!\n");
#endif
				return -2;
			}
			if (offset==BUF_SIZE) {
				for (c=head;c<offset;c++){
					if (buffer[c]=='\r'){
						buffer[c]='\0';
						tail=c;
						goto copy_phase_1;
					}
				}
#ifdef __DEBUG_GPS_RCNSR
				printf("buffer overflow!\n");
#endif
				bzero(buffer, BUF_SIZE);
				offset=0;
				return -1;
			}
			continue;
		}
		c0=0;
		offset=last_offset+res;
#ifdef __DEBUG_GPS_RCNSR
		printf("buffer report: [%s]\nleft: %d; read: %d\n",buffer+head,BUF_SIZE-offset,res);
#endif
	//check if a line is contained.
		for(c=last_offset;c<offset;c++){
			if (buffer[c]=='\r'){
				buffer[c]='\0';
				tail=c;
				flag=1;
				break;
			}
		}
	//if so, break.
		if (flag) break;
	}
#ifdef __DEBUG_GPS_RCNSR
	printf("loop out.\n");
#endif

copy_phase_1:
	for(c=head;c<tail;c++){
		if(buffer[c]!='\r'&&buffer[c]!='\n'&&buffer[c]!=' '&&buffer[c]!='\0')
			break;
	}
	head=c;
	
copy_phase_2:
	for(i=0;c<tail;c++,i++)
		ret_buf[i]=buffer[c];
	ret_buf[i]='\0';
	len=i;
	for(i=0;c<offset;i++,c++)
		buffer[i]=buffer[c];
	buffer[i]='\0';
	offset=i;
	return len;
}
