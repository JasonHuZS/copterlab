#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "navigation/gps_rsblr.h"

//static function groups
static int split_str(char *str, char sp, char **chparr){
	int i=1,c=0;
	chparr[0]=str;
	while (1){
		if (str[c]=='\0') break;
		if (str[c]==sp){
			str[c]='\0';
			chparr[i]=str+c+1;
			i++;
		}
		c++;
	}
	return i;
}
static char *magic_cmp(char *str, char *magic){
	if (*str=='$')
		str++;
	int i;
	for(i=0;i<5;i++)
		if (str[i]!=magic[i])
			return NULL;
	return str;
}
static __u32 parse_num(unsigned int digit, char *str){
	int i,res,d;
	res=0;
	for (i=0; i<digit; i++) {
		d=str[i]-'0';
		res*=10;
		res+=d;
	}
	return res;
}
static char *parse_utc_time(char *str, struct utc_time *time){
	__u32 s;
	if (*str=='\0'||str==NULL){
		time->h=0;
		time->m=0;
		return str+1;
	}
	time->h=parse_num(2,str);
	time->m=parse_num(2,str+2);
	s=parse_num(2,str+4);
	time->s=1000*s+parse_num(3,str+7);
	return str+10;
}
static char *parse_date(char *str, struct date *date){
	if (*str=='\0'||str==NULL){
		date->day=0;
		date->month=0;
		date->year=0;
		return str+1;
	}
	date->day=parse_num(2,str);
	date->month=parse_num(2,str+2);
	date->year=parse_num(2,str+4);
	return str+6;
}
static char *parse_latitude(char *str, struct degree *degree){
	__u32 m;
	if (*str=='\0'||str==NULL){
		degree->d=0;
		degree->m=0;
		return str+1;
	}
	degree->d=parse_num(2,str);
	m=parse_num(2,str+2);
	degree->m=10000*m+parse_num(4,str+5);
	return str+9;
}
static char *parse_longitude(char *str, struct degree *degree){
	__u32 m;
	if (*str=='\0'||str==NULL){
		degree->d=0;
		degree->m=0;
		return str+1;
	}
	degree->d=parse_num(3,str);
	m=parse_num(2,str+3);
	degree->m=10000*m+parse_num(4,str+6);
	return str+10;
}

int __parse_gpgga(char *str, struct gpggainfo *infop){
	char *sppos[15];
	split_str(str, ',', sppos);
	parse_utc_time(sppos[1],&(infop->time));
	parse_latitude(sppos[2],&(infop->pos.latitude));
	infop->pos.ns=*sppos[3];
	parse_longitude(sppos[4],&(infop->pos.longitude));
	infop->pos.ew=*sppos[5];
	infop->pos_fix=atoi(sppos[6]);
	infop->satellites=atoi(sppos[7]);
	infop->hdop=atof(sppos[8]);
	infop->altitude=atof(sppos[9]);
	infop->alt_unit=*sppos[10];
	infop->geoid=atof(sppos[11]);
	infop->geoid_unit=*sppos[12];
	infop->diff_corr=atoi(sppos[13]);
	infop->ref_id=atoi(sppos[14]);
	return 0;
}

int __parse_gpgll(char *str, struct gpgllinfo *infop){
	char *sppos[8];
	split_str(str, ',', sppos);
	parse_latitude(sppos[1],&(infop->pos.latitude));
	infop->pos.ns=*sppos[2];
	parse_longitude(sppos[3],&(infop->pos.longitude));
	infop->pos.ew=*sppos[4];
	parse_utc_time(sppos[5],&(infop->time));
	infop->status=*sppos[6];
	infop->mod=(enum mod_indct)*sppos[7];
	return 0;
}

int __parse_gpgsa(char *str, struct gpgsainfo *infop){
	char *sppos[18];
	split_str(str, ',', sppos);
	infop->mode1=*sppos[1];
	infop->mode2=*sppos[2];
	infop->ch1=atoi(sppos[3]);
	infop->ch2=atoi(sppos[4]);
	infop->ch3=atoi(sppos[5]);
	infop->ch4=atoi(sppos[6]);
	infop->ch5=atoi(sppos[7]);
	infop->ch6=atoi(sppos[8]);
	infop->ch7=atoi(sppos[9]);
	infop->ch8=atoi(sppos[10]);
	infop->ch9=atoi(sppos[11]);
	infop->ch10=atoi(sppos[12]);
	infop->ch11=atoi(sppos[13]);
	infop->ch12=atoi(sppos[14]);
	infop->pdop=atof(sppos[15]);
	infop->hdop=atof(sppos[16]);
	infop->vdop=atof(sppos[17]);
	return 0;
}
int __parse_gpgsv(char *str, struct gpgsvinfo *infop){
	char *sppos[24];
	int lim=split_str(str, ',', sppos);
	int l,i;
	infop->nmsg=atoi(sppos[1]);
	infop->msg=atoi(sppos[2]);
	lim=lim>24?24:lim;
	for (i=3,l=0;i<lim;i+=4) {
		infop->info_arr[l].id=atoi(sppos[i]);
		infop->info_arr[l].elevation=atoi(sppos[i+1]);
		infop->info_arr[l].azimuth=atoi(sppos[i+2]);
		infop->info_arr[l].snr=atoi(sppos[i+3]);
	}
	return 0;
}
int __parse_gprmc(char *str, struct gprmcinfo *infop){
	char *sppos[13];
	split_str(str, ',', sppos);
	parse_utc_time(sppos[1],&(infop->time));
	infop->status=*sppos[2];
	parse_latitude(sppos[3],&(infop->pos.latitude));
	infop->pos.ns=*sppos[4];
	parse_longitude(sppos[5],&(infop->pos.longitude));
	infop->pos.ew=*sppos[6];
	infop->speed=atof(sppos[7]);
	infop->course=atof(sppos[8]);
	parse_date(sppos[9],&(infop->date));
	infop->mag_var=atof(sppos[10]);
	infop->mag_var_dir=*sppos[11];
	infop->mod=(enum mod_indct)*sppos[12];
	return 0;
}
int __parse_gpvtg(char *str, struct gpvtginfo *infop){
	char *sppos[10];
	split_str(str, ',', sppos);
	infop->course=atof(sppos[1]);
	infop->ref=*sppos[2];
	infop->course2=atof(sppos[3]);
	infop->ref2=*sppos[4];
	infop->speed=atof(sppos[5]);
	infop->unit=*sppos[6];
	infop->speed2=atof(sppos[7]);
	infop->unit2=*sppos[8];
	infop->mod=(enum mod_indct)*sppos[9];
	return 0;
}
int __parse_gpmss(char *str, struct gpmssinfo *infop){
	char *sppos[5];
	split_str(str, ',', sppos);
	infop->sig_strength=atoi(sppos[1]);
	infop->snr=atoi(sppos[2]);
	infop->freq=atof(sppos[3]);
	infop->rate=atoi(sppos[4]);
	return 0;
}
int __parse_gpzda(char *str, struct gpzdainfo *infop){
	char *sppos[7];
	split_str(str, ',', sppos);
	parse_utc_time(sppos[1],&(infop->time));
	infop->date.day=atoi(sppos[2]);
	infop->date.month=atoi(sppos[3]);
	infop->date.year=atoi(sppos[4]);
	infop->offset=atoi(sppos[5]);
	infop->offset2=atoi(sppos[6]);
	return 0;
}
int general_parse(char *str, struct gpsinfo *infop, struct satellite_info *info_arr){
	if (infop==NULL||str==NULL)
		return -1;
	enum gps_type type=NONE;
	char *rstr=str;
	if (rstr=magic_cmp(str, GPGGA_MAGIC)) {
		type=GPGGA;
		__parse_gpgga(str, &(infop->info.gpgga));
	}
	else if (rstr=magic_cmp(str, GPGLL_MAGIC)) {
		type=GPGLL;
		__parse_gpgll(str, &(infop->info.gpgll));
	}
	else if (rstr=magic_cmp(str, GPGSA_MAGIC)) {
		type=GPGSA;
		__parse_gpgsa(str, &(infop->info.gpgsa));
	}
	else if (rstr=magic_cmp(str, GPGSV_MAGIC)) {
		type=GPGSV;
		if (info_arr==NULL) return -1;
		infop->info.gpgsv.info_arr=info_arr;
		__parse_gpgsv(str, &(infop->info.gpgsv));
	}
	else if (rstr=magic_cmp(str, GPRMC_MAGIC)) {
		type=GPRMC;
		__parse_gprmc(str, &(infop->info.gprmc));
	}
	else if (rstr=magic_cmp(str, GPVTG_MAGIC)) {
		type=GPVTG;
		__parse_gpvtg(str, &(infop->info.gpvtg));
	}
	else if (rstr=magic_cmp(str, GPMSS_MAGIC)) {
		type=GPMSS;
		__parse_gpmss(str, &(infop->info.gpmss));
	}
	else if (rstr=magic_cmp(str, GPZDA_MAGIC)) {
		type=GPZDA;
		__parse_gpzda(str, &(infop->info.gpzda));
	}else return -1;

	infop->type=type;
	return 0;
}
