#include <string.h>
#include <stdio.h>
#include "navigation/gps_updtr.h"
#include "navigation/measures.h"
#include "navigation/gps_rsblr.h"

enum gps_type gps_update(char *str, struct gps_struct *var){
	struct gpsinfo info;
	int l;
	if (var==NULL||str==NULL) return NONE;
	l=strlen(str);
	if (str[l-3]!='*') return NONE;
	str[l-3]='\0';
	if (general_parse(str,&info,NULL))
		return NONE;
	switch (info.type){
		case GPGGA:
			var->time=info.info.gpgga.time;
			var->pos=info.info.gpgga.pos;
			var->altitude=info.info.gpgga.altitude;
			var->alt_unit=info.info.gpgga.alt_unit;
			var->pos_fix=info.info.gpgga.pos_fix;
			var->satellites=info.info.gpgga.satellites;
			var->hdop=info.info.gpgga.hdop;
			break;
		case GPGLL:
			var->status=info.info.gpgll.status;
			var->pos=info.info.gpgll.pos;
			var->time=info.info.gpgll.time;
			break;
		case GPGSA:
			var->pdop=info.info.gpgsa.pdop;
			var->hdop=info.info.gpgsa.hdop;
			var->vdop=info.info.gpgsa.vdop;
			break;
		case GPGSV:
			return GPGSV;
		case GPRMC:
			var->time=info.info.gprmc.time;
			var->pos=info.info.gprmc.pos;
			var->status=info.info.gprmc.status;
			break;
		case GPVTG:
			return GPVTG;
		case GPMSS:
			return GPMSS;
		case GPZDA:
			return GPZDA;
		default:return NONE;
	}
	if (clock_gettime(CLOCK_REALTIME, &(var->stamp))){
		perror("gettime error");
		return NONE;
	}
	return info.type;
}
