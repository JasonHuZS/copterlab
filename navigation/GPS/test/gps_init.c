#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include "navigation/gps_rcnsr.h"
#define		 __DEBUG_SIRF_ONLY

/* SiRF init messages
//
// Note that we will only see a SiRF in NMEA mode if we are explicitly configured
// for NMEA.  GPS_AUTO will try to set any SiRF unit to binary mode as part of
// the autodetection process.
 */
const static char *SiRF_init_string[]={
    "$PSRF103,0,0,1,1*25\r\n",   // GGA @ 1Hz
    "$PSRF103,1,0,0,1*25\r\n",   // GLL off
    "$PSRF103,2,0,0,1*26\r\n",   // GSA off
    "$PSRF103,3,0,0,1*27\r\n",   // GSV off
    "$PSRF103,4,0,1,1*20\r\n",   // RMC off
    "$PSRF103,5,0,1,1*20\r\n",   // VTG @ 1Hz
    "$PSRF103,6,0,0,1*22\r\n",   // MSS off
    "$PSRF103,8,0,0,1*2C\r\n",   // ZDA off
    "$PSRF151,1*3F\r\n",         // WAAS on (not always supported)
    "$PSRF106,21*0F\r\n",        // datum = WGS84
    ""};

/* MediaTek init messages 
//
// Note that we may see a MediaTek in NMEA mode if we are connected to a non-DIYDrones
// MediaTek-based GPS.
 */
const static char *MTK_init_string[]={
    "$PMTK314,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n", // GGA & VTG once every fix
    "$PMTK330,0*2E\r\n",                                 // datum = WGS84
    "$PMTK313,1*2E\r\n",                                 // SBAS on
    "$PMTK301,2*2E\r\n",                                 // use SBAS data for DGPS
    ""};

/* ublox init messages 
//
// Note that we will only see a ublox in NMEA mode if we are explicitly configured
// for NMEA.  GPS_AUTO will try to set any ublox unit to binary mode as part of
// the autodetection process.
//
// We don't attempt to send $PUBX,41 as the unit must already be talking NMEA
// and we don't know the baudrate.
 */
const static char *ublox_init_string[]={
    "$PUBX,40,gga,0,1,0,0,0,0*7B\r\n",   // GGA on at one per fix
    "$PUBX,40,vtg,0,1,0,0,0,0*7F\r\n",   // VTG on at one per fix
    "$PUBX,40,rmc,0,0,0,0,0,0*67\r\n",   // RMC off (XXX suppress other message types?)
    ""};

#define BUF_SIZE	(2000)
char buf[2000];

int main() {
	int fd=gps_init("/dev/ttyUSB0",38400,8,'N',1);
	if (fd==-1) {
		perror("cannot open device!");
		return -1;
	}
	int i,l,sl,ret;
	for (i=0;i<sizeof(SiRF_init_string)/sizeof(char *);i++){
		l=strlen(SiRF_init_string[i]);
		sl=0;
		while(1) {
			ret=write(fd, SiRF_init_string[i]+sl,l-sl);
			if(ret<0) {
				perror("SiRF error");
				return -1;
			}
			sl+=ret;
			if(sl==l) break;
		}
#ifdef __DEBUG_SIRF_ONLY
		printf("SiRF: %d\n",i);
#endif
	}
#ifndef __DEBUG_SIRF_ONLY
	for (i=0;i<sizeof(MTK_init_string)/sizeof(char *);i++){
		l=strlen(MTK_init_string[i]);
		sl=0;
		while(1) {
			ret=write(fd, MTK_init_string[i]+sl,l-sl);
			if(ret<0) {
				perror("MTK error");
				return -1;
			}
			sl+=ret;
			if(sl==l) break;
		}
	}
	for (i=0;i<sizeof(ublox_init_string)/sizeof(char *);i++){
		l=strlen(ublox_init_string[i]);
		sl=0;
		while(1) {
			ret=write(fd, ublox_init_string[i]+sl,l-sl);
			if(ret<0) {
				perror("ublox error");
				return -1;
			}
			sl+=ret;
			if(sl==l) break;
		}
	}
	
	printf("change successful!\n");
#endif
	while (1) {
		bzero(buf,BUF_SIZE);
		ret=read(fd,buf,BUF_SIZE);
		if(ret<0)
			perror("read wrong!");
		else 
			printf("%s",buf);
	}
	return 0;
}
