#define _BSD_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "wrapper/tty_func.h"

#define DEVICE		"/dev/ttyS0"
#define BUF_SIZE	(2000)

char buf[2000];

int main() {
	int fd=open_and_init_tty(DEVICE);
	int c=0;
	set_baudrate(fd,38400);
	set_dataformat(fd,8,'N',1);
	swflow_ctrl(fd);

	int res=0;
	if (fd==-1) {
		perror("cannot open device!");
		return -1;
	}
	while (1) {
		bzero(buf,BUF_SIZE);
		res=read(fd,buf,BUF_SIZE);
		if(res<0)
			perror("read wrong!");
		else {
			printf("%d:\t<%s>\n",c,buf);
			c++;
		}
	}
	return 0;
}
