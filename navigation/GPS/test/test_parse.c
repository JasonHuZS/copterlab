#define _BSD_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "navigation/gps_rcnsr.h"
#include "navigation/gps_updtr.h"
#include "navigation/basic_types.h"

char buf[BUF_SIZE];

struct gps_struct gps_var;

int main() {
	int fd=gps_init("/dev/ttyUSB0",38400,8,'N',1);
	int i=0;
	enum gps_type ty;
	if (fd==-1) {
		perror("cannot open device!");
		return -1;
	}
	printf("inited.\n");
	while (1){
		if (gps_readline(fd, buf)==-2) break;
		printf("%d:\t<%s>\n",i,buf);
		i++;
		

		ty=gps_update(buf, &gps_var);
		if (ty==NONE) continue;
		printf("position:\n");
		printf("\td=%d.m=%d.%c,d=%d.m=%d.%c\n",gps_var.pos.latitude.d,gps_var.pos.latitude.m,gps_var.pos.ns,gps_var.pos.longitude.d,gps_var.pos.longitude.m,gps_var.pos.ew);
		printf("\tsatellites:%d\n",gps_var.satellites);
		printf("\tstatus:%c\n",gps_var.status);
		printf("\thdop:%f\n",gps_var.hdop);
	}
	return 0;
}
