#include <stdio.h>
#include <stdlib.h>
#include <navigation/gps_rcnsr.h>

char buf[BUF_SIZE];

int main() {
	int fd=gps_init("/dev/ttyUSB0",38400,8,'N',1);
	int i=0;
	if (fd==-1) {
		perror("open file error");
		return -1;
	}
	printf("inited.\n");
	while (1){
		if (gps_readline(fd, buf)==-2) break;
		printf("%d:\t<%s>\n",i,buf);
		printf("\tfirst: %d\n",buf[0]);
		i++;
	}
	return 0;
}
