#include "navigation/inertial.h"
#include <stdio.h>

void matrix_set_A(Mtype angle_x, Mtype angle_y, Mtype angle_z, Mtype (*A)[3][3])
{
	int i,j;
	Mtype t[3][3];
	t[0][0]=1;
	t[0][1]=-angle_z;
	t[0][2]=angle_y;
	t[1][0]=angle_z;
	t[1][1]=1;
	t[1][2]=-angle_x;
	t[2][0]=-angle_y;
	t[2][1]=angle_x;
	t[2][2]=1;
	for (i=0;i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			(*A)[i][j]=t[i][j];
		}
	}
}

void get_C_mea(Mtype J[3], Mtype K[3], Mtype (*C)[3][3])
{
	Mtype I[3];
	int i;
	cross_product(J,K,&I);
	for (i=0;i<3;i++) {(*C)[0][i]=I[i];}
	for (i=0;i<3;i++) {(*C)[1][i]=J[i];}
	for (i=0;i<3;i++) {(*C)[2][i]=K[i];}
}

void accel_calib(int fd, float (*accel_cal)[3])
{
	char ch;
	float acc_x_s,acc_y_s,acc_z_s;
	float acc_x1,acc_x2,acc_y1,acc_y2,acc_z1,acc_z2;

	printf("Please place the IMU x up\n");
	scanf("%c",&ch);
	mpu6050_accelx(fd,&acc_x_s);
	acc_x1=acc_x_s/ratio_a;
	printf("Please place the IMU x down\n");
	scanf("%c",&ch);
	mpu6050_accelx(fd,&acc_x_s);
	acc_x2=acc_x_s/ratio_a;
	printf("Please place the IMU y up\n");
	scanf("%c",&ch);
	mpu6050_accely(fd,&acc_y_s);
	acc_y1=acc_y_s/ratio_a;
	printf("Please place the IMU y down\n");
	scanf("%c",&ch);
	mpu6050_accelx(fd,&acc_y_s);
	acc_y2=acc_y_s/ratio_a;
	printf("Please place the IMU z up\n");
	scanf("%c",&ch);
	mpu6050_accelz(fd,&acc_z_s);
	acc_z1=acc_z_s/ratio_a;
	printf("Please place the IMU z down\n");
	scanf("%c",&ch);
	mpu6050_accelx(fd,&acc_z_s);
	acc_z2=acc_z_s/ratio_a;
	
	(*accel_cal)[0]=(acc_x1+acc_x2)/2;
	(*accel_cal)[1]=(acc_y1+acc_y2)/2;
	(*accel_cal)[2]=(acc_z1+acc_z2)/2;
}

void IMU_update(int fd, Mtype accel_cal[3], Mtype (*angle)[3], Mtype (*Z)[3])
{
	Mtype gyro_x,gyro_y,gyro_z,acc_x_s,acc_y_s,acc_z_s;
	mpu6050_gyrox(fd,&gyro_x);
	mpu6050_gyroy(fd,&gyro_y);
	mpu6050_gyroz(fd,&gyro_z);
	mpu6050_accelx(fd,&acc_x_s);
	mpu6050_accely(fd,&acc_y_s);
	mpu6050_accelz(fd,&acc_z_s);
		
	(*angle)[0]=gyro_x/ratio_g;
	(*angle)[1]=gyro_y/ratio_g;
	(*angle)[2]=gyro_z/ratio_g;
	(*Z)[0]=(acc_x_s/ratio_a-accel_cal[0])*1;
	(*Z)[1]=(acc_y_s/ratio_a-accel_cal[1])*1;
	(*Z)[2]=(acc_z_s/ratio_a-accel_cal[2])*1;
}

void DCM_update_Kalman(Mtype angle[3], Mtype Z[3], Mtype Comp[3], Mtype (*DCM)[3][3], Mtype (*Poss)[3][3])
{
	Mtype angle_x=angle[0],angle_y=angle[1],angle_z=angle[2];
	Mtype A[3][3],A_t[3][3];
	Mtype Cr[3][3],Cm[3][3];
	Mtype Pr[3][3],Pm[3][3],Pm_temp[3][3],Kg[3][3],Kg_temp[3][3];
	Mtype C_mea[3][3],C_mea_temp[3][3],C_temp[3][3],Cr_t[3][3];
	Mtype Q[3][3],R[3][3],I[3][3];
	int i,j;
	
	//Value transportation
	//matrix_copy(*DCM,&Cr);
	//matrix_copy(*Poss,&Pr);
	for (i=0;i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			Cr[i][j]=(*DCM)[i][j];
			Pr[i][j]=(*Poss)[i][j];
		}
	}

	matrix_set_A(angle_x,angle_y,angle_z,&A);
	matrix_multiply_3x3(A,Cr,&Cm);
	matrix_transform(A,&A_t);	
	matrix_multiply_3x3(Pr,A_t,&Pm);
	matrix_multiply_3x3(A,Pm,&Pm);	

	matrix_init(&Q);
	matrix_numMul(Q,0.00001,&Q);
	matrix_add_3x3(Pm,Q,&Pm);

	matrix_init(&R);
	matrix_numMul(R,0.00001,&R);
	matrix_add_3x3(Pm,R,&Pm_temp);
	matrix_reverse(Pm_temp,&Pm_temp);

	matrix_multiply_3x3(Pm,Pm_temp,&Kg);
	
	get_C_mea(Comp,Z,&C_mea);
	matrix_subtract_3x3(C_mea,Cm,&C_mea_temp);
	matrix_multiply_3x3(Kg,C_mea_temp,&C_temp);
	//For test
	matrix_zero(&C_temp);
	//
	matrix_add_3x3(Cm,C_temp,&Cr);
	
	matrix_init(&I);
	matrix_subtract_3x3(I,Kg,&Kg_temp);
	matrix_multiply_3x3(Kg_temp,Pm,&Pr);	
	
	//Value Transportation
	for (i=0;i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			(*DCM)[i][j]=Cr[i][j];
			(*Poss)[i][j]=Pr[i][j];
		}
	}
}
