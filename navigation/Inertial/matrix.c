#include "navigation/matrix.h"
#include <stdio.h>

void matrix_multiply_3x1(Mtype x[3][3], Mtype y[3], Mtype (*z)[3])
{
	Mtype t[3];
	int i;
	t[0]=x[0][0]*y[0]+x[0][1]*y[1]+x[0][2]*y[2];
	t[1]=x[1][0]*y[0]+x[1][1]*y[1]+x[1][2]*y[2];
	t[2]=x[2][0]*y[0]+x[2][1]*y[1]+x[2][2]*y[2];
	for (i=0;i<3;i++)
	{
		(*z)[i]=t[i];
	}
}

void matrix_multiply_3x3(Mtype x[3][3], Mtype y[3][3], Mtype (*z)[3][3])
{
	Mtype t[3][3];
	int i,j;
	t[0][0]=x[0][0]*y[0][0]+x[0][1]*y[1][0]+x[0][2]*y[2][0];
	t[0][1]=x[0][0]*y[0][1]+x[0][1]*y[1][1]+x[0][2]*y[2][1];
	t[0][2]=x[0][0]*y[0][2]+x[0][1]*y[1][2]+x[0][2]*y[2][2];
	t[1][0]=x[1][0]*y[0][0]+x[1][1]*y[1][0]+x[1][2]*y[2][0];
	t[1][1]=x[1][0]*y[0][1]+x[1][1]*y[1][1]+x[1][2]*y[2][1];
	t[1][2]=x[1][0]*y[0][2]+x[1][1]*y[1][2]+x[1][2]*y[2][2];
	t[2][0]=x[2][0]*y[0][0]+x[2][1]*y[1][0]+x[2][2]*y[2][0];
	t[2][1]=x[2][0]*y[0][1]+x[2][1]*y[1][1]+x[2][2]*y[2][1];
	t[2][2]=x[2][0]*y[0][2]+x[2][1]*y[1][2]+x[2][2]*y[2][2];
	for (i=0;i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			(*z)[i][j]=t[i][j];
		}
	}
}

void matrix_numMul(Mtype x[3][3], Mtype num, Mtype (*z)[3][3])
{
	int i,j;
	for (i=0;i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			(*z)[i][j]=x[i][j]*num;
		}
	}
}
	
void matrix_transform(Mtype x[3][3], Mtype (*z)[3][3])
{
	Mtype t[3][3];
	int i,j;
	t[0][0]=x[0][0];
	t[0][1]=x[1][0];
	t[0][2]=x[2][0];
	t[1][0]=x[0][1];
	t[1][1]=x[1][1];
	t[1][2]=x[2][1];
	t[2][0]=x[0][2];
	t[2][1]=x[1][2];
	t[2][2]=x[2][2];
	for (i=0;i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			(*z)[i][j]=t[i][j];
		}
	}
}

void matrix_add_3x1(Mtype x[3], Mtype y[3], Mtype (*z)[3])
{
	int i;
	for (i=0;i<3;i++)
	{
		(*z)[i]=x[i]+y[i];
	}
}

void matrix_add_3x3(Mtype x[3][3], Mtype y[3][3], Mtype (*z)[3][3])
{
	int i,j;
	for (i=0;i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			(*z)[i][j]=x[i][j]+y[i][j];
		}
	}
}

void matrix_subtract_3x1(Mtype x[3], Mtype y[3], Mtype (*z)[3])
{
	int i;
	for (i=0;i<3;i++)
	{
		(*z)[i]=x[i]-y[i];
	}
}

void matrix_subtract_3x3(Mtype x[3][3], Mtype y[3][3], Mtype (*z)[3][3])
{
	int i,j;
	for (i=0;i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			(*z)[i][j]=x[i][j]-y[i][j];
		}
	}
}

void matrix_init(Mtype (*Q)[3][3])
{
	int i,j;
	Mtype t[3][3];
	t[0][0]=1;
	t[0][1]=0;
	t[0][2]=0;
	t[1][0]=0;
	t[1][1]=1;
	t[1][2]=0;
	t[2][0]=0;
	t[2][1]=0;
	t[2][2]=1;
	for (i=0;i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			(*Q)[i][j]=t[i][j];
		}
	}
}

void matrix_reverse(Mtype x[3][3], Mtype (*z)[3][3])
{
	Mtype def;
	Mtype t[3][3];
	int i,j;
	def=x[0][0]*x[1][1]*x[2][2] - x[0][0]*x[1][2]*x[2][1] - x[0][1]*x[1][0]*x[2][2] + x[0][1]*x[1][2]*x[2][0] + x[0][2]*x[1][0]*x[2][1] - x[0][2]*x[1][1]*x[2][0];
	if (def==0) 
		def=1;
	t[0][0]=(x[1][1]*x[2][2]-x[1][2]*x[2][1])/def;
	t[0][1]=(x[0][2]*x[2][1]-x[0][1]*x[2][2])/def;
	t[0][2]=(x[0][1]*x[1][2]-x[0][2]*x[1][1])/def;
	t[1][0]=(x[1][2]*x[2][0]-x[1][0]*x[2][2])/def;
	t[1][1]=(x[0][0]*x[2][2]-x[0][2]*x[2][0])/def;
	t[1][2]=(x[0][2]*x[1][0]-x[0][0]*x[1][2])/def;
	t[2][0]=(x[1][0]*x[2][1]-x[1][1]*x[2][0])/def;
	t[2][1]=(x[0][1]*x[2][0]-x[0][0]*x[2][1])/def;
	t[2][2]=(x[0][0]*x[1][1]-x[0][1]*x[1][0])/def;
	for (i=0;i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			(*z)[i][j]=t[i][j];
		}
	}
}

void cross_product(Mtype A[3], Mtype B[3], Mtype (*z)[3])
{
	int i;
	Mtype t[3];
	t[0]=A[1]*B[2]-A[2]*B[1];
	t[1]=A[2]*B[0]-A[0]*B[2];
	t[2]=A[0]*B[1]-A[1]*B[0];
	for (i=0;i<3;i++) {(*z)[i]=t[i];}
}

void matrix_copy(Mtype A[3][3], Mtype (*B)[3][3])
{
	int i,j;
	for (i=0;i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			(*B)[i][j]=A[i][j];
		}
	}
}

void matrix_zero(Mtype (*A)[3][3])
{
	int i,j;
	for (i=0;i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			(*A)[3][3]=0;
		}
	}
}
