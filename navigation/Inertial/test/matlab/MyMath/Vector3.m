v1=[0,0,0];
v2=[1,2,3];
v3=[4,5,6];
disp(['vec 1: ',num2str(v1)]);
disp(['vec 2: ',num2str(v2)]);
disp(['vec 3: ',num2str(v3)]);

disp(['size of vec2: ',num2str(sqrt(dot(v2,v2)))]);
v2=v2/norm(v2);
v3=v3/norm(v3);
disp(['normalized vec2: ',num2str(v2)]);
disp(['dot product: ',num2str(dot(v2,v3))]);
v4=cross(v2,v3);
disp(['cross product: ',num2str(v4)]);
disp(['neg :',num2str(-v4)]);