import MyMath.Vector3 as vec;
Vector3=vec.Vector3

import MyMath.Matrix3 as mat;
Matrix3=mat.Matrix3

class DCM:
	def __init__(self, mat=Matrix3(), dt=0.001, wg=0.5, wa=0.5):
		self.dcm=mat
		self.dt=dt
		self.wg=Vector3()
		self.va=Vector3()
		self.weight_g=wg
		self.weight_a=wa

	def update_vec(self, accel):
		self.va+=accel*dt
		return self.va

	def correction(self):
		err=self.dcm.a*self.dcm.b/2
		a1=self.dcm.a-err*self.dcm.b
		b1=self.dcm.b-err*self.dcm.a
		a2=a1.normalize()
		b2=b1.normalize()
		c2=a2%b2
		self.dcm=Matrix3(a2,b2,c2)

	def fine_tune(self, delta):
		self.dcm.a=self.a+delta%self.a
		self.dcm.b=self.b+delta%self.b
		self.dcm.c=self.c+delta%self.c

	def delta_accel(self):
		return self.dt*(self.dcm.c%self.va)
	
	def update(self, gyro, accel):
		dthg=gyro*self.dt
		dtha=self.delta_accel()
		dth=(dthg*self.weight_g+dtha*weight_a)/(weight_a+weight_g)
		self.update_vec(accel)
		self.fine_tune(dth)
		self.correction()

