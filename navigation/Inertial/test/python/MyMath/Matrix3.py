if __name__=='__main__':
        from Vector3 import *
else:
        from .Vector3 import *

class Matrix3:
	def __init__(self,
			veca=Vector3(1,0,0),
			vecb=Vector3(0,1,0),
			vecc=Vector3(0,0,1)):
		self.a=veca
		self.b=vecb
		self.c=vecc

	def __str__(self):
		return  "a:\n\t"+str(self.a)+\
			"\nb:\n\t"+str(self.b)+\
			"\nc:\n\t"+str(self.c)
	
	def __add__(self, mat):
		return Matrix3( Vector3(self.a.x+mat.a.x,
					self.a.y+mat.a.y,
					self.a.z+mat.a.z),
				Vector3(self.b.x+mbt.b.x,
					self.b.y+mbt.b.y,
					self.b.z+mbt.b.z),
				Vector3(self.c.x+mct.c.x,
					self.c.y+mct.c.y,
					self.c.z+mct.c.z))
	__radd__=__add__

	def __neg__(self):
		return Matrix3( Vector3(-self.a.x,-self.a.y,-self.a.z),
				Vector3(-self.b.x,-self.b.y,-self.b.z),
				Vector3(-self.c.x,-self.c.y,-self.c.z))

	def __sub__(self, mat):
		return self+mat.__neg__

	def __mul__(self, mat):
	        if isinstance(mat, Vector3):
	            v = mat
	            return Vector3(self.a.x * v.x + self.a.y * v.y + self.a.z * v.z,
	                           self.b.x * v.x + self.b.y * v.y + self.b.z * v.z,
	                           self.c.x * v.x + self.c.y * v.y + self.c.z * v.z)
	        elif isinstance(mat, Matrix3):
	            m = mat
	            return Matrix3(Vector3(self.a.x * m.a.x + self.a.y * m.b.x + self.a.z * m.c.x,
	                                   self.a.x * m.a.y + self.a.y * m.b.y + self.a.z * m.c.y,
	                                   self.a.x * m.a.z + self.a.y * m.b.z + self.a.z * m.c.z),
	                           Vector3(self.b.x * m.a.x + self.b.y * m.b.x + self.b.z * m.c.x,
	                                   self.b.x * m.a.y + self.b.y * m.b.y + self.b.z * m.c.y,
	                                   self.b.x * m.a.z + self.b.y * m.b.z + self.b.z * m.c.z),
	                           Vector3(self.c.x * m.a.x + self.c.y * m.b.x + self.c.z * m.c.x,
	                                   self.c.x * m.a.y + self.c.y * m.b.y + self.c.z * m.c.y,
	                                   self.c.x * m.a.z + self.c.y * m.b.z + self.c.z * m.c.z))
	        v = mat
		return Matrix3(self.a * v, self.b * v, self.c * v)

	def __div__(self, num):
		return Matrix3(self.a/num,self.b/num,self.c/num)

	def __copy__(self):
		return Matrix3(self.a.copy(),self.b.copy(),self.c.copy())
	copy=__copy__

	def normalize(self):
		return Matrix3( self.a.normalize(), self.b.normalize(), self.c.normalize())

	def indent(self):
		return Matrix3();

	def transpose(self):
		return Matrix3( Vector3(self.a.x,self.b.x,self.c.x),
				Vector3(self.a.y,self.b.y,self.c.y),
				Vector3(self.a.z,self.b.z,self.c.z))
	def det(self):
		return  self.a.x*self.b.y*self.c.z+\
				self.a.y*self.b.z*self.c.x+\
				self.a.z*self.b.x*self.c.y-\
				self.a.z*self.b.y*self.c.x-\
				self.a.y*self.b.x*self.c.z-\
				self.a.x*self.b.z*self.c.y	
	
	def eye(self):
		self=Matrix3()
		return self

	def zeros(self):
		self=Matrix3(   Vector3(0,0,0),
				Vector3(0,0,0),
				Vector3(0,0,0))

def _test():
	i=Matrix3()
	a=Matrix3( Vector3(1,2,3),
		   Vector3(4,5,6),
		   Vector3(7,8,9))
	print("identity: "+str(i))
	print("a: "+str(a))
	b=a.transpose()
	print("b=a^T: \n"+str(b))
	c=b.normalize()
	print("c=b.normalize(): \n"+str(c))
	d=a*c;
	print("d=a*c: \n"+str(d))
	print("d.det(): \n\t"+str(d.det()))
	return

if __name__=='__main__':
	_test()
