from math import sqrt

class Vector3:
	def __init__(self, x=0, y=0, z=0):
		self.x=float(x)
		self.y=float(y)
		self.z=float(z)

	def __str__(self):
		return "x=%.4f, y=%.4f, z=%.4f"%(self.x,self.y,self.z)

	def __add__(self, vec):
		return Vector3( self.x+vec.x, 
				self.y+vec.y,
				self.z+vec.z)
	__radd__=__add__

	def __ladd__(self, vec):
		self.x+=vec.x
		self.y+=vec.y
		self.z+=vec.z

	def __sub__(self, vec):
		return Vector3( self.x-vec.x, 
				self.y-vec.y,
				self.z-vec.z)
	
	def inner_product(self, vec):
		return self.x*vec.x+\
			self.y*vec.y+\
			self.z*vec.z
	
	def cross_product(self, vec):
		return Vector3( self.y*vec.z-self.z*vec.y,
				self.z*vec.x-self.x*vec.z,
				self.x*vec.y-self.y*vec.x)
	
	def __mul__(self, st):
		if isinstance(st, Vector3):
			return self.inner_product(st)
		return Vector3(self.x*st,self.y*st,self.z*st)

	__mod__=cross_product

	def __div__(self, num):
		return Vector3( self.x/num, self.y/num, self.z/num)
	
	def __neg__(self):
		return Vector3(-self.x, -self.y, -self.z)

	def __copy__(self):
		return Vector3(self.x, self.y, self.z)
	copy=__copy__

	def size(self):
		return sqrt(self.x**2+self.y**2+self.z**2)

	def normalize(self):
		size=self.size()
		return self/size

	def clear(self):
		self.x=0
		self.y=0
		self.z=0
		return self

def _test():
	v1=Vector3()
	v2=Vector3(1,2,3)
	v3=Vector3(4,5,6)
	print("vector 1: "+str(v1))
	print("vector 2: "+str(v2))
	print("vector 3: "+str(v3))
	print("size of vec2: "+str(v2.size()))
	v2=v2.normalize()
	print("normalized vec2: "+str(v2))
	v3=v3.normalize()
	print("dot product: "+str(v2*v3))
	print("cross product: "+str(v2%v3))
	v4=v2%v3
	print("neg: "+str(-v4))
	v4.clear()
	print("clear: "+str(v4))	

if __name__ == '__main__':
	_test()
