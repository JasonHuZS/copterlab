#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include "navigation/inertial.h"
#include "navigation/matrix.h"
#include "navigation/mpu6050.h"

#define I2C_DEV		"/dev/i2c-1"

struct timespec to,tn;

int main()
{
	int fd=open(I2C_DEV, O_RDWR);
	int count=0;
	int i,j;
	Mtype angle[3],Z[3],comp[3]={0,0,0},accel_cal[3]={0,0,0};
	Mtype G_ef[3]={0,0,g},G_bf[3];
	Mtype DCM[3][3]={{1,0,0},{0,1,0},{0,0,1}},DCM_t[3][3];
	Mtype Poss[3][3]={{0,0,0},{0,0,0},{0,0,0}};
	float timed;

	if (fd<0) {
		perror("i2c cannot open");
		return -1;
	}
	
	mpu6050_init(fd);
	//accel_calib(fd,&accel_cal);
	
	clock_gettime(CLOCK_REALTIME, &tn);
	
	while (1)
	{
		if(clock_gettime(CLOCK_REALTIME, &to)<0)
		printf("Wrong!");
		
		IMU_update(fd,accel_cal,&angle,&Z);
		DCM_update_Kalman(angle,Z,comp,&DCM,&Poss);

		matrix_transform(DCM,&DCM_t);
		matrix_multiply_3x1(DCM_t,G_ef,&G_bf);
		if (count==100)
		{	
			printf("%f\t%f\t%f\n",G_bf[0],Z[0],accel_cal[0]);
			printf("%f\t%f\t%f\n",G_bf[1],Z[1],accel_cal[1]);
			printf("%f\t%f\t%f\n",G_bf[2],Z[2],accel_cal[2]);
			printf("\n");
			count=0;
			printf("%f\n",timed);
		}
		count++;
		if (clock_gettime(CLOCK_REALTIME, &tn)<0)
		printf("Wrong!");
		timed=0;
		timed=(tn.tv_sec-to.tv_sec)*1000000;
		if (timed<0) timed+=1000000;
		timed+=(tn.tv_nsec-to.tv_nsec)/1000;
		usleep(10000-timed);
	}
	return 0;
}
