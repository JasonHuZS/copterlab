#include "navigation/mpu6050.h"
#include <sys/ioctl.h>

int mpu6050_init(int fd) {
	__u8 buf[2];
	if (ioctl(fd,I2C_SLAVE,SLV_ADDR)) {
		perror("failed to acquire bus");
		return -1;
	}
	if (mpu6050_write(fd, PWR_MGMT_1, 0x00))
		return -1;
	if (mpu6050_write(fd, SMPLRT_DIV, 0x07))
		return -1;
	if (mpu6050_write(fd, CONFIG, 0x06))
		return -1;
	if (mpu6050_write(fd, GYRO_CONFIG, 0x18))
		return -1;
	if (mpu6050_write(fd, ACCEL_CONFIG, 0x01))
		return -1;
	return 0;
}

inline int mpu6050_write(int fd, __u8 cmd, __u8 data) {
	__u8 buf[2];
	buf[0]=cmd;
	buf[1]=data;
	if (write(fd, buf, 2)!=2){
		perror("I2C write error!");
		return -1;
	}
	return 0;
}
inline __s16 mpu6050_read(int fd, __u8 cmd) {
	__u8 buf[2];
	buf[0]=cmd;
	if (write(fd, buf, 1)!=1) {
		perror("I2C pseudo-write error!");
		return -1;
	}
	if (read(fd, buf, 1)!=1) {
		perror("I2C read error!");
		return -2;
	}
	return buf[0];
}

int mpu6050_accelx(int fd, float *acc){
	__s16 ac;
	__u8 buf[2];
	buf[0]=ACCEL_XOUT_H;
	if (write(fd, buf, 1)!=1) {
		perror("I2C pseudo-write error!");
		return -1;
	}
	if (read(fd, buf, 2)!=2) {
		perror("I2C read error!");
		return -2;
	}
	ac=buf[0];ac<<=8;ac+=buf[1];
	//printf("buf: 0x%2x%2x, ac= %d\n",buf[0],buf[1],ac);
	*acc=(float)ac;
	return 0;
}
int mpu6050_accely(int fd, float *acc){
	__s16 ac;
	__u8 buf[2];
	buf[0]=ACCEL_YOUT_H;
	if (write(fd, buf, 1)!=1) {
		perror("I2C pseudo-write error!");
		return -1;
	}
	if (read(fd, buf, 2)!=2) {
		perror("I2C read error!");
		return -2;
	}
	ac=buf[0];ac<<=8;ac+=buf[1];
	*acc=(float)ac;
	return 0;
}
int mpu6050_accelz(int fd, float *acc){
	__s16 ac;
	__u8 buf[2];
	buf[0]=ACCEL_ZOUT_H;
	if (write(fd, buf, 1)!=1) {
		perror("I2C pseudo-write error!");
		return -1;
	}
	if (read(fd, buf, 2)!=2) {
		perror("I2C read error!");
		return -2;
	}
	ac=buf[0];ac<<=8;ac+=buf[1];
	*acc=(float)ac;
	return 0;
}
int mpu6050_gyrox(int fd, float *acc){
	__s16 ac;
	__u8 buf[2];
	buf[0]=GYRO_XOUT_H;
	if (write(fd, buf, 1)!=1) {
		perror("I2C pseudo-write error!");
		return -1;
	}
	if (read(fd, buf, 2)!=2) {
		perror("I2C read error!");
		return -2;
	}
	ac=buf[0];ac<<=8;ac+=buf[1];
	*acc=(float)ac;
	return 0;
}
int mpu6050_gyroy(int fd, float *acc){
	__s16 ac;
	__u8 buf[2];
	buf[0]=GYRO_YOUT_H;
	if (write(fd, buf, 1)!=1) {
		perror("I2C pseudo-write error!");
		return -1;
	}
	if (read(fd, buf, 2)!=2) {
		perror("I2C read error!");
		return -2;
	}
	ac=buf[0];ac<<=8;ac+=buf[1];
	*acc=(float)ac;
	return 0;
}
int mpu6050_gyroz(int fd, float *acc){
	__s16 ac;
	__u8 buf[2];
	buf[0]=GYRO_ZOUT_H;
	if (write(fd, buf, 1)!=1) {
		perror("I2C pseudo-write error!");
		return -1;
	}
	if (read(fd, buf, 2)!=2) {
		perror("I2C read error!");
		return -2;
	}
	ac=buf[0];ac<<=8;ac+=buf[1];
	*acc=(float)ac;
	return 0;
}
int mpu6050_fetch(int fd, struct mpu6050_data *data){
	if (!mpu6050_accelx(fd,&(data->accelx)))
		return -1;
	if (!mpu6050_accely(fd,&(data->accelx)))
		return -1;
	if (!mpu6050_accelz(fd,&(data->accelx)))
		return -1;
	if (!mpu6050_gyrox(fd,&(data->accelx)))
		return -1;
	if (!mpu6050_gyroy(fd,&(data->accelx)))
		return -1;
	if (!mpu6050_gyroz(fd,&(data->accelx)))
		return -1;
}
