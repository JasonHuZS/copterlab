#include <stdio.h>
#include <stdlib.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "navigation/mpu6050.h"

#define I2C_DEV		"/dev/i2c-1"

int main() {
	int fd=open(I2C_DEV, O_RDWR);
	__s16 buf;

	if (fd<0) {
		perror("i2c cannot open");;
		return -1;
	}
	if (ioctl(fd,I2C_SLAVE,SLV_ADDR)) {
		perror("failed to acquire bus");
		return -1;
	}
	mpu6050_init(fd);

	while (1) {
		buf=mpu6050_read(fd,ACCEL_XOUT_H);
		printf("0x%2x\n",buf&0xff);
		sleep(1);
	}

	return 0;
}
