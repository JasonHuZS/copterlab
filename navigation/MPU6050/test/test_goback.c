#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include "navigation/mpu6050.h"

#define I2C_DEV		"/dev/i2c-1"
struct timespec to,tn;
struct mpu6050_data data;
float cood;

int main() {
	int fd=open(I2C_DEV, O_RDWR);
	if (fd<0) {
		perror("i2c cannot open");;
		return -1;
	}

	float timed,dist,speed;
	__u8 buf[2];
	__s16 tmp;
	cood=0.0;
	speed=0.0;
	mpu6050_init(fd);

	clock_gettime(CLOCK_REALTIME, &tn);
	while(1) {
		to=tn;
		clock_gettime(CLOCK_REALTIME, &tn);

		mpu6050_fetch(fd, &data);

		buf[0]=ACCEL_XOUT_H;
		write(fd,buf,1);
		read(fd,buf,2);
		tmp=buf[0];
		tmp<<=8;
		tmp+=buf[1];
	//	data.accelx=(float)tmp;

		timed=tn.tv_sec-to.tv_sec;
		timed+=(float)(tn.tv_nsec-to.tv_nsec)/1000000000;
		data.accelx/=1638.4;
		dist=0.5*data.accelx*timed*timed+speed*timed;
		speed+=timed*data.accelx;
		cood+=dist;
		printf("accelx: %f, cood: %f, speed: %f, timeval: %f\n",data.accelx,cood,speed,timed);

		usleep(100000);
	}
	return 0;
}
