#include <stdio.h>
#include <unistd.h>
#include "navigation/ultrasonic.h"

char *filename="/dev/ttyACM0";

int main(){
	FILE *fp=obsavd_init(filename);
	if (fp==NULL){
		printf("wrong!\n");
		return -1;
	}
#if 1
	if (send_general_command(fp, detect_down, 0x72)){
		return -1;
	}
	usleep(100000);
#endif
	printf("opened!\n");
	while (1){
#if 1
		if (detect_obstacle(fp, detect_down)){
			printf("wrong2!\n");
			sleep(1);
			continue;
		}
#else
		if (send_general_command(fp, detect_down, 0xB8)){
			printf("wrong2!\n");
			sleep(1);
			continue;
		}
#endif
		printf("start detect!\n");
		usleep(87000);
		int pos;
		pos=read_position(fp, detect_down);
		printf("result: %d.\n", pos);
		if (pos<0){
			printf("wrong3!\n");
			sleep(1);
			continue;
		}
		usleep(500000);
	}
}
