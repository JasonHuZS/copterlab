#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#include "wrapper/io.h"
#include "navigation/ultrasonic.h"

static const char detect_cmd[]="AT+I>2,02B8\r\n";
static const char addr_cmd[]="AT+I=00,%02x\r\n";
static const char get_cmd[]="AT+I&2,02\r\n";
static char uls_buf[100];
int addr[]={
	DOWN_ADDR, 
	LEFT_ADDR, RIGHT_ADDR,
	FRONT_ADDR, BACK_ADDR
};

static void response_strip(char *response){
	int len=strlen(response);
	int i;
	for (i=0;i<len;i++)
		if (response[i]=='\r'||response[i]=='\n'){
			response[i]='\0';
			return;
		}
	return;
			
}

FILE *obsavd_init(char *filename){
	int fd=wrap_uart_init(filename, 115200, 8, 'N', 1);
	if (fd<0){
		perror("obsavd file open error");
		return NULL;
	}
	char *no_echo="AT+X=0\n\r";
	FILE *fp=fdopen(fd, "r+");
	if (fp==NULL)
		return NULL;
#ifdef __DEBUG_SHOW_REPONSE
	fprintf(fp, no_echo);
	fgets(uls_buf, 100, fp);
	printf(uls_buf);
	printf("----------------\n");
	if (uls_buf[0]=='A'){
		fgets(uls_buf, 100, fp);
		printf(uls_buf);
		printf("----------------\n");
		fgets(uls_buf, 100, fp);
		printf(uls_buf);
		printf("----------------\n");
	}
#else
	if (fprintf(fp, no_echo)<=0){
		fprintf(stderr, "no_echo write error!\n");
		return NULL;
	}
	if (fgets(uls_buf, 100, fp)==NULL){
		fprintf(stderr, "get response error!\n");
		return NULL;
	}
	if (uls_buf[0]=='A'){
		if (fgets(uls_buf, 100, fp)==NULL){
			fprintf(stderr, "get response error!\n");
			return NULL;
		}
		if (fgets(uls_buf, 100, fp)==NULL){
			fprintf(stderr, "get response error!\n");
			return NULL;
		}
	}
#endif
	return fp;
}

int set_device(FILE *fp, detect_dir_t direction){
	int to=addr[direction];
	int ret=fprintf(fp, addr_cmd, to<<1);
	if (ret<=0){
		fprintf(stderr, "detect_obstacle write error!\n");
		return -1;
	}
	if (fgets(uls_buf, 100, fp)==NULL){
		fprintf(stderr, "detect_obstacle read from file error!\n");
		return -2;
	}
	response_strip(uls_buf);
	if (strcmp(uls_buf+3, "FAIL")==0){
		fprintf(stderr, "set failed!\n");
		return -3;
	}
	return 0;
}

int send_general_command(FILE *fp, detect_dir_t direction, unsigned short cmd){
	int ret=set_device(fp, direction);
	if (ret)
		return ret;
	
	if (fprintf(fp, "AT+I>2,02%02x\r\n", cmd)<=0){
		fprintf(stderr, "send command error:0x%02x.\\n", cmd);
		return -1;
	}
	if (fgets(uls_buf, 100, fp)==NULL){
		fprintf(stderr, "detect_obstacle read from file error!\n");
		return -2;
	}

#ifdef __DEBUG_SHOW_REPONSE
	printf(uls_buf);
#endif

	return 0;
}

int detect_obstacle(FILE *fp, detect_dir_t direction){
	int ret=set_device(fp, direction);
	if (ret)
		return ret;

	ret=fprintf(fp, detect_cmd);
	if (ret<=0){
		fprintf(stderr, "detect_obstacle write error!\n");
		return -1;
	}
	if (fgets(uls_buf, 100, fp)==NULL){
		fprintf(stderr, "detect_obstacle read from file error!\n");
		return -2;
	}
#ifdef __DEBUG_SHOW_REPONSE
	printf(uls_buf);
#endif
	response_strip(uls_buf);
	if (strcmp(uls_buf+3, "FAIL")==0){
		fprintf(stderr, "set failed!\n");
		return -3;
	}

	return 0;
}

int read_position(FILE *fp, detect_dir_t direction){
	int ret=set_device(fp, direction);
	int result=0;
	if (ret)
		return ret;

	ret=fprintf(fp, get_cmd);
	if (ret<=0){
		fprintf(stderr, "detect_obstacle write error!\n");
		return -1;
	}
	if (fgets(uls_buf, 100, fp)==NULL){
		fprintf(stderr, "detect_obstacle read from file error!\n");
		return -2;
	}
#ifdef __DEBUG_SHOW_REPONSE
	printf(uls_buf);
#endif
	response_strip(uls_buf);
	if (strcmp(uls_buf+3, "FAIL")==0){
		fprintf(stderr, "set failed!\n");
		return -3;
	}

	int i;
	for (i=0;i<4;i++){
		switch(uls_buf[4+i]){
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				result*=16;
				result+=uls_buf[4+i]-'0';
				break;
			case 'A':
			case 'B':
			case 'C':
			case 'D':
			case 'E':
			case 'F':
				result*=16;
				result+=uls_buf[4+i]-'A'+10;
				break;
			default:
				return -4;
		}
	}
	return result;
}
