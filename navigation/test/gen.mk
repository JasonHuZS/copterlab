%.o:
	@echo CC:	$(basename $@).c -o $@
	@${CC} ${CFLAGS} -c -o $@ $(filter %.c, $^)
test_fork.o: test_fork.c ../../include/wrapper/ipc.h ../../include/wrapper/proc.h 
test_sched.o: test_sched.c ../../include/navigation/gps_updtr.h ../../include/navigation/basic_types.h ../../include/navigation/measures.h ../../include/navigation/gps_rcnsr.h ../../include/navigation/compass.h ../../include/wrapper/io.h ../../include/wrapper/ipc.h ../../include/wrapper/proc.h 
test_withnetwork.o: test_withnetwork.c ../../include/navigation/gps_updtr.h ../../include/navigation/basic_types.h ../../include/navigation/measures.h ../../include/navigation/gps_rcnsr.h ../../include/navigation/comm_apm.h ../../include/wrapper/ipc.h ../../include/wrapper/proc.h ../../include/wrapper/socket.h ../../include/wrapper/threading.h ../../include/wrapper/ipc.h ../../include/networking/ctrl.h ../../include/navigation/protocol.h 
test_compgps.o: test_compgps.c ../../include/navigation/gps_updtr.h ../../include/navigation/basic_types.h ../../include/navigation/measures.h ../../include/navigation/gps_rcnsr.h ../../include/navigation/compass.h ../../include/wrapper/io.h ../../include/wrapper/ipc.h ../../include/wrapper/proc.h 
test_combine.o: test_combine.c ../../include/navigation/gps_updtr.h ../../include/navigation/basic_types.h ../../include/navigation/measures.h ../../include/navigation/gps_rcnsr.h ../../include/navigation/compass.h ../../include/wrapper/io.h ../../include/navigation/inertial.h ../../include/navigation/mpu6050.h ../../include/navigation/matrix.h ../../include/navigation/matrix.h ../../include/navigation/mpu6050.h ../../include/wrapper/io.h ../../include/wrapper/ipc.h ../../include/wrapper/proc.h 

