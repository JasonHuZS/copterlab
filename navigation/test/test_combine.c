#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sched.h>
#include <errno.h>
#include <time.h>
#include <sys/wait.h>

#include "navigation/gps_updtr.h"
#include "navigation/compass.h"
#include "navigation/inertial.h"
#include "navigation/matrix.h"
#include "navigation/mpu6050.h"
#include "wrapper/ipc.h"
#include "wrapper/proc.h"

#define I2C_DEV			"/dev/i2c-1"

int do_gps(void *args);
int do_navi(void *args);
static struct {
	sem_t sem;
	struct gps_struct gps;
} *shrmem;

#define MISSION_NUM		(2)
struct mission mission_list[MISSION_NUM]={
	{.go=do_gps,  .args=NULL},
	{.go=do_navi, .args=NULL}
};

int main() {
	pid_t pid;
	int i;

	shrmem=wrap_anonmmap(1);
	if (shrmem==MAP_FAILED){
		fprintf(stderr, "mmap error!\n");
		return -1;
	}
	if (wrap_anonmutex(&shrmem->sem)){
		return -1;
	}
	if (wrap_semwait(&shrmem->sem)){
		return -1;
	}
	
	for (i=0;i<MISSION_NUM;i++){
		pid=wrap_fork();
		if (pid==0)
			do_mission(&mission_list[i]);
		else
			mission_list[i].pid=pid;
	}
	pid_t my_pid=getpid();
	int status;
	printf("inside of parent: %d.\n",my_pid);
	usleep(10000);
	if (wrap_waitall())
		fprintf(stderr, "unhandlable error occurs!\n");
	sem_destroy(&shrmem->sem);
	if (wrap_munmap(shrmem, 1)){
		fprintf(stderr, "munmap error!\n");
		return -1;
	}
	return 0;
}

int do_gps(void *args){
	int fd=gps_init("/dev/ttyUSB0",38400,8,'N',1);
	enum gps_type ty;
	char buf[BUF_SIZE];
	if (fd==-1) {
		perror("cannot open device!");
		exit(-1);
	}
	printf("gps inited.\n");
	sem_post(&shrmem->sem);
	struct gps_struct gps_var;
	while (1){
		if (gps_readline(fd, buf)==-2){
			fprintf(stderr, "child 2 exited abnormally!\n");
			exit(-1);
		}

		ty=gps_update(buf, &gps_var);
		wrap_semwait(&shrmem->sem);
		shrmem->gps=gps_var;
		sem_post(&shrmem->sem);
	}
	exit(0);
}

int do_navi(void *args){
	struct sched_param sp;
	int oc=0;
	sp.__sched_priority=1;
	if (sched_setscheduler(0, SCHED_FIFO, &sp)){
		if (errno==EINVAL){
			perror("setscheduler param wrong");
			exit(-1);
		}
		else{
			perror("unknown error!");
			exit(-2);
		}
	}
	int fd=open(I2C_DEV, O_RDWR);
	if (fd<0) {
		perror("i2c cannot open");;
		exit(-1);
	}
	if (ioctl(fd,I2C_SLAVE,COMPASS_ADDRESS)) {
		perror("failed to acquire bus");
		exit(-1);
	}
	usleep(100000);
	if (compass_specify(fd)){
		fprintf(stderr,"not HMC5888!\n");
		exit(-1);
	}
	printf("HMC5888L recognised!\n");

	if (compass_init(fd)){
		fprintf(stderr,"init failed!\n");
		exit(-1);
	}
	printf("compass init succeeded!\n");

	struct compass buf;
	struct compass_field field;
	struct gps_struct gps_var;
	while(1){
		if (compass_format(fd, &buf)){

		}
		if (compass_handle(&buf, &field)){
		}
		oc++;
		if (oc==5){
			oc=0;
			compass_offset(&field);
		}
		printf("compass:\n");
		printf("\tx:%f, y:%f, z:%f\n",field.x,field.y,field.z);
	}
	
	exit(0);
}
