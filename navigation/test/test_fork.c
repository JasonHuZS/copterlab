#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/sem.h>

#include "wrapper/ipc.h"
#include "wrapper/proc.h"

static pid_t chpid[2];
static void *shrmem;

void child1();
void child2();

int main() {
	pid_t pid;

	shrmem=wrap_anonmmap(1);
	if (shrmem==MAP_FAILED){
		fprintf(stderr, "mmap error!\n");
		return -1;
	}
	pid=wrap_fork();
	if (pid>0){
		pid_t my_pid=getpid();
		chpid[0]=pid;
		pid=wrap_fork();
		if (pid==0) child1();
		chpid[1]=pid;
		int status;
		printf("inside of parent: %d.\n",my_pid);
		usleep(1000);
wait_child:
		while ((pid=wait(&status))>0)
			if (WIFEXITED(status))
				printf("\tchild %d exited.\n", pid);
			else
				printf("\tchild %d exited abnormally.\n", pid);
		if (errno==EINTR){
			fprintf(stderr,"interrupted!\n");
			goto wait_child;
		}
		else if (errno==ECHILD)
			printf("no child left.\n");
		else
			fprintf(stderr,"unknown status of waitpid() return!\n");
			
	}else //inside of child
		child2();
	if (wrap_munmap(shrmem, 1)){
		fprintf(stderr, "munmap error!\n");
		return -1;
	}
	return 0;
}

void child1(){
	printf("in child 1's view: %d, %d.\n", chpid[0], chpid[1]);
	int *arp=(int *)shrmem;
	int i;
	for(i=0; i<10; i++){
		printf("child 1-%2d: %d\n",i ,arp[i]);
		arp[i]=i;
	}
	usleep(1000);
	for(i=0; i<10; i++)
		printf("child 1-%2d: %d\n",i ,arp[i]);
	exit(0);
}

void child2(){
	printf("in child 2's view: %d, %d.\n", chpid[0], chpid[1]);
	int *arp=(int *)shrmem;
	int i;
	for(i=9; i>=0; i--){
		printf("child 2-%2d: %d\n",i ,arp[i]);
		arp[i]=10-i;
	}
	usleep(1000);
	for(i=9; i>=0; i--)
		printf("child 2-%2d: %d\n",i ,arp[i]);
	exit(0);
}
