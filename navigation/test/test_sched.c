#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sched.h>
#include <errno.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/sem.h>

#include "navigation/gps_updtr.h"
#include "navigation/compass.h"
#include "wrapper/ipc.h"
#include "wrapper/proc.h"

#define I2C_DEV		"/dev/i2c-1"

static pid_t chpid[2];
static struct {
	sem_t sem;
	struct gps_struct gps;
} *shrmem;

void child1();
void child2();

int main() {
	pid_t pid;

	shrmem=wrap_anonmmap(1);
	if (shrmem==MAP_FAILED){
		fprintf(stderr, "mmap error!\n");
		return -1;
	}
	if (wrap_anonmutex(&shrmem->sem)){
		return -1;
	}
	if (wrap_semwait(&shrmem->sem)){
		return -1;
	}
	
	pid=wrap_fork();
	if (pid>0){
		chpid[0]=pid;
		pid=wrap_fork();
		if (pid==0) child1();
		pid_t my_pid=getpid();
		chpid[1]=pid;
		int status;
		printf("inside of parent: %d.\n",my_pid);
		usleep(10000);
		if (wrap_waitall())
			fprintf(stderr, "unhandlable error occurs!\n");
	}else //inside of child
		child2();
	sem_destroy(&shrmem->sem);
	if (wrap_munmap(shrmem, 1)){
		fprintf(stderr, "munmap error!\n");
		return -1;
	}
	return 0;
}

void child1(){
	struct sched_param sp;
	sp.__sched_priority=1;
	if (sched_setscheduler(0, SCHED_FIFO, &sp)){
		if (errno==EINVAL){
			perror("setscheduler param wrong");
			exit(-1);
		}
		else{
			perror("unknown error!");
			exit(-2);
		}
	}
	int fd=open(I2C_DEV, O_RDWR);
	if (fd<0) {
		perror("i2c cannot open");;
		exit(-1);
	}
	if (ioctl(fd,I2C_SLAVE,COMPASS_ADDRESS)) {
		perror("failed to acquire bus");
		exit(-1);
	}
	usleep(100000);
	if (compass_specify(fd)){
		fprintf(stderr,"not HMC5888!\n");
		exit(-1);
	}
	printf("HMC5888L recognised!\n");

	if (compass_init(fd)){
		fprintf(stderr,"init failed!\n");
		exit(-1);
	}
	printf("compass init succeeded!\n");

	struct compass buf;
	struct compass_field field;
	struct gps_struct gps_var;
	while(1){
		if (compass_format(fd, &buf)){
			continue;
		}
		if (compass_handle(&buf, &field)){
			usleep(200000);
			continue;
		}
		wrap_semwait(&shrmem->sem);
		gps_var=shrmem->gps;
		sem_post(&shrmem->sem);
		printf("compass:\n");
		printf("\tx:%f, y:%f, z:%f\n",field.x,field.y,field.z);
		printf("gps:\n");
		printf("\td=%d.m=%d.%c,d=%d.m=%d.%c\n",gps_var.pos.latitude.d,gps_var.pos.latitude.m,gps_var.pos.ns,gps_var.pos.longitude.d,gps_var.pos.longitude.m,gps_var.pos.ew);
		printf("\tsatellites:%d\n",gps_var.satellites);
		printf("\tstatus:%c\n",gps_var.status);
		printf("\thdop:%f\n",gps_var.hdop);
	}
	
	exit(0);
}

void child2(){
	int fd=gps_init("/dev/ttyUSB0",38400,8,'N',1);
	enum gps_type ty;
	char buf[BUF_SIZE];
	if (fd==-1) {
		perror("cannot open device!");
		exit(-1);
	}
	printf("gps inited.\n");
	sem_post(&shrmem->sem);
	struct gps_struct gps_var;
	while (1){
		if (gps_readline(fd, buf)==-2){
			fprintf(stderr, "child 2 exited abnormally!\n");
			exit(-1);
		}

		wrap_semwait(&shrmem->sem);
		ty=gps_update(buf, &shrmem->gps);
		sem_post(&shrmem->sem);
	}
	exit(0);
}
