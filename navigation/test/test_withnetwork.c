#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "navigation/gps_updtr.h"
#include "navigation/comm_apm.h"
#include "wrapper/ipc.h"
#include "wrapper/proc.h"
#include "wrapper/socket.h"
#include "wrapper/threading.h"
#include "networking/ctrl.h"

#define MISSION_NUM		(2)
#define SERVER_SERVICE		"192.168.1.124:10001"
#define CTRL_SERVER		"192.168.1.124:10001"
#define DEVICE			"/dev/ttyUSB0"

pthread_t ctrl_thrd;
ctrl_args_t ctrl_args;

int do_comm(void *args);
int do_client(void *args);
static struct {
	sem_t sem;
	struct gps_struct gps;
} *shrmem;

struct mission mission_list[MISSION_NUM]={
	STATIC_MISSION(do_comm, NULL),
	STATIC_MISSION(do_client, NULL)
};
int main() {
	pid_t pid;
	int i;

	shrmem=wrap_anonmmap(1);
	if (shrmem==MAP_FAILED){
		fprintf(stderr, "mmap error!\n");
		return -1;
	}
	if (wrap_anonmutex(&shrmem->sem)){
		return -1;
	}
	if (wrap_semwait(&shrmem->sem)){
		return -1;
	}
	
	for (i=0;i<MISSION_NUM;i++){
		pid=wrap_fork();
		if (pid==0)
			do_mission(&mission_list[i]);
		else
			mission_list[i].pid=pid;
	}
	pid_t my_pid=getpid();
	int status;
	printf("inside of parent: %d.\n",my_pid);
	usleep(10000);
	if (wrap_waitall())
		fprintf(stderr, "unhandlable error occurs!\n");
	sem_destroy(&shrmem->sem);
	if (wrap_munmap(shrmem, 1)){
		fprintf(stderr, "munmap error!\n");
		return -1;
	}
	return 0;
}

struct communication comm1;
int comm_fd;
char buf[20];
void sighandler(int sig, siginfo_t *info, void *none){
	int cmd=info->si_value.sival_int;

	int instr=PRO_INSTR(cmd);
	int num=PRO_NUM(cmd);
	printf("intr: \t%d. num: \t%d.\n", instr, num);
/*
	switch (instr){
		case YAW_INSTR:
			set_yaw(&comm1, num);
			commond_write(comm_fd, comm1, buf); 
			break;
		case ROLL_INSTR:
			set_roll(&comm1, num);
			commond_write(comm_fd, comm1, buf); 
			break;
		case PITCH_INSTR:
			set_pitch(&comm1, num);
			commond_write(comm_fd, comm1, buf); 
			break;
		case THROT_INSTR:
			set_throttle(&comm1, num);
			commond_write(comm_fd, comm1, buf); 
			break;
		default: break;
	}
	*/
}

int do_comm(void *args){
        /*comm_fd=wrap_uart_init(DEVICE,57600,8,'N',1);
	if (comm_fd<0){
		return -1;
	}*/

	struct sigaction act;
	reset_radio(&comm1);

	sigemptyset(&act.sa_mask);
	act.sa_sigaction=sighandler;
	act.sa_flags=SA_SIGINFO | SA_RESTART;

	sigaction(SIGRTMIN+2, &act, NULL);

	while(1){
		pause();
	}

	return 0;
}
int do_client(void *args){
	ctrl_args.server_info=CTRL_SERVER;
	ctrl_args.ctrl_proc=mission_list[0].pid;
	ctrl_args.sig=SIGRTMIN+2;

	printf("client: %d.\n", ctrl_args.ctrl_proc);

	if (wrap_thrd_create(&ctrl_thrd, ctrl_plane_start, (void *)&ctrl_args)){
		fprintf(stderr, "create thread error!\n");
		return -1;
	}

	void *retval;
	wrap_thrd_join(ctrl_thrd, &retval);

	printf("return value:\t%d.\n", (int)retval);
	return 0;
}
int back_main(){
	struct gps_struct gps;
	gps.pos_fix=PPS;
	gps.pos.latitude.d=10;
	gps.pos.latitude.m=234567;
	gps.pos.longitude.d=20;
	gps.pos.longitude.m=597352;
	gps.pos.ew='e';
	gps.pos.ns='s';
	gps.altitude=10.0;
	gps.alt_unit='m';
	gps.satellites=3;
	gps.hdop=10.0;
	gps.pdop=20.0;
	gps.vdop=30.0;
	gps.speed=5.0;
	gps.unit='m';
	gps.status='A';
	gps.time.h=1;
	gps.time.m=20;
	gps.time.s=40;
	gps.stamp.tv_nsec=0;
	gps.stamp.tv_sec=0;

	int sock=wrap_client(SERVER_SERVICE);
	if (sock<0)
		return -1;

	ssize_t res=wrap_send(sock, &gps, sizeof(gps), 0);
	if (res<0){
		perror("send error");
		return -1;
	}
	close(sock);
	return 0;
}
