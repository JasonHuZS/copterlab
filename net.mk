#ftp related
USER=cubie
PASS=cubie
IP=192.168.1.101
TARGET_BASE=/home/cubie/quadcopter
DO_FTP=1

#version control related
submit:
	@make svn_submit

#svn related
SVN_SERVER=svn://10.92.13.7/copterlab

svn_checkout:
	@${SCRIPT_DIR}/svn_checkout.sh ${SVN_SERVER}

svn_submit:
	@${SCRIPT_DIR}/svn_submit.sh
