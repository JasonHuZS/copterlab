#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#include "wrapper/socket.h"
#include "wrapper/threading.h"
#include "navigation/protocol.h"
#include "networking/ctrl.h"

//#define __DEBUG_SHOW_RECV

static char ctrl_buf[100];

void *ctrl_plane_start(void *args){
	ctrl_args_t *cargs=(ctrl_args_t *)args;
	char *ip_port=cargs->server_info;

	printf("control plane starts connecting: %s.\n", ip_port);
	int fd=wrap_client(ip_port);
	int ret;
	union sigval val;
	if (fd<0)
		return (void *)(-1);

	printf("control plane connected.\n");
	printf("target pid: %d, sig: %d.\n",cargs->ctrl_proc, cargs->sig);
	
	while (1){
#ifdef __DEBUG_SHOW_RECV
		printf("start recieving...\n");
#endif
		ret=wrap_recv(fd, ctrl_buf, 4, 0);
		if (ret!=4){
			fprintf(stderr, "control plane error: instr error!\n");
			continue;
		}

#ifndef __DEBUG_SHOW_RECV
		val.sival_int=*(int *)ctrl_buf;
		sigqueue(cargs->ctrl_proc, cargs->sig, val);
#else
		val.sival_int=*(int *)ctrl_buf;
		printf("%d, %d, %d, %d\n", ctrl_buf[0], ctrl_buf[1], ctrl_buf[2], ctrl_buf[3]);
		printf("command: \t%d.\n", val.sival_int);
		printf("agent: \t%d.\n", PRO_AGENT(val.sival_int));
		printf("instr: \t%d.\n", PRO_INSTR(val.sival_int));
		printf("number: \t%d.\n", PRO_NUM(val.sival_int));
#endif
	}
	return NULL;
}
