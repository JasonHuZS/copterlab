#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include "wrapper/threading.h"
#include "wrapper/socket.h"
#include "navigation/gps_updtr.h"
#include "networking/data.h"

static pthread_mutex_t data_mtx=PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t data_cond=PTHREAD_COND_INITIALIZER;

void *data_plane_start(void *args){
	char *ip_port=((data_args_t *)args)->server_info;
	data_buf_t *data=((data_args_t *)args)->data;
	struct timespec slp;

	int fd=wrap_client(ip_port);
	int ret;
	if (fd<0)
		return (void *)(-1);

	slp.tv_sec=1;
	slp.tv_nsec=0;
	while (1){
		struct gps_struct gps_copy;
		if (data->gps_sent==0){
			wrap_semwait(&data->gps_sem);
			gps_copy=data->gps;
			data->gps_sent=1;
			wrap_sempost(&data->gps_sem);

			if (wrap_send(fd, GPS, 3, 0)==-1 || 
			wrap_send(fd, &gps_copy, sizeof(struct gps_struct), 0)==-1){
				fprintf(stderr, "gps sent wrong, error may occur on server side!\n");
			}
		}
		//more

		//then sleep
		pthread_mutex_lock(&data_mtx);
		pthread_mutex_timedwait(&data_cond, &data_mtx, &slp);
		pthread_mutex_unlock(&data_mtx);
	}
	return NULL;
}
