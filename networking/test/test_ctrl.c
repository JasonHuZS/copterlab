#include <stdio.h>
#include "wrapper/threading.h"
#include "networking/ctrl.h"

#define CTRL_SERVER		"192.168.1.124:10001"

pthread_t ctrl_thrd;
ctrl_args_t args;

int main(){
	args.server_info=CTRL_SERVER;
	args.ctrl_proc=0;
	args.sig=0;

	if (wrap_thrd_create(&ctrl_thrd, ctrl_plane_start, (void *)&args)){
		fprintf(stderr, "create thread error!\n");
		return -1;
	}

	void *retval;
	wrap_thrd_join(ctrl_thrd, &retval);

	printf("return value:\t%d.\n", (int)retval);
	return 0;
}
