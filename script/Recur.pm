package Recur;
use strict;
use warnings;
use File::Spec;
require Exporter;

our @ISA=qw(Exporter);
our @EXPORT=qw(filter filterout scanfolders scanfiles);

sub filter {
	my ($routine,@arr)=@_;
	my @ret=();
	foreach (@arr){
		push @ret,$_ if $routine->($_);
	}
	return @ret;
}

sub filterout {
	my ($routine,@arr)=@_;
	my @ret=();
	foreach (@arr){
		push @ret,$_ unless $routine->($_);
	}
	return @ret;
}

sub __scanfolders {
	my ($path,$container,$level)=@_;
	opendir my $dh,$path or warn "cannot open dir($path): $!\n";
	my @subs=readdir $dh;
	my @subf=filter(sub{return ($_[0] ne '.')&&($_[0] ne '..')&&(-d File::Spec->catfile($path,$_[0]))},@subs);
	push @$container,File::Spec->catdir($level,$_) foreach @subf;
	__scanfolders(File::Spec->catdir($path,$_),$container,File::Spec->catdir($level,$_)) foreach (@subf);
	closedir $dh;
}

sub scanfolders {
	my $path=shift;
	my @container=();
	__scanfolders($path,\@container,'.');
	return @container;
}

sub scanfiles{
	my $path=shift;
	my @subf=('.');
	my @container=();
	scanfolders($path,\@subf);
	foreach my $dir(@subf){
		opendir my $dh,$dir or die "cannot open dir($dir): $!\n";
		push @container,File::Spec->catfile($dir,$_)
			foreach filter(sub{
					return -f File::Spec->catfile($dir,$_[0]);
				},readdir $dh);
		closedir $dh;
	}
	return @container;
}

1;
__END__
