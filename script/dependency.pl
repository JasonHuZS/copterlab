#!/usr/bin/perl
use strict;
use warnings;
use Cwd 'abs_path';
use File::Spec;
use File::Basename;
use Data::Dumper;

die "too few arguments!!\n" if @ARGV<2;
my ($projdir,@objdir)=@ARGV;
$projdir=abs_path($projdir);
my $inc=File::Spec->catdir($projdir,'include');
my %hfile=();

#
#	PHASE ONE
#  scan out all the h files in include folder and its subfolders
#
sub filter {
	my ($routine,@arr)=@_;
	my @ret=();
	foreach (@arr){
		push @ret,$_ if $routine->($_);
	}
	return @ret;
}
sub scan_c {
	my ($path,$dist,$cfile)=@_;
	opendir my $dh,$path or warn "cannot open dir: $!\n";
	map {$cfile->{$1}=0 if m/(.*)\.c$/i} readdir $dh;
	close $dh;
}
sub scan_h {
	my ($path,$dist,$hfile)=@_;
	opendir my $dh,$path or warn "cannot open dir($path): $!\n";
	map {$hfile->{File::Spec->catfile($dist,$1)}=[] if m/(.*)\.h$/i} readdir $dh;
	close $dh;
}
sub subfolders {
	my ($path,$container,$level)=@_;
	opendir my $dh,$path or warn "cannot open dir($path): $!\n";
	my @subs=readdir $dh;
	my @subf=filter(sub{return ($_[0] ne '.')&&($_[0] ne '..')
			&&($_[0] ne '.svn')&&(-d File::Spec->catfile($path,$_[0]))},@subs);
	push @$container,File::Spec->catdir($level,$_) foreach @subf;
	subfolders(File::Spec->catdir($path,$_),$container,File::Spec->catdir($level,$_)) foreach (@subf);
	closedir $dh;
}
my @incdir=('include');
subfolders($inc,\@incdir,'include');
scan_h(File::Spec->catdir($projdir,$_),$_,\%hfile) foreach @incdir;

#
#	PHASE TWO
#  calculate h-file dependency in include folder and its subfolders
#
foreach my $h(keys %hfile){
	my $path=File::Spec->catfile($projdir,$h.'.h');
	open my $hfh,$path or die "cannot open file(".$path."): $!\n";
	while(<$hfh>){
		chomp;
		next unless m/^\#include \"(.*)\"/;
		my $match=$1;
		my ($base)=($match=~m/(.*)\.h/);
		if (exists $hfile{File::Spec->catfile('include',$base)}){
			push @{$hfile{$h}},$base;
		}
		else {
			warn $base.'.h: '."file does not exists!\n";
		}
	}
	close $hfh;
}

#
#	PHASE THREE
#  iterate to scan c&h files in object folders.
#
foreach my $obj(@objdir){
	my @subdir=($obj);
	subfolders(File::Spec->catdir($projdir,$obj),\@subdir,$obj);

	foreach my $subobj(@subdir){
		my @level=();
		my $rel=$subobj;
		$rel=~s/(\\|\/)$//;
		push @level,'..' foreach File::Spec->splitdir($rel);
		my $dist=File::Spec->catdir(@level);
		my (%cfile,%ohfile);
		my @insubdir=($subobj);

		my $path=File::Spec->catdir($projdir,$subobj);
		print "scaning folder: ".$path."\n";
		subfolders($path,\@insubdir,$subobj);
		scan_h(File::Spec->catdir($projdir,$_),$_,\%ohfile) foreach @insubdir;
		scan_c($path,'',\%cfile);
#
#	PHASE FOUR
#  calculate c file dependency.
#
#		print Dumper(\@insubdir,\%hfile,\%ohfile);
		open my $mkfh,'>',File::Spec->catfile($path,'gen.mk');
		print $mkfh "%.o:\n";
		print $mkfh "\t\@echo CC:	\$(basename \$@).c -o \$@\n";
		print $mkfh "\t\@\${CC} \${CFLAGS} -c -o \$@ \$(filter %.c, \$^)\n";
		foreach my $c(keys %cfile){
			print $mkfh $c.'.o: '.$c.'.c ';
			my $cpath=File::Spec->catfile($projdir,$subobj,$c.'.c');
			open my $cfh,$cpath or die "cannot open file(".$cpath."): $!\n";
			while(<$cfh>){
				chomp;
				next unless m/^\#include \"(.*)\"/;
				my $match=$1;
				my ($base)=($match=~m/(.*)\.h/);
				if (exists $ohfile{File::Spec->catfile($subobj,$base)}){
					print $mkfh "$base.h ";
					print $mkfh "$base.o " if (exists $cfile{$base}&& $c ne $base)
				}elsif (exists $hfile{File::Spec->catfile('include',$base)}) {
					print $mkfh File::Spec->catfile($dist,'include',$base.'.h ');
					foreach (@{$hfile{File::Spec->catfile('include',$base)}}){
						print $mkfh File::Spec->catfile($dist,'include',$_.'.h ');
						print $mkfh "$_.o " if (exists $cfile{$_});
					}
					print $mkfh "$base.o " if (exists $cfile{$base}&& $c ne $base);
				}else {
					warn $base.'.h: '."file does not exists!\n";
				}
			}
			print $mkfh "\n";
		}
		print $mkfh "\n";
		close $mkfh;
	}
}
