#!/usr/bin/perl
use warnings;
use strict;
use Cwd 'abs_path';
use File::Spec;

my ($from,$to)=@ARGV;
$from=abs_path($from);
$to=abs_path($to);
my @from=File::Spec->splitdir($from);
my @to=File::Spec->splitdir($to);
my $i=0;
my $lim=@from>@to?scalar @to:scalar @from;
for(;$i<$lim;$i++){
	last if ($from[$i] ne $to[$i]);
}
my @dist=();
push @dist,'..' foreach splice @from,$i;
push @dist,splice @to,$i;
print File::Spec->catdir(@dist);
