#!/bin/bash

cd Main
valid_conf=`cat .conf_list | sed -n -e '/^__DEBUG/p'`
echo -n > .config

for conf in $valid_conf; do 
	echo -n $conf ?\(Y/N\)
	while [ 1 -eq 1 ]
	do
		read -n 1 choose
		echo
		case $choose in
		[Yy]) echo $conf >> .config
		break;;
		[Nn])break;;
		*)echo -n unknow input! please choose again\(Y/N\):
		;;
		esac
	done
done
