#!/usr/bin/perl
use strict;
use warnings;

#format: ./ftp.pl /home/me/dir1 you@192.168.1.100:/home/you/dir2 password
my ($from, $to, $pass)=@ARGV;

#print join ', ',@_;
my ($usr, $others)=split '@',$to;
my ($ip, $tof)=($others=~m/([^:]*)(:.*)?/);
$tof=~s/:([^:]*)/$1/;
my $cmd='';
$cmd.="open $ip\n";
$cmd.="user $usr $pass\n";
$cmd.="binary\n";
$cmd.="lcd $from\n";
$cmd.="cd $tof\n";
$cmd.="prompt\n";
$cmd.="mput *\n";
$cmd.="close\n";
$cmd.="bye\n";
my $ftp="ftp -n<<THIS\n".$cmd.
	"THIS";
my $back=`$ftp`;
exit -1 if $back=~/not connected/i;
exit -1 if $back=~/refused/i;
exit 0
