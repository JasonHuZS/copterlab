#!/bin/bash

server_addr=$1

echo checkout will rewrite all the files to the lastest version on server, are you sure to do that? \(Y/N\)
read response

case $response in
	[Yy]) cd .. && svn checkout $server_addr
	exit 0;;
	[Nn]) echo user abort.
	exit 0;;
	*) echo unknown input, exceptional abort.
	exit 0;;
esac
