#!/bin/bash

echo do some cleaning job...
make clean_all >> /dev/null

add_list=`svn status | sed -n -e "s/^?\s\++\?\s\+\(.*\)$/\1/p"`
del_list=`svn status | sed -n -e "s/^!\s\+\(.*\)$/\1/p"`

for entry in $add_list; do 
	echo svn add $entry
	svn add $entry
done

for entry in $del_list; do
	echo svn delete $entry
	svn delete $entry
done

echo updating current project...
svn update
echo

echo all modifications are right? \(Y/N\)
read ret
case $ret in
	[Yy]) 
	echo please type in your log:
	read logging
	svn commit -m "$logging"
	make tags
	svn update
	exit 0;;
	[Nn]) echo user abort.
	exit 0;;
	*) echo unknow input, exceptional abort.
	exit 0;;
esac
