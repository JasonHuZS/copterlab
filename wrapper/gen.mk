%.o:
	@echo CC:	$(basename $@).c -o $@
	@${CC} ${CFLAGS} -c -o $@ $(filter %.c, $^)
proc.o: proc.c ../include/wrapper/proc.h 
ipc.o: ipc.c ../include/wrapper/ipc.h 
fdmgmt.o: fdmgmt.c ../include/wrapper/fdmgmt.h 
threading.o: threading.c ../include/wrapper/threading.h ../include/wrapper/ipc.h 
io.o: io.c ../include/wrapper/io.h ../include/wrapper/tty_func.h 
tty_func.o: tty_func.c ../include/wrapper/tty_func.h 
socket.o: socket.c ../include/wrapper/socket.h 

