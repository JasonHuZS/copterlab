#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include "wrapper/io.h"

#define RW_TRY_LIMIT		(10)

int wrap_uart_init(char *port, int baudrate, int datab, int parity, int stopb){
	int fd=open_and_init_tty(port);
	if (fd==-1) {
		perror("cannot open device");
		return fd;
	}
	set_baudrate(fd,baudrate);
	set_dataformat(fd,datab,parity,stopb);
	swflow_ctrl(fd);
	return fd;
}

inline void wrap_uart_close(int fd){
	close(fd);
}

inline int wrap_i2c_write(int fd, __u8 cmd, __u8 data) {
	__u8 buf[2];
	buf[0]=cmd;
	buf[1]=data;
	if (write(fd, buf, 2)!=2){
		perror("I2C write error!");
		return -1;
	}
	return 0;
}

inline __s16 wrap_i2c_read(int fd, __u8 cmd) {
	__u8 buf[2];
	buf[0]=cmd;
	if (write(fd, buf, 1)!=1) {
		perror("I2C pseudo-write error!");
		return -1;
	}
	if (read(fd, buf, 1)!=1) {
		perror("I2C read error!");
		return -2;
	}
	return buf[0];
}

ssize_t wrap_read(int fd, void *buf, size_t len){
	int ret, total=0;
	int tried=0;

	while (total<len){
		ret=read(fd, buf+total, len-total);
		if (ret<0){
			if (errno==EAGAIN||errno==EINTR) continue;
			perror("read wrapper");
			return -1;
		}
		else if (ret==0){	//for robustness of io read
			tried++;
			if (tried>=RW_TRY_LIMIT)
				break;
		}
		total+=ret;
	}
	return total;
}

ssize_t wrap_write(int fd, const void *buf, size_t len){
	int ret, total=0;
	int tried=0;

	while (total<len){
		ret=write(fd, buf+total, len-total);
		if (ret<0){
			if (errno==EAGAIN||errno==EINTR) continue;
			if (errno==ENOSPC){
				perror("write wrapper");
				break;
			}
			perror("write wrapper");
			return -1;
		}
		else if (ret==0){	//for robustness of io read
			tried++;
			if (tried>=RW_TRY_LIMIT)
				break;
		}
		total+=ret;
	}
	return total;
}
