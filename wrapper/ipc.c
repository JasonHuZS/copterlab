#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/mman.h>
#include "wrapper/ipc.h"

#define MMAP_TRY_LIMIT		(5)
#define POSIX_SEM_TRY_LIMIT	(5)

void *wrap_namedmmap(int fd, unsigned int pages){
	if (pages==0){
		errno=EINVAL;
		return MAP_FAILED;
	}
	int try=0;
	size_t page=sysconf(_SC_PAGESIZE);
	size_t len=page*pages;
	off_t ret=lseek(fd, (off_t)len, SEEK_SET);
	if (ret==(off_t)-1){
		int tmperr=errno;
		perror("size too large to descriped");
		errno=tmperr;
		return MAP_FAILED;
	}
	void *shrmem;
try_namedmmap:
	shrmem=mmap(NULL, len, PROT_READ | PROT_WRITE,
			MAP_SHARED, fd, 0);
	if (shrmem==MAP_FAILED){
		int tmperr=errno;
		switch (tmperr){
			case EAGAIN:try++;
				    if(try<MMAP_TRY_LIMIT) goto try_namedmmap;
				    break;
			case ENOMEM: fprintf(stderr, "no enough memory!\n");
				     break;
			default:break;
		}
		perror("wrap_anonmmap failed!");
		errno=tmperr;
		close(fd);
		return MAP_FAILED;
	}
	close(fd);
	return shrmem;
}

void *wrap_anonmmap(unsigned int pages){
	if (pages==0){
		errno=EINVAL;
		return MAP_FAILED;
	}
	int try=0;
	size_t page=sysconf(_SC_PAGESIZE);
	void *shrmem;
try_anonmmap:
	shrmem=mmap(NULL, pages*page, PROT_READ | PROT_WRITE,
			MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	if (shrmem==MAP_FAILED){
		int tmperr=errno;
		switch (tmperr){
			case EAGAIN: try++;
				     if (try<MMAP_TRY_LIMIT) goto try_anonmmap;
				     break;
			case ENOMEM: fprintf(stderr, "no enough memory!\n");
				     break;
			default:break;
		}
		perror("wrap_anonmmap failed!");
		errno=tmperr;
		return MAP_FAILED;
	}
	return shrmem;
}

inline int wrap_munmap(void *addr, unsigned int pages){
	size_t page=sysconf(_SC_PAGESIZE);
	int ret=munmap(addr, pages*page);
	return ret;
}

sem_t *wrap_namedsem(char *name, int val){
	if (name==NULL) return SEM_FAILED;
	int try=0;
	int len;
	sem_t *sem;
try_namedsem:
	len=sprintf(name, "tmp%d",try);
	try++;
	sem=sem_open(name, O_CREAT | O_EXCL, 0666, val);
	if (sem==SEM_FAILED){
		if (errno==EEXIST){
			if (try<POSIX_SEM_TRY_LIMIT)
				    goto try_namedsem;
		}
		return SEM_FAILED;
	}
	return sem;
}

int wrap_anonsem(sem_t *sem, int val){
	int res=sem_init(sem, 1, val);
	if (res==-1){
		int tmperr=errno;
		perror("anonsem init error");
		errno=tmperr;
		return -1;
	}
	return 0;
}

inline int wrap_anonmutex(sem_t *sem){
	return wrap_anonsem(sem, 1);
}

inline int wrap_semdestroy(sem_t *sem){
	return sem_destroy(sem);
}

int wrap_semwait(sem_t *sem){
	int ret;
	while ((ret=sem_wait(sem))==-1){
		if (errno!=EINTR) return -1;
	}
	return 0;
}

inline int wrap_sempost(sem_t *sem){
	return sem_post(sem);
}
