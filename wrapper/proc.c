#include <stdlib.h>
#include <errno.h>
#include "wrapper/proc.h"

void do_mission(mission_t *mission_p){
	if (mission_p==NULL)
		exit(INVAL_MISSION);
	if (mission_p->go==NULL)
		exit(INVAL_MISSION);
	int ret=exec_mission(mission_p);
	exit(ret);
}

int do_rip(int state){
	return 0;
}
void do_undertaking(mission_t *mission_p, int state){
	if (mission_p==NULL)
		exit(INVAL_MISSION);
	if (mission_p->undertake==REST_IN_PEACE)
		exit(0);
	be_undertaker(mission_p, state);
	exit(0);
}

pid_t wrap_fork(){
	pid_t pid;
	pid=fork();
	if (pid==-1){
		perror("fork error");
		exit(-1);
	}
	return pid;
}

int wrap_waitall(){
	int status;
	int try=0;
	pid_t pid;
try_waitall:
	while ((pid=wait(&status))>0);
	if (errno==EINTR){
		try++;
		if (try<5)
			goto try_waitall;
		return -1;
	}
	else if (errno==ECHILD)
		return 0;
	else return -1;
}
