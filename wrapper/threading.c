#include <stdio.h>
#include <errno.h>
#include "wrapper/threading.h"

int wrap_thrd_create(pthread_t *thread, void *(*routine) (void *), void *arg){
	int res;
try_thrd_create:
	res=pthread_create(thread, NULL, routine, arg);
	if (res<0)
		if (errno==EAGAIN) goto try_thrd_create;
		else return -1;
	return res;
}

inline int wrap_thrd_cancel(pthread_t thread){
	return pthread_cancel(thread);
}

int wrap_thrd_join(pthread_t thread, void **retval){
	int res=pthread_join(thread, retval);
	if (res<0)
		if (errno==EDEADLK){
			perror("dead lock");
			fprintf(stderr, "deadlock occurs, thread No.: %d.\n", thread);
		}
	return res;
}

inline int wrap_thrd_detach_me(){
	return pthread_detach(pthread_self());
}

inline int wrap_mutex_init(pthread_mutex_t *mutex){
	if (pthread_mutex_init(mutex, NULL)){
		perror("mutex init error");
		return -1;
	}
	return 0;
}

int wrap_errchk_mutex_init(pthread_mutex_t *mutex){
	pthread_mutexattr_t mutex_attr;
	if (pthread_mutexattr_init(&mutex_attr)){
		perror("mutex attr init");
		return -1;
	}
	if (pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_ERRORCHECK)){
		perror("mutex attr set");
		return -1;
	}
	if (pthread_mutex_init(mutex, &mutex_attr)){
		perror("mutex init error");
		return -1;
	}
	if (pthread_mutexattr_destroy(&mutex_attr)){
		perror("mutex attr destroy");
		return -1;
	}
	return 0;
}

int wrap_recur_mutex_init(pthread_mutex_t *mutex){
	pthread_mutexattr_t mutex_attr;
	if (pthread_mutexattr_init(&mutex_attr)){
		perror("mutex attr init");
		return -1;
	}
	if (pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE)){
		perror("mutex attr set");
		return -1;
	}
	if (pthread_mutex_init(mutex, &mutex_attr)){
		perror("mutex init error");
		return -1;
	}
	if (pthread_mutexattr_destroy(&mutex_attr)){
		perror("mutex attr destroy");
		return -1;
	}
	return 0;
}

inline int wrap_mutex_destory(pthread_mutex_t *mutex){
	return pthread_mutex_destroy(mutex);
}
