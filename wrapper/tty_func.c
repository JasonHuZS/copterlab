#define _BSD_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include "wrapper/tty_func.h"

static struct termios origin_attrs;
static struct termios old_attrs;

int open_tty(const char *dev){
	int fd=open(dev, O_RDWR | O_NOCTTY | O_NDELAY | O_SYNC);
	if (fd==-1)
		perror(FILE_OPEN_ERROR);
	if (fcntl(fd, F_SETFL, 0)<0){
		perror("fcntl error!");
		close(fd);
		fd=-1;
	}
	if (!isatty(STDIN_FILENO)){
		perror("standard input error! ");
		close(fd);
		fd=-1;
	}
	return fd;
}

void close_tty(int fd){
	if  (tcsetattr(fd,TCSANOW,&old_attrs)) { 
		perror(GET_ATTR_ERROR);
	}
	close(fd);
}

int open_and_init_tty(const char *dev){
	int fd=open_tty(dev);
	if (fd==-1){
		perror(FILE_OPEN_ERROR);
		return -1;
	}
	if  (tcgetattr(fd,&old_attrs)) { 
		perror(GET_ATTR_ERROR);
		return -1;
	}
	struct termios opt;
	int br=B9600;
	cfsetispeed(&opt, br);
	cfsetospeed(&opt, br); 
	opt.c_cflag = (CS8 | CLOCAL | CREAD);
	opt.c_iflag = (IGNPAR | IXON | IXOFF | IXANY);
	opt.c_lflag = 0;
	opt.c_oflag = 0;
	opt.c_cc[VTIME] = 30;
	opt.c_cc[VMIN] = 5;
	tcflush(fd, TCIOFLUSH);
	if (tcsetattr(fd,TCSANOW,&opt) != 0) {
		perror(SET_ATTR_ERROR);   
		return -1;
	}
	return fd;
}

int set_baudrate(int fd, int baud){
	int br;
	int status;
	struct termios opt;
	if (tcgetattr(fd, &opt)) {
		perror(GET_ATTR_ERROR);
		return -1;
	}
	switch (baud) {
		case 115200: 	br=B115200;break;
//		case 76800: 	br=B76800; break;
		case 57600: 	br=B57600; break;
		case 38400: 	br=B38400; break;
		case 19200: 	br=B19200; break;
		case 9600: 	br=B9600;  break;
		case 4800: 	br=B4800;  break;
		case 2400: 	br=B2400;  break;
		case 1200: 	br=B1200;  break;
		case 600: 	br=B600;   break;
		case 300: 	br=B300;   break;
		default:	perror("Wrong Baud rate!");
				return -1;
	}
	tcflush(fd, TCIOFLUSH);
	cfsetispeed(&opt, br);
	cfsetospeed(&opt, br);   
	status = tcsetattr(fd, TCSANOW, &opt);  
	if (status) {        
		perror(BAUD_RATE_ERROR);  
		return -2;
	}
	tcflush(fd,TCIOFLUSH);   
	return 0;
}

int set_dataformat(int fd,int databits,int parity,int stopbits){ 
	struct termios options; 
	if  (tcgetattr(fd,&options)) { 
		perror(GET_ATTR_ERROR);     
		return -1;
	}
	options.c_cflag &= ~CSIZE; 
	switch (databits) {
	case 5:
		options.c_cflag |= CS5; 
		break;
	case 6:
		options.c_cflag |= CS6; 
		break;
	case 7:	
		options.c_cflag |= CS7; 
		break;
	case 8: 
		options.c_cflag |= CS8;
		break;   
	default:    
		perror("Unsupported data size");
		return -2;  
	}
	switch (parity) {
	case 'n':		//no parity check
	case 'N':
		options.c_cflag &= ~PARENB;
		options.c_iflag &= ~INPCK;
		break;  
	case 'o':		//odd check
	case 'O':
		options.c_cflag |= (PARODD | PARENB);
		options.c_iflag |= (INPCK | ISTRIP);
		break;  
	case 'e':		//even check
	case 'E':
		options.c_cflag |= PARENB;
		options.c_cflag &= ~PARODD;
		options.c_iflag |= (INPCK | ISTRIP);
		break;
	default:   
		perror("Unsupported parity");    
		return -3;  
	}  
	switch (stopbits) {
	case 1:			//1 stop bit
		options.c_cflag &= ~CSTOPB;  
		break;  
	case 2:			//2 stop bits
		options.c_cflag |= CSTOPB;  
		break;
	default:    
		perror("Unsupported stop bits");  
		return -4; 
	} 
	tcflush(fd,TCIFLUSH);
	options.c_cc[VTIME] = 50;
	options.c_cc[VMIN] = 1;
	if (tcsetattr(fd,TCSANOW,&options) != 0) {
		perror(SET_ATTR_ERROR);   
		return -1;
	}
	return 0;
}

int set_databit(int fd, int databits){
	struct termios options; 
	if  (tcgetattr(fd,&options)) { 
		perror(GET_ATTR_ERROR);     
		return -1;
	}
	options.c_cflag &= ~CSIZE; 
	switch (databits) {
	case 5:
		options.c_cflag |= CS5; 
		break;
	case 6:
		options.c_cflag |= CS6; 
		break;
	case 7:	
		options.c_cflag |= CS7; 
		break;
	case 8: 
		options.c_cflag |= CS8;
		break;   
	default:    
		perror("Unsupported data size");
		return -1;  
	}
	tcflush(fd,TCIFLUSH);
	if (tcsetattr(fd,TCSANOW,&options) != 0) {
		perror(SET_ATTR_ERROR);   
		return -2;
	}
	return 0;
}

int set_parity(int fd, int parity) {
	struct termios options; 
	if  (tcgetattr(fd,&options)) { 
		perror(GET_ATTR_ERROR);     
		return -1;
	}
	options.c_cflag &= ~CSIZE; 
	switch (parity) {
	case 'n':		//no parity check
	case 'N':
		options.c_cflag &= ~PARENB;
		options.c_iflag &= ~INPCK;
		break;  
	case 'o':		//odd check
	case 'O':
		options.c_cflag |= (PARODD | PARENB);
		options.c_iflag |= (INPCK | ISTRIP);
		break;  
	case 'e':		//even check
	case 'E':
		options.c_cflag |= PARENB;
		options.c_cflag &= ~PARODD;
		options.c_iflag |= (INPCK | ISTRIP);
		break;
	default:   
		perror("Unsupported parity");    
		return -1;  
	}
	tcflush(fd,TCIFLUSH);
	if (tcsetattr(fd,TCSANOW,&options) != 0) {
		perror(SET_ATTR_ERROR);   
		return -2;
	}
	return 0;
}

int set_stopbit(int fd, int stopbits) {
	struct termios options; 
	if  (tcgetattr(fd,&options)) { 
		perror(GET_ATTR_ERROR);     
		return -1;
	}
	options.c_cflag &= ~CSIZE; 
	switch (stopbits) {
	case 1:			//1 stop bit
		options.c_cflag &= ~CSTOPB;  
		break;  
	case 2:			//2 stop bits
		options.c_cflag |= CSTOPB;  
		break;
	default:    
		perror("Unsupported stop bits");  
		return -1; 
	}
	tcflush(fd,TCIFLUSH);
	if (tcsetattr(fd,TCSANOW,&options) != 0) {
		perror(SET_ATTR_ERROR);   
		return -2;
	}
	return 0;
}

int hwflow_ctrl(int fd){
	struct termios options;
	if  (tcgetattr(fd,&options)) { 
		perror(GET_ATTR_ERROR);     
		return -1;
	}
	options.c_cflag |= CRTSCTS;
	options.c_iflag &= ~(IXON | IXOFF | IXANY);
	tcflush(fd,TCIFLUSH);
	if (tcsetattr(fd,TCSANOW,&options) != 0) {
		perror(SET_ATTR_ERROR);   
		return -2;
	}
	return 0;
}

int swflow_ctrl(int fd){
	struct termios options;
	if  (tcgetattr(fd,&options)) { 
		perror(GET_ATTR_ERROR);     
		return -1;
	}
	options.c_cflag &= ~CRTSCTS;
	options.c_iflag |= (IXON | IXOFF | IXANY);
	tcflush(fd,TCIFLUSH);
	if (tcsetattr(fd,TCSANOW,&options) != 0) {
		perror(SET_ATTR_ERROR);   
		return -2;
	}
	return 0;
}

int store_termios(int fd){
	if  (tcgetattr( fd,&origin_attrs)) { 
		perror(GET_ATTR_ERROR);
		return -1;
	}
	return 0;
}
int recover_termios(int fd){
	tcflush(fd, TCIOFLUSH);
	if (tcsetattr(fd,TCSANOW,&origin_attrs)) {
		perror(SET_ATTR_ERROR);
		return -1;
	}
	tcflush(fd, TCIOFLUSH);
	return 0;
}
